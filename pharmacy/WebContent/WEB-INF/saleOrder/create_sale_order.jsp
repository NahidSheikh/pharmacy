<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hospital Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
        
        
          <!-- select js
		============================================ -->
        <link rel="stylesheet" href="selectjs/style.css">
		<script src="selectjs/selectjquery.js"></script>
		<script src="selectjs/choosen.js"></script>
		
		
		
		<!--  datepicker  -->
		
	    <link rel="stylesheet" href="css2/datepicker.css">
		<script src="selectjs/datepicker.js"></script>
		
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
    	
    	$(".chosen").chosen();
		$(".add_stock").hide();

    	
    });
    
   
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
   
    function pat_add() {
    	var patient_name=$("#patient_name").val();
    	//alert(patient_name);
    	var pat_id=patient_name.split("/");
    	var p_name=pat_id[0];
    	var p_mb=pat_id[1];
    	var p_id=pat_id[2];
    	
    	    	
		 $.ajax({
			type:"GET",
			url:"PatientServletList",
			data:{p_id:p_id},
			dataType: "json",
	   		  success: function(json){
	  		var json1 = jQuery.parseJSON(JSON.stringify(json));
	  		//alert(json1);
	  		var dat=json1.split("/");
	  		$("#patient_name").val(p_name);
	  		$("#mobile_number").val(p_mb);
	  		$("#mailing_address").val(dat[0]);
	  		$("#patient_id").val(dat[2]);
	  		
	  		var gender=dat[1];
	  		if(gender=='Male')
	        {
	           $("#male_u" ).attr( 'checked', 'checked' )
	        }
	       if(gender=='Female')
	    {
	       $("#female_u" ).attr( 'checked', 'checked' )
	    }

				
			}
		});	 
		 
		 
		
    }
    
    
    
    
    function medicine_info() {
    	var medicine_name=$("#batch_lot").val();
    	//alert(medicine_name);
    	var med_split=medicine_name.split("/");
    	
    	
    	$.ajax({
			type:"POST",
			url:"OutpatientPharmcyBillServlet",
			data:{med_id:med_split[0]},
			dataType: "json",
	   		  success: function(json){
	  		var json1 = jQuery.parseJSON(JSON.stringify(json));
	  		//alert(json1);
  		    	 $.each(json1, function(index, element) {
  			var mrp=parseFloat(element.mrp)/parseFloat(element.unit);

	  		$("#medicine_company").val(element.company_name);
	  		$("#only_pack").val(element.unit);
	  		$("#medicine_pack").val(element.pack);
			$("#medicine_type").val(element.medicine_type);
			$("#medicine_category").val(element.medicine_category);
			$("#medicine_hsn").val(element.hsn);
			$("#medicine_rate").val(mrp.toFixed(2));
			$("#med_exp_date").val(element.e_date);
			$("#batch_id").val(element.b_id);
			$("#tax_name").val(element.tax);
			$("#out_table_id").val(element.id);
			
			var batch_lot=$("#batch_lot").val();
	    	var batch_lot_sp=batch_lot.split(":");
	    	
	    	$("#ava_quant").val(batch_lot_sp[3]);
	    	//alert(batch_lot_sp[3]);
			
  		    	 });
			}
		});	
		
		
	}
   
    function amount_cal() {
    	
    	var rate =$("#medicine_rate").val();
    	var qauntity=$("#medicine_quantity").val();
    	var ava_quant=$("#ava_quant").val();
    	//alert(ava_quant);
    	
    	/* alert(qauntity);

    	alert(ava_quant); */
    	
    	
    	if(rate.trim()=='')
    		{
    		rate=0;
    		}
    	
    	if(parseFloat(qauntity)>parseFloat(ava_quant))
    		{
    		$("#medicine_quantity").val(0);
    		}
    	
    	
    
    	if(parseFloat(qauntity)>0)
    		{
    		$(".add_stock").show();
    		}
    	else{
    		$(".add_stock").hide();

    	}
        
    	var amount = parseFloat(rate) * (parseFloat(qauntity));
    	$("#medicine_amount").val(amount.toFixed(2));
	}
    
    
    
    function add_medicine() {

    	var medicine_name=$("#medicine_name").val();
    	var medicine_company=$("#medicine_company").val();
    	var medicine_pack=$("#medicine_pack").val();
    	var medicine_type=$("#medicine_type").val();
    	var medicine_category=$("#medicine_category").val();
    	var medicine_hsn=$("#medicine_hsn").val();
    	
    	var medicine_rate=$("#medicine_rate").val();
    	var medicine_quantity=$("#medicine_quantity").val();
    	var batch_lot=$("#batch_lot").val();
    	var medicine_amount=$("#medicine_amount").val();
    	var batch_id=$("#batch_id").val();
    	var out_table_id=$("#out_table_id").val();
    	
    	var med_exp_date=$("#med_exp_date").val();
    	var tax_name=$("#tax_name").val();
    	var ava_quant=$("#ava_quant").val();
    	var only_pack=$("#only_pack").val();
    	
    	
    	
    	
    	
    	var total_amount=0;

    	/******************************************/
    	/************ Calulate Tax  *************/
    	var cgst=0;
    	var sgst=0;
    	var igst=0;
    	var tax_amount=0;
    	var tax_split=tax_name.split("@");
    	if(tax_split[0]=="GST")
    		{
    		  tax_amount=parseFloat(medicine_amount)*(parseFloat(tax_split[1])/100);
    		  cgst=parseFloat(tax_amount)/2;
    		  sgst=parseFloat(tax_amount)/2;
    		}else{
    			tax_amount=parseFloat(medicine_amount)*(parseFloat(tax_split[1])/100);
    			igst=tax_amount;
    		}
    	
        /************ End Calulate Tax  *************/
    	/******************************************/

        total_amount=parseFloat(medicine_amount);
        var dec_tax_amount=parseFloat(medicine_amount)-parseFloat(tax_amount);
        
        
        
        var m_split=medicine_name.split("/");
        var m_id=m_split[0];
        var m_name=m_split[1];
        
    	var sno=$("#med_table tbody tr").length+1;
    	
    	var mark='<tr id="row_'+m_id+'">'+
    		     '<td ><input type="hidden" name="m_id" id="m_id_'+m_id+'" value="'+m_id+'">'+sno+'</td>'+
    		     '<td>'+
    		     '<input type="hidden" name="m_name"     id="m_name_'+m_id+'" value="'+m_name+'">'+m_name+
    		     '<input type="hidden" name="m_company"     id="m_company_'+m_id+'" value="'+medicine_company+'">'+
    		     '<input type="hidden" name="m_pack"     id="m_pack_'+m_id+'" value="'+medicine_pack+'">'+
    		     '<input type="hidden" name="m_type"     id="m_type_'+m_id+'" value="'+medicine_type+'">'+
    		     '<input type="hidden" name="m_category"     id="m_category_'+m_id+'" value="'+medicine_category+'">'+
    		     '<input type="hidden" name="m_hsn"     id="m_hsn_'+m_id+'" value="'+medicine_hsn+'">'+
    		     '<input type="hidden" name="m_batch_lot"     id="m_batch_lot_'+m_id+'" value="'+batch_lot+'">'+
    		     '<input type="hidden" name="m_ava_quant"  id="m_ava_quant_'+m_id+'" value="'+ava_quant+'">'+
    		     '<input type="hidden" name="m_only_pack"  id="m_only_pack_'+m_id+'" value="'+only_pack+'">'+
    		     '<input type="hidden" name="m_out_table_id"  id="m_out_table_id_'+m_id+'" value="'+out_table_id+'">'+

    		     '</td>'+
    		     '<td><input type="hidden" name="m_batch_id"     id="m_batch_id_'+m_id+'" value="'+batch_id+'">'+batch_id+'</td>'+

    		     '<td><input type="hidden" name="m_rate"     id="m_rate_'+m_id+'" value="'+medicine_rate+'">'+medicine_rate+'</td>'+
    		     '<td><input type="hidden" name="m_quantity" id="m_quantity_'+m_id+'" value="'+medicine_quantity+'">'+medicine_quantity+'</td>'+

    		     '<td><input type="hidden" name="m_amount"   id="m_amount_'+m_id+'" value="'+dec_tax_amount.toFixed(2)+'">'+dec_tax_amount.toFixed(2)+'</td>'+
    		     '<td><input type="hidden" name="m_tax_name" id="m_tax_name_'+m_id+'" value="'+tax_name+'">'+tax_name+'</td>'+
    		     '<td><input type="hidden" name="m_tax_amount" id="m_tax_amount_'+m_id+'" value="'+tax_amount.toFixed(2)+'">'+tax_amount.toFixed(2)+'</td>'+
    		     '<td><input type="hidden" name="m_cgst"     id="m_cgst_'+m_id+'" value="'+cgst.toFixed(2)+'">'+cgst.toFixed(2)+'</td>'+
    		     '<td><input type="hidden" name="m_sgst"     id="m_sgst_'+m_id+'" value="'+sgst.toFixed(2)+'">'+sgst.toFixed(2)+'</td>'+
    		     '<td><input type="hidden" name="m_igst"     id="m_igst_'+m_id+'" value="'+igst.toFixed(2)+'">'+igst.toFixed(2)+'</td>'+
    		     '<td><input type="hidden" name="m_total_amount" id="m_total_amount_'+m_id+'" value="'+total_amount.toFixed(2)+'">'+total_amount.toFixed(2)+'</td>'+
    		     '<td><input type="hidden" name="m_exp_date"  id="m_exp_date_'+m_id+'" value="'+med_exp_date+'">'+med_exp_date+'</td>'+
    		     '<td><center><i class="fa fa-trash" onclick="med_delete('+m_id+')"></i></center></td>'+
    	         '</tr>'; 
    	       
    	         $("#med_table tbody ").append(mark);
            
    	

    	         /******************************************/
    	     	/************ Calulate Total  *************/
    	     	
    	     	var total_cgst=$("#total_cgst").val();
    	        var total_sgst=$("#total_sgst").val();
    	        var total_igst=$("#total_igst").val();
    	        var total_tax=$("#total_tax").val();
    	        var sub_total=$("#sub_total").val();
    	        var net_amount=$("#net_amount_hid").val();
    	        
    	        total_cgst=parseFloat(total_cgst)+parseFloat(cgst.toFixed(2));
    	        total_sgst=parseFloat(total_sgst)+parseFloat(sgst.toFixed(2));
    	        total_igst=parseFloat(total_igst)+parseFloat(igst.toFixed(2));
    	        total_tax=parseFloat(total_tax)+parseFloat(tax_amount.toFixed(2));
    	        sub_total=parseFloat(sub_total)+parseFloat(parseFloat(dec_tax_amount).toFixed(2));
    	        net_amount=parseFloat(net_amount)+parseFloat(total_amount.toFixed(2));

    	        $("#total_cgst").val(total_cgst.toFixed(2));
    	        $("#total_sgst").val(total_sgst.toFixed(2));
    	        $("#total_igst").val(total_igst.toFixed(2));
    	        $("#total_tax").val(total_tax.toFixed(2));
    	        $("#sub_total").val(sub_total.toFixed(2));
    	        $("#net_amount").val(net_amount.toFixed(2));

    	        $("#net_amount_hid").val(net_amount.toFixed(2));

    	          /************End Calulate Total  *************/
    	         /******************************************/
    	         
    	         
    	         /*******************  ******************/
    	         
    	        $("#medicine_name").val("");
    	    	$("#medicine_company").val("");
    	    	$("#medicine_pack").val("");
    	    	$("#medicine_type").val("");
    	    	$("#medicine_category").val("");
    	    	$("#medicine_hsn").val("");
    	    	$("#medicine_rate").val("");
    	    	$("#medicine_quantity").val("");
    	    	$("#free_quantity").val("");
    	    	$("#medicine_amount").val("");
    	    	$("#med_exp_date").val("");
    	    	$("#batch_id").val("");
    	    	$("#stock_quantity").val("");
    	    	$("#only_pack").val("");
    	    	$("#mrp").val("");

        		$(".add_stock").hide();

	}
    
    
    function med_delete(val) {
    	var id=val;
    	
    	var total_cgst=$("#total_cgst").val();
    	var total_sgst=$("#total_sgst").val();
    	var total_igst=$("#total_igst").val();
    	var total_tax=$("#total_tax").val();
    	var sub_total=$("#sub_total").val();
    	var net_amount=$("#net_amount_hid").val();
    	
    	var m_cgst=$("#m_cgst_"+id).val();
    	var m_sgst=$("#m_sgst_"+id).val();
    	var m_igst=$("#m_igst_"+id).val();
    	var m_tax_amount=$("#m_tax_amount_"+id).val();
    	var m_amount=$("#m_amount_"+id).val();
    	var m_total_amount=$("#m_total_amount_"+id).val();
    	
    	total_cgst=parseFloat(total_cgst)-parseFloat(m_cgst);
    	total_sgst=parseFloat(total_sgst)-parseFloat(m_sgst);
    	total_igst=parseFloat(total_igst)-parseFloat(m_igst);
    	total_tax=parseFloat(total_tax)-parseFloat(m_tax_amount);
    	sub_total=parseFloat(sub_total)-parseFloat(m_amount);
    	net_amount=parseFloat(net_amount)-parseFloat(m_total_amount);
    	
    	$("#total_cgst").val(total_cgst.toFixed(2));
    	$("#total_sgst").val(total_sgst.toFixed(2));
    	$("#total_igst").val(total_igst.toFixed(2));
    	$("#total_tax").val(total_tax.toFixed(2));
    	$("#sub_total").val(sub_total.toFixed(2));
    	$("#net_amount").val(net_amount.toFixed(2));
    	$("#net_amount_hid").val(net_amount.toFixed(2));


    	
    	$("#row_"+id).remove();
    	renumberRows1();
	}

    function renumberRows1() {
        $('#med_table tr').each(function(index, el){
            $(this).children('td').first().text((index++));
        });
    }
    
    function round_off_function() {
		var round_off=$("#round_off").val();
		var net_amount_hid=$("#net_amount_hid").val();
		//alert(round_off);
		
		if(round_off=="")
			{
			round_off=0;
			}
		var n_amount=0;
		if(parseFloat(round_off)<0)
			{
			n_amount=parseFloat(net_amount_hid)+parseFloat(round_off);
			}else{
				n_amount=parseFloat(net_amount_hid)+parseFloat(round_off);

			}
		$("#net_amount").val(n_amount.toFixed(2));
	}
    
    function due_date_func() {
    	
		var terms=$("#terms").val();
		var date=$("#date").val();

		
		if(terms=='default')
			{
			alert("Select term!");
			}
		else if(date=='')
			{
			alert("Select From Date!");

			}else{
		
		var days_sp=terms.split(" ");
		var on_date=date.split("/");
		myDate = new Date();
		plusSeven = new Date(myDate.setDate(parseInt(on_date[0]) + parseInt(days_sp[0])));
		var dd=new Date(plusSeven);
		var due_date=dd.getDate() + '/' + (dd.getMonth()+1) + '/' + dd.getFullYear();
		$("#to_date").val(due_date);

			}

	}
    
    
    function show_table() {
		window.location.href='create_stock_inward';
	}
    
    function add_patient() {
    	$("#clearhistory").modal();
		
	}
    
    function save_patient_info() {
        var patient_name = $("input[name='patient_name_i']").val();
        var mobile_no=$("input[name='mobile_no_i']").val();
        var gender = $("input[name='gender_i']:checked").val();
        var address=$("input[name='address_i']").val();
    

        
        
        $.ajax({
        	  type:"Get",
        	  url:"OutsidePatientRegistration",
        	  data:{patient_name:patient_name,mobile_no:mobile_no,gender:gender,address:address},
        	  datatype:"json",
        	  success:function(json){
        		  var json1 = jQuery.parseJSON(JSON.stringify(json));
        		     //alert(json1);
        		     var mark='<option value="'+json1+'">'+json1+'</option>';
        		     
        		     $("#ven_id").append(mark);
        		     
        		     $("input[name='patient_name_i']").val("");
        		     $("input[name='mobile_no_i']").val("");
        		     $("input[name='gender_i']:checked").val("");
        		     $("input[name='address_i']").val("");
        		    
        		    	$("#clearhistory").modal('hide');

        	  }
        });


		
	}
    
    
    function add_batch() {
    	var medicine_name=$("#medicine_name").val();
    	//alert(medicine_name);
    	var med_split=medicine_name.split("/");
    	
    	$.ajax({
			type:"Get",
			url:"OutpatientPharmcyBillServlet",
			data:{m_name:med_split[1],med_id:med_split[0]},
			dataType: "json",
	   		  success: function(json){
	  		var json1 = jQuery.parseJSON(JSON.stringify(json));
	  		//alert(json1);.
	  		$("#batch_lot").empty();
	  		var mm='<option value="default">Select Batch Id</option>';
	  		$("#batch_lot").append(mm);
	  		
  		    	 $.each(json1, function(index, element) {
                  /*  alert(element.id);
                   alert(element.batch_id);
                    */
                   var mark='<option value="'+element.id+':'+element.batch_id+':'+element.exp_date+':'+element.pend_quantity+'" style="font-size:18px">Batch Id='+element.batch_id+
                   "::Exp Date="+element.exp_date+"::Quantity="+element.pend_quantity+'</option>';

  		    		 $("#batch_lot").append(mark);
  		    		 
  		    		 //var mm='<span>'++'</span>'
  		    		 
  		    	 });
	  		
			}
		});	
		
	}
    
    </script>
    
    <style type="text/css">
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>

<%
request.getSession().setAttribute("sale_ses", "sale_medicine");
%>
    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <jsp:include page="../public/topmenu.jsp"></jsp:include>
            <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>
            <jsp:include page="subheader_saleorder.jsp"></jsp:include>
        </div>
        
        
                                
                                            
        
        
        
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-3 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Sale Medicine:</h4>
                            </div>
                           
                            
                            <!-- <div class="col-md-3 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                         				<div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="show_table()">Show Table</button>
                                            
                                          </div>
                            </div> -->
                            
                            
                            
                     <form action="insert_sale_order" method="post" >
                     
                            <table class="table table-bordered table-padding">
   							 <thead class="table-padding">
                                <tr class="table-padding">
                                    <th colspan="2" class="table-padding">Patient Name :  &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="Add Patient" style="color: black;" onclick="add_patient()"></th>
                                    <th colspan="1" class="table-padding">Mobile No. :</th>
                                    <th colspan="1" class="table-padding">Gender :</th>
                                    
                                    <th colspan="2" class="table-padding">Mailing Address :</th>
                    
                                </tr>
                                </thead>
                                <tbody class="table-padding">
                               
                        				<tr class="table-padding">
                        				 <td colspan="2"  class="table-padding">
                        				 <input type="hidden" id="patient_id" name="patient_id">
                        				 <input type="text" list="ven_id" name="patient_name" id="patient_name"  class="form-control color-border " placeholder="Patient Name" required="required" autocomplete="off" onchange="pat_add()">
                        				 <datalist id="ven_id">
<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from patient_registration ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("patient_name")%>/<%=rs.getString("mobile_no")%>/<%=rs.getString("patient_id")%>"><%=rs.getString("patient_name")%>/<%=rs.getString("mobile_no")%>/<%=rs.getString("patient_id")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>	                        				 </datalist>
                        				 </td>
                        				 <td colspan="1" class="table-padding" ><input type="text" name="mobile_number" id="mobile_number" maxlength="10"   class="form-control color-border" placeholder="Mobile Number" required="required" autocomplete="off" ></td>
                        				 <td colspan="1" class="table-padding" >
                        				 <input type="radio" name="gender" value="Male" id="male_u"><span class="gend_color">Male</span>
                        				 <input type="radio" name="gender" value="Female" id="female_u"><span class="gend_color">Female</span>
                        				 </td>
                        				 
                        				 <td colspan="3" class="table-padding" ><input type="text" name="mailing_address" id="mailing_address"   class="form-control color-border" placeholder="Mailing Address" required="required" autocomplete="off" ></td>
                        				</tr>
                        		 </tbody>
                        		 
                        		
                        		
                        		 
                        		 
                        		  <thead class="table-padding">
                                <tr class="table-padding">
                                   <th colspan="1" class="table-padding">Bill Date:</th>
                                   <th colspan="2" class="table-padding">Bill No:</th>
                                
                                    <th colspan="2" class="table-padding">Outward No:</th>
                                    <th colspan="1" class="table-padding"><!-- Due Date: --></th>
                                    
                                    
                                </tr>
                                </thead>
                                 <tbody>
                                <tr class="table-padding">
                                         <td colspan="1" class="table-padding" ><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text" autocomplete="off" /></td>
                                         <td colspan="2" class="table-padding" >
                                         <%
                                         int bill_no=commonclaasses.sale_bill_no()+1;
                                         %>
                                         
                                         <input type="text" name="bill_no" id="bill_no" value="<%=bill_no %>"  class="form-control color-input" placeholder="Bill No" autocomplete="off" readonly="readonly"></td>
                                
                        				 <td colspan="2" class="table-padding" >
                        				 <% 
                        				 int invoice_id=commonclaasses.sale_bill_no(); 
                        				// System.out.println("invoice_no====="+invoice_id);
                        				 int invoice_no=invoice_id+1;
                        				 String invoce="Out/"+invoice_no;
                        				 
                        				 %>
                        				 
										  <input type="text" name="invoice_no" id="invoice_no" value="<%=invoce%>"   class="form-control color-input" placeholder="Invoice No"  autocomplete="off" readonly="readonly"> </td>                       			   
										 <td colspan="1" class="table-padding" ><!-- <input type="text" name="to_date" id="to_date"   class="form-control color-border" placeholder="To Date" autocomplete="off" > --></td>
                        			
                        		</tr>
                                </tbody>
                                
                                
                                <thead class="table-padding">
                                <tr class="table-padding">
                                    <th colspan="2" class="table-padding">Medicine Name :</th>
                                    <th colspan="2" class="table-padding">Batch Id :</th>
                                    
                                    <th colspan="1" class="table-padding">Medicine Company :</th>
                                    <th colspan="1" class="table-padding">Package :</th>
<!--                                     <th colspan="1" class="table-padding">Medicine Category :</th>
 -->                    
                                </tr>
                                </thead>
                                <tbody class="table-padding">
                               
                        				<tr class="table-padding">
                        				 <td colspan="2"  class="table-padding">
                        				 
                        				 
                        				 <input type="hidden" id="out_table_id">
                        				 <input type="hidden" id="ava_quant">
                        				 
                        				 
                        				 
                                                <select name="medicine_name" id="medicine_name"  class=" form-control color-border chosen" onchange="add_batch()">
																	<option value="default">Select Medicine Name</option>
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from master_medicine ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("id")%>/<%=rs.getString("medicine_name")%>"><%=rs.getString("medicine_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
															</select> 
                        				 
                        				 
                        				 
                        				 </td>
                        				 
                        				 <td colspan="2"  class="table-padding">
                                                <select name="batch_lot" id="batch_lot"  class=" form-control color-border " onchange="medicine_info()">
																	<option value="default">Select Batch </option>
																	
															</select> 
                        				 
                        				 
                        				 
                        				 </td>
                        				 <td colspan="1" class="table-padding"><input type="text" name="medicine_company" id="medicine_company"   class="form-control color-input" placeholder="Medicine Company"  autocomplete="off" readonly="readonly"></td>
                        				 <td colspan="1" class="table-padding">
                        				 
                        				 <input type="hidden" id="only_pack">
                        				 <input type="text" name="medicine_pack" id="medicine_pack"   class="form-control color-input" placeholder="Package"  autocomplete="off" readonly="readonly">
                        				 
                        				 </td>
                        				
                        				</tr>
                        		 </tbody> 
                        		 
                        		 
                        		 
                        		<thead class="table-padding">
                                <tr class="table-padding">
                                    <th colspan="1" class="table-padding">Medicine Type :</th>
                                
                                    <th colspan="1" class="table-padding">Medicine Category :</th>
                                    <th colspan="1" class="table-padding">HSN :</th>
                                    <th colspan="1" class="table-padding">MRP:</th>
                                    <th colspan="1" class="table-padding">Quantity:</th>
<!--                                     <th colspan="1" class="table-padding">Free Quantity:</th>
 -->                                    
                                    <th colspan="1" class="table-padding">Amount:</th>
                     
                                </tr>
                                </thead>
                                <tbody class="table-padding">
                               
                        				<tr class="table-padding">
                        				<td colspan="1" class="table-padding" ><input type="text" name="medicine_type" id="medicine_type"   class="form-control color-input" placeholder="Medicine Type" autocomplete="off" readonly="readonly"></td>
                        				
                        				 <td colspan="1" class="table-padding"><input type="text" name="medicine_category" id="medicine_category"   class="form-control color-input" placeholder="Medicine Cotegory"  autocomplete="off" readonly="readonly"></td>
                        				 <td colspan="1" class="table-padding"><input type="text" name="medicine_hsn" id="medicine_hsn"   class="form-control color-input" placeholder="HSN"  autocomplete="off" readonly="readonly"></td>
                        				 <td colspan="1" class="table-padding"><input type="number" name="medicine_rate" id="medicine_rate"   class="form-control  color-input" placeholder="MRP"  autocomplete="off"  onkeyup="amount_cal()" readonly="readonly"></td>
                        				 <td colspan="1" class="table-padding" ><input type="number" name="medicine_quantity" id="medicine_quantity"   class="form-control color-border" placeholder="Quantity"  autocomplete="off" onkeyup="amount_cal()"></td>
<!--                         				 <td colspan="1" class="table-padding" ><input type="number" name="free_quantity" id="free_quantity"   class="form-control color-border" placeholder="Free Quantity"  autocomplete="off" onkeyup="amount_cal()"></td>
 -->                        				 <td colspan="1" class="table-padding"><input type="text" name="medicine_amount" id="medicine_amount"   class="form-control color-input" placeholder="Amount"  autocomplete="off" readonly="readonly"></td>
                        				
                        				</tr>
                        		 </tbody> 
                        		 
                        		 
                        		 <thead class="table-padding">
                                <tr class="table-padding">
                                    <th colspan="1" class="table-padding">Expiry Date:</th>
                                
                                    <th colspan="1" class="table-padding">Batch Id:</th>
                                    <!-- <th colspan="1" class="table-padding">MRP:</th>
                                    
                                    <th colspan="1" class="table-padding">Stock Quantity:</th> -->
                                    <th colspan="1" class="table-padding">Tax:</th>
                                    <th colspan="1" class="table-padding add_stock"    ></th>
                                     <!--<th colspan="1" class="table-padding">Rate :</th>
                                    <th colspan="1" class="table-padding">Quantity:</th>
                                    <th colspan="1" class="table-padding">Amount:</th>
                                    <th colspan="1" class="table-padding">Expiry Date:</th> -->
                     
                                </tr>
                                </thead>
                                <tbody class="table-padding">
                               
                        				<tr class="table-padding">
                        				 <td colspan="1" class="table-padding"><input type="text" name="med_exp_date" id="med_exp_date"   class="form-control color-input" placeholder="Expiry Date" autocomplete="off" readonly="readonly"></td>
                        				 <td colspan="1" class="table-padding" >
										  <input type="text" name="batch_id" id="batch_id"   class="form-control color-input" placeholder="Batch Id"  autocomplete="off" readonly="readonly"> </td>                       			   
										 <!-- <td colspan="1" class="table-padding" >
										  <input type="number" name="mrp" id="mrp"   class="form-control color-border" placeholder="MRP"  autocomplete="off" > </td>                       			   
										 
										 
										  <td colspan="1" class="table-padding"><input type="text" name="stock_quantity" id="stock_quantity"   class="form-control color-input" placeholder="Stock Quantity" autocomplete="off" readonly="readonly"></td>
										 
                        				  --><td colspan="1" class="table-padding">
                        				     <input name="tax_name" id="tax_name"  class=" form-control color-input " readonly="readonly">
																	
                        				 </td>
                        				 <td colspan="1" class="table-padding add_stock"><center><input type="button" value="Add" style="color: white;background-color: grey" class="btn btn-default" onclick="add_medicine()"></center></td>
                        				<!-- <td colspan="1" class="table-padding"><input type="number" name="medicine_rate" id="medicine_rate"   class="form-control  color-border" placeholder="Rate" required="required" autocomplete="off"  onkeyup="amount_cal()"></td>
                        				 <td colspan="1" class="table-padding" ><input type="number" name="medicine_quantity" id="medicine_quantity"   class="form-control color-border" placeholder="Quantity" required="required" autocomplete="off" ></td>
                        				 <td colspan="1" class="table-padding"><input type="text" name="medicine_amount" id="medicine_amount"   class="form-control color-input" placeholder="Amount" required="required" autocomplete="off" readonly="readonly"></td>
                        				 <td colspan="1" class="table-padding"><input type="text" name="med_exp_date" id="med_exp_date"   class="form-control color-input" placeholder="Expiry Date" required="required" autocomplete="off" readonly="readonly"></td>
                        				 -->
                        				</tr>
                        		 </tbody> 
                        		 
                        		 
                        		 </table>
                        		 
                        		 
                        		 <table class="table table-bordered" id="med_table">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Medicine_Name</th>
                                    <th>Batch_id</th>
                                    <th>MRP</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                    <th>Tax_Name:</th>
                                    <th>Tax_Amount:</th>
                                    <th>CGST:</th>
                                    <th>SGST:</th>
                                    <th>IGST:</th>
                                    <th>Total_Amount:</th>
                                    <th>Expiry_Date:</th>
                                   
                                    <th>Action:</th>
                                    
                                </tr>
                                </thead>
                                <tbody >
                               
                        		 </tbody>
                            </table>
                            
                            <div class="row">
                            <div class="col-md-4">
                            <label class="form-label">Memo:</label>
                            <textarea name="memo" rows="5" cols="10" class="form-control  color-border "></textarea>
                            </div>
                            
                             <div class="col-md-8">
                             <table class="table table-bordered table-padding">
   							 <thead class="table-padding">
                                <tr class="table-padding">
                                    <td colspan="1" >Total CGST :</td>
                                    <td colspan="1"><input type="number" name="total_cgst" id="total_cgst"  value="0" class="form-control  color-input " placeholder="Total CGST" required="required" autocomplete="off" readonly="readonly"></td>
                                    <td colspan="1" >Total Tax :</td>
                                    <td colspan="1"><input type="number" name="total_tax" id="total_tax" value="0" class="form-control  color-input" placeholder="Total Tax" required="required" autocomplete="off" readonly="readonly"></td>
                                </tr>
                                
                                <tr class="table-padding">
                                    <td colspan="1" >Total SGST :</td>
                                    <td colspan="1"><input type="number" name="total_sgst" id="total_sgst" value="0" class="form-control  color-input" placeholder="Total SGST" required="required" autocomplete="off" readonly="readonly"></td>
                                    <td colspan="1" >Sub Total :</td>
                                    <td colspan="1"><input type="number" name="sub_total" id="sub_total" value="0" class="form-control  color-input" placeholder="Sub Total" required="required" autocomplete="off" readonly="readonly"></td>
                                </tr>
                                <tr class="table-padding">
                                    <td colspan="1" >Total IGST :</td>
                                    <td colspan="1"><input type="number" name="total_igst" id="total_igst" value="0" class="form-control  color-input" placeholder="Total IGST" required="required" autocomplete="off" readonly="readonly"></td>
                                    <td colspan="1" >Net Amount :</td>
                                    <td colspan="1">
                                    <input type="number" name="net_amount" id="net_amount" value="0" class="form-control  color-input" placeholder="Net Amount" required="required" autocomplete="off" readonly="readonly">
                                    <input type="hidden"  id="net_amount_hid" value="0" >
                                    
                                    </td>
                                </tr>
                                <tr class="table-padding" style="text-align: right;">
                                    <td colspan="1" >Round Off:</td>
                                    <td colspan="1">
                                    <input type="number" name="round_off" id="round_off" value="0" class="form-control  color-border" placeholder="Round Off"  autocomplete="off" onkeyup="round_off_function()">
                                    </td>
                                </tr>
                                </thead>
                                <tbody class="table-padding">
                                </tbody>
                                </table>
                                </div>
                              </div>
                        		 
                        		 
                        		 <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Save</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                        		 
                        		</form> 
                           <!--  <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div> -->
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      
     
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
      
      
      
        <!-- Modal -->
  <div class="modal fade" id="clearhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Add Patient </h4>
        </div>
        <div class="modal-body">
             <div class="row">
             <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
                          <label class="form-label">Patient Name:</label>                                                       
                         </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                     <input  type="text" name="patient_name_i"  class="form-control" placeholder="Patient Name"  autocomplete="off">
                       
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     
                                                     <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
                          											<label class="form-label">Mobile Number:</label>                                                       
                        										 </div>
                                                             </div>
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
                                                                    <input  type="text" name="mobile_no_i" id="mobile_no_i" maxlength="10" class="form-control" placeholder="Mobile Number"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                            
                                                             <div class="col-md-2">
                                                                <div class="input-mark-inner">
                          											<label class="form-label">Gender:</label>                                                       
                        										 </div>
                                                             </div>
                                                            <div class="col-md-4">
                                                                <div class="input-mark-inner">
 										<input type="radio" name="gender_i" value="Male" >Male
                        				 <input type="radio" name="gender_i" value="Female" >Female
                        				                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
                          <label class="form-label">Address:</label>                                                       
                         </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                     <input  type="text" name="address_i" id="address_i"  class="form-control" placeholder="Address"  autocomplete="off">
                       
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                      </div>   
                                                        
                                                        
                                   </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="save_patient_info()">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  
</div>

    

    <!-- jquery
		============================================ -->
<!--     <script src="js/vendor/jquery-1.12.4.min.js"></script>
 -->    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
    
    
    <script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'dd/mm/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
		
		var med_exp_date=$('input[name="med_exp_date"]');
		med_exp_date.datepicker({
			format: 'mm/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
		
	})
	
	
</script>
</body>

</html>