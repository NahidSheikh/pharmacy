<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hospital Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
		$("#submit_form").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#show_table_deleted_entries").hide();
		$("#updatestaffmodal").hide();

    	
    });
    
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
    
    
    var med_arr=[];
    var tttt=0;
    //alert("l1==="+med_arr.length);
    function deletefunc(val){
    	var id=val;
    	window.location.href='doctor_view_appointment_master_opd?id='+id+'';

    	
    }
    
    
    function form_change(val){
    	if(val==0)
    		{
    		$("#submit_form").show();
    		$("#show_table").hide();
    		$("#show_table_deleted_entries").hide();

    		}
    	
    	if(val==1)
		{
		$("#submit_form").hide();
		$("#show_table").show();
		$("#show_table_deleted_entries").hide();

		}
    	if(val==2)
		{
		$("#submit_form").hide();
		$("#show_table").hide();
		$("#show_table_deleted_entries").show();

		}
    }
    
    function form_change_modal(val){
    	if(val==1)
    		{
    		$("#updatestaffmodal").show();
    		$("#deletestaffmodal").hide();
    		$("#historystaffmodal").hide();
    		$("#patientappointmentmodal").hide();

    		}
    	
    	if(val==0)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").show();
		$("#historystaffmodal").hide();
		$("#patientappointmentmodal").hide();

		}
    	
    	if(val==3)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#patientappointmentmodal").show();

		}
    	if(val==2)
		{
    	$("#historystaffmodal").show();
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		$("#patientappointmentmodal").hide();

		var s_id_his=$("#s_id_his").val();
		
		
		//$("#tablehis").append(markup);

		}
    }
    
   
    
    function clear_history() {
    	$("#clearhistory").modal();
		
	}
    function page_refresh()
    {
    	window.location.href='view_appointment_master_opd';
    }
    
    function search_medicine_funct() {
    	$("#searchMedicineModal").modal();
	}
    
    function search_injection_funct() {
    	$("#searchInjectionModal").modal();
	}
    
    
    function medicine_modal() {
      // $("#searchmedcineID").modal();
       //alert("hiiii");
       var ser_medcine_name=$("#ser_medcine_name").val();
       
       $.ajax({
   		  type: "POST",
   		  url: "MedicineServlet",
   		  data: {ser_medcine_name : ser_medcine_name},
   		  dataType: "json",
    	  success: function(json){
   		    var json1 = jQuery.parseJSON(JSON.stringify(json));
   		    var i=1;
   		 		 $("#searchmedcineID tbody").empty();
   		 		 
   		    	 $.each(json1, function(index, element) {
   		    		 //alert(element.id);
   		    		 var whole_med_id=$("#whole_med_id").val();
  		 
  		 if(whole_med_id!='')
  			 {
  			/******* Remove Collect Id ***********/
	    	 if(whole_med_id.includes(',')){
	    		 
 				var w_id_split=whole_med_id.split(",");
 				var t=0;
 				var i=0;
 				for(i=0;i<w_id_split.length;i++)
 					{

 			    	if(w_id_split[i]==element.id){
 			    		//alert(1);
 			    		
 			    	 var markup= '<tr id="row_ser_med_'+element.id+'" onclick="row_ser_med_func('+element.id+')">'+
			              '<td style="padding:2px">'+
			              '<input type="hidden" value="'+element.id+'">'+
			              '<input type="hidden" id="med_name_'+element.id+'" value="'+element.medicine_name+'">'+
			              '<input type="hidden" value="1" id="med_id_'+element.id+'">'+
			              '<input type="button" value=" " id="med_but_'+element.id+'" class="color_button_medicine_serch">&nbsp;&nbsp;'+element.medicine_name+
			              '</td>'+
			              '</tr>';
	    				$("#searchmedcineID tbody").append(markup);
	    				t=1;
 			    	}
 					}
 				if(t!=1){
 					var markup= '<tr id="row_ser_med_'+element.id+'" onclick="row_ser_med_func('+element.id+')">'+
		              '<td style="padding:2px">'+
		              '<input type="hidden" value="'+element.id+'">'+
		              '<input type="hidden" id="med_name_'+element.id+'" value="'+element.medicine_name+'">'+
		              '<input type="hidden" value="0" id="med_id_'+element.id+'">'+
		              '<input type="button" value=" " id="med_but_'+element.id+'" class="color_button_medicine">&nbsp;&nbsp;'+element.medicine_name+
		              '</td>'+
		              '</tr>';
		              
	    	$("#searchmedcineID tbody").append(markup);
 				}

	    	 }else{
	    		 if(whole_med_id==element.id){
	    		 var markup= '<tr id="row_ser_med_'+element.id+'" onclick="row_ser_med_func('+element.id+')">'+
	              '<td style="padding:2px">'+
	              '<input type="hidden" value="'+element.id+'">'+
	              '<input type="hidden" id="med_name_'+element.id+'" value="'+element.medicine_name+'">'+
	              '<input type="hidden" value="1" id="med_id_'+element.id+'">'+
	              '<input type="button" value=" " id="med_but_'+element.id+'" class="color_button_medicine_serch">&nbsp;&nbsp;'+element.medicine_name+
	              '</td>'+
	              '</tr>';
				$("#searchmedcineID tbody").append(markup);
	    		 }else{
			    		var markup= '<tr id="row_ser_med_'+element.id+'" onclick="row_ser_med_func('+element.id+')">'+
			              '<td style="padding:2px">'+
			              '<input type="hidden" value="'+element.id+'">'+
			              '<input type="hidden" id="med_name_'+element.id+'" value="'+element.medicine_name+'">'+
			              '<input type="hidden" value="0" id="med_id_'+element.id+'">'+
			              '<input type="button" value=" " id="med_but_'+element.id+'" class="color_button_medicine">&nbsp;&nbsp;'+element.medicine_name+
			              '</td>'+
			              '</tr>';
			              
		    	$("#searchmedcineID tbody").append(markup);
			    	}
	    	 }
	    	 /******* End Remove Collect Id ***********/
  			 
  			 
  			 }else{
   		    		 
       			  var markup= '<tr id="row_ser_med_'+element.id+'" onclick="row_ser_med_func('+element.id+')">'+
       			              '<td style="padding:2px">'+
       			              '<input type="hidden" value="'+element.id+'">'+
       			              '<input type="hidden" id="med_name_'+element.id+'" value="'+element.medicine_name+'">'+
       			              '<input type="hidden" value="0" id="med_id_'+element.id+'">'+
       			              '<input type="button" value=" " id="med_but_'+element.id+'" class="color_button_medicine">&nbsp;&nbsp;'+element.medicine_name+
       			              '</td>'+
       			              '</tr>';
       			              
    		    	$("#searchmedcineID tbody").append(markup);
  			 }
   		    	 i++;
   		    	 }); 
   		    } 
   		});
       
       
  		 
       
       
    }
    
    
    
    function injection_modal() {
        // $("#searchmedcineID").modal();
         //alert("hiiii");
         var ser_injection_name=$("#ser_injection_name").val();
         
         $.ajax({
     		  type: "POST",
     		  url: "InjectionServlet",
     		  data: {ser_injection_name : ser_injection_name},
     		  dataType: "json",
      	      success: function(json){
     		    var json1 = jQuery.parseJSON(JSON.stringify(json));
     		    var i=1;
     		 		 $("#searchinjectionID tbody").empty();
     		 		 
     		    	 $.each(json1, function(index, element) {
     		    		 //alert(element.id);
     		    		 var whole_inj_id=$("#whole_inj_id").val();
    		 
    		 if(whole_inj_id!='')
    			 {
    			/******* Remove Collect Id ***********/
  	    	 if(whole_inj_id.includes(',')){
  	    		 
   				var w_id_split=whole_inj_id.split(",");
   				var t=0;
   				var i=0;
   				for(i=0;i<w_id_split.length;i++)
   					{

   			    	if(w_id_split[i]==element.id){
   			    		//alert(1);
   			    		
   			    	 var markup= '<tr id="row_ser_inj_'+element.id+'" onclick="row_ser_inj_func('+element.id+')">'+
  			              '<td style="padding:2px">'+
  			              '<input type="hidden" value="'+element.id+'">'+
  			              '<input type="hidden" id="inj_name_'+element.id+'" value="'+element.medicine_name+'">'+
  			              '<input type="hidden" value="1" id="inj_id_'+element.id+'">'+
  			              '<input type="button" value=" " id="inj_but_'+element.id+'" class="color_button_medicine_serch">&nbsp;&nbsp;'+element.medicine_name+
  			              '</td>'+
  			              '</tr>';
  	    				$("#searchinjectionID tbody").append(markup);
  	    				t=1;
   			    	}
   					}
   				if(t!=1){
   					var markup= '<tr id="row_ser_inj_'+element.id+'" onclick="row_ser_inj_func('+element.id+')">'+
  		              '<td style="padding:2px">'+
  		              '<input type="hidden" value="'+element.id+'">'+
  		              '<input type="hidden" id="inj_name_'+element.id+'" value="'+element.medicine_name+'">'+
  		              '<input type="hidden" value="0" id="inj_id_'+element.id+'">'+
  		              '<input type="button" value=" " id="inj_but_'+element.id+'" class="color_button_medicine">&nbsp;&nbsp;'+element.medicine_name+
  		              '</td>'+
  		              '</tr>';
  		              
  	    	$("#searchinjectionID tbody").append(markup);
   				}

  	    	 }else{
  	    		 if(whole_med_id==element.id){
  	    		 var markup= '<tr id="row_ser_inj_'+element.id+'" onclick="row_ser_inj_func('+element.id+')">'+
  	              '<td style="padding:2px">'+
  	              '<input type="hidden" value="'+element.id+'">'+
  	              '<input type="hidden" id="inj_name_'+element.id+'" value="'+element.medicine_name+'">'+
  	              '<input type="hidden" value="1" id="inj_id_'+element.id+'">'+
  	              '<input type="button" value=" " id="inj_but_'+element.id+'" class="color_button_medicine_serch">&nbsp;&nbsp;'+element.medicine_name+
  	              '</td>'+
  	              '</tr>';
  				$("#searchinjectionID tbody").append(markup);
  	    		 }else{
  			    		var markup= '<tr id="row_ser_inj_'+element.id+'" onclick="row_ser_inj_func('+element.id+')">'+
  			              '<td style="padding:2px">'+
  			              '<input type="hidden" value="'+element.id+'">'+
  			              '<input type="hidden" id="inj_name_'+element.id+'" value="'+element.medicine_name+'">'+
  			              '<input type="hidden" value="0" id="inj_id_'+element.id+'">'+
  			              '<input type="button" value=" " id="inj_but_'+element.id+'" class="color_button_medicine">&nbsp;&nbsp;'+element.medicine_name+
  			              '</td>'+
  			              '</tr>';
  			              
  		    	$("#searchinjectionID tbody").append(markup);
  			    	}
  	    	 }
  	    	 /******* End Remove Collect Id ***********/
    			 
    			 
    			 }else{
     		    		 
         			  var markup= '<tr id="row_ser_med_'+element.id+'" onclick="row_ser_inj_func('+element.id+')">'+
         			              '<td style="padding:2px">'+
         			              '<input type="hidden" value="'+element.id+'">'+
         			              '<input type="hidden" id="inj_name_'+element.id+'" value="'+element.medicine_name+'">'+
         			              '<input type="hidden" value="0" id="inj_id_'+element.id+'">'+
         			              '<input type="button" value=" " id="inj_but_'+element.id+'" class="color_button_medicine">&nbsp;&nbsp;'+element.medicine_name+
         			              '</td>'+
         			              '</tr>';
         			              
      		    	$("#searchinjectionID tbody").append(markup);
    			 }
     		    	 i++;
     		    	 }); 
     		    } 
     		});
         
         
    		 
         
         
      }
    
    function row_ser_med_func(val){
	     var id=val;
	     var med_id=$("#med_id_"+id).val();
	     var med_name=$("#med_name_"+id).val();
    	 var whole_med_id=$("#whole_med_id").val();

	     
	     if(med_id=='0')
	    	 {
	    	 /******* Colleect Id ***********/
	    	 if(whole_med_id.trim()=='')
	    		 {
	    		 $("#whole_med_id").val(id);
	    		 }else{
		    		 var w_id=whole_med_id+","+id;
		    		 $("#whole_med_id").val(w_id);
	    		 }
	    	 /******* End Colleect Id ***********/

	    	 $("#med_but_"+id).css("background-color","#337ab7");
	    	 $("#med_id_"+id).val(1);
	    	 
	    	 var mark='<div class="col-md-6" id="add_med_'+id+'" style="background-color:grey;border:2px solid white">'+med_name+
	    	          '</div>';
	    	          $("#ser_med_add").append(mark);
	    	          
	    	 }
	     if(med_id=='1')
    	 {
	    	 /******* Remove Colleect Id ***********/
	    	 if(whole_med_id.includes(',')){
	    		 
 				var w_id_split=whole_med_id.split(",");
 				
 				var i=0;
 				var w_id='';
 				var t=0;
 				for(i=0;i<w_id_split.length;i++)
 					{
 					if(w_id_split[i]!=id)
 						{
 						if(t==0){
 						 w_id=w_id_split[i];
 						 t=1;
 						}else{
 	 						 w_id=w_id+","+w_id_split[i];
 						}
 						}
 					}
	    		 $("#whole_med_id").val(w_id);

	    	 }else{
	    		 
	    		 if(whole_med_id==id)
	    			 {
		    		 $("#whole_med_id").val('');
	    			 }
	    		 
	    	 }
	    	 /******* End Remove Colleect Id ***********/

    	 $("#med_but_"+id).css("background-color","white");
    	 $("#med_id_"+id).val(0);
         $("#add_med_"+id).remove();
         
    	 }
	     
    }
    
    
    
    function row_ser_inj_func(val){
	     var id=val;
	     var inj_id=$("#inj_id_"+id).val();
	     var inj_name=$("#inj_name_"+id).val();
   	 var whole_inj_id=$("#whole_inj_id").val();

	     
	     if(inj_id=='0')
	    	 {
	    	 /******* Colleect Id ***********/
	    	 if(whole_inj_id.trim()=='')
	    		 {
	    		 $("#whole_inj_id").val(id);
	    		 }else{
		    		 var w_id=whole_inj_id+","+id;
		    		 $("#whole_inj_id").val(w_id);
	    		 }
	    	 /******* End Colleect Id ***********/

	    	 $("#inj_but_"+id).css("background-color","#337ab7");
	    	 $("#inj_id_"+id).val(1);
	    	 
	    	 var mark='<div class="col-md-6" id="add_inj_'+id+'" style="background-color:grey;border:2px solid white">'+inj_name+
	    	          '</div>';
	    	          $("#ser_inj_add").append(mark);
	    	          
	    	 }
	     if(inj_id=='1')
   	 {
	    	 /******* Remove Colleect Id ***********/
	    	 if(whole_inj_id.includes(',')){
	    		 
				var w_id_split=whole_inj_id.split(",");
				
				var i=0;
				var w_id='';
				var t=0;
				for(i=0;i<w_id_split.length;i++)
					{
					if(w_id_split[i]!=id)
						{
						if(t==0){
						 w_id=w_id_split[i];
						 t=1;
						}else{
	 						 w_id=w_id+","+w_id_split[i];
						}
						}
					}
	    		 $("#whole_inj_id").val(w_id);

	    	 }else{
	    		 
	    		 if(whole_inj_id==id)
	    			 {
		    		 $("#whole_inj_id").val('');
	    			 }
	    		 
	    	 }
	    	 /******* End Remove Colleect Id ***********/

   	 $("#inj_but_"+id).css("background-color","white");
   	 $("#inj_id_"+id).val(0);
        $("#add_inj_"+id).remove();
        
   	 }
	     
   }
    
    function add_medicine(){
    	
    var Sr_No=$("#medicine_table tbody tr").length+1;
    
    var whole_med_id=$("#whole_med_id").val();
		 
		 if(whole_med_id!='')
			 {
			/******* Remove Collect Id ***********/
   	 if(whole_med_id.includes(',')){
   		 
			var w_id_split=whole_med_id.split(",");
			var i=0;
			for(i=0;i<w_id_split.length;i++)
				{
				var add_med=$("#add_med_"+w_id_split[i]).text();
				//alert(add_med);
				
				
			var mark="<tr id='row_"+w_id_split[i]+"'><input type='hidden' name='m_id' value='"+w_id_split[i]+"'>"+
	         "<td >"+Sr_No+"</td>"+
	         "<td><input type='text' value='"+add_med+"' name='m_name' class='form-control color-border' readonly='readonly' style='color:black'></td>"+
	         "<td>"+
	         "<select class='form-control color-border' name='mae'>"+
	         "<option>0-0-0</option>"+
	         "<option>1-0-0</option>"+
	         "<option>0-1-0</option>"+
	         "<option>1-1-0</option>"+
	         "<option>0-0-1</option>"+
	         "<option>1-0-1</option>"+
	         "<option>0-1-1</option>"+
	         "<option>1-1-1</option>"+
	         "</select>"+
	         "</td>"+
	         "<td><input type='number' name='dose' class='form-control color-border'></td>"+
	         "<td><input type='number' name='duration' class='form-control color-border'></td>"+
	         "<td>"+
	         "<button type='button'  class='btn btn-default btn-sm' onclick='med_del("+w_id_split[i]+")'>"+
	         "<span class='glyphicon glyphicon-remove'></span> Remove"+ 
	         "</button></td>"+
	         "</tr>";
	         
	         $("#medicine_table tbody").append(mark);
	         Sr_No++;
	         
	         
				}
			
		}else{
				var add_med=$("#add_med_"+whole_med_id).text();
				//alert(add_med);
				
				
			var mark="<tr id='row_"+whole_med_id+"'><input type='hidden' name='m_id' value='"+whole_med_id+"'>"+
	         "<td >"+Sr_No+"</td>"+
	         "<td><input type='text' name='m_name' value="+add_med+" class='form-control color-border' readonly='readonly' style='color:black'></td>"+
	         "<td>"+
	         "<select class='form-control color-border' name='mae'>"+
	         "<option>0-0-0</option>"+
	         "<option>1-0-0</option>"+
	         "<option>0-1-0</option>"+
	         "<option>1-1-0</option>"+
	         "<option>0-0-1</option>"+
	         "<option>1-0-1</option>"+
	         "<option>0-1-1</option>"+
	         "<option>1-1-1</option>"+
	         "</select>"+
	         "</td>"+
	         "<td><input type='number' name='dose' class='form-control color-border'></td>"+
	         "<td><input type='number' name='duration' class='form-control color-border'></td>"+
	         "<td>"+
	         "<button type='button'  class='btn btn-default btn-sm' onclick='med_del("+whole_med_id+")'>"+
	         "<span class='glyphicon glyphicon-remove'></span> Remove"+ 
	         "</button></td>"+
	         "</tr>";
	         
	         $("#medicine_table tbody").append(mark);
	         Sr_No++;		
			}
		}
		 
		 
    
		 
		 if(whole_med_id!='')
		 {
		/******* Remove Collect Id ***********/
	 if(whole_med_id.includes(',')){
		 
		var w_id_split=whole_med_id.split(",");
		var i=0;
		for(i=0;i<w_id_split.length;i++)
			{
			 $("#med_but_"+w_id_split[i]).css("background-color","white");
	    	 $("#med_id_"+w_id_split[i]).val(0);
	         $("#add_med_"+w_id_split[i]).remove();
			}
	 }
		
	}else{
		$("#med_but_"+whole_med_id).css("background-color","white");
   	 $("#med_id_"+whole_med_id).val(0);
        $("#add_med_"+whole_med_id).remove();
	}
      $("#whole_med_id").val(""); 
      $("#searchMedicineModal").modal('hide');
    }
    
    
    
    
    function add_injection(){
    	
        var Sr_No=$("#injection_table tbody tr").length+1;
        
        var whole_inj_id=$("#whole_inj_id").val();
    		 
    		 if(whole_inj_id!='')
    			 {
    			/******* Remove Collect Id ***********/
       	 if(whole_inj_id.includes(',')){
       		 
    			var w_id_split=whole_inj_id.split(",");
    			var i=0;
    			for(i=0;i<w_id_split.length;i++)
    				{
    				var add_inj=$("#add_inj_"+w_id_split[i]).text();
    				//alert(add_med);
    				
    				
    			var mark="<tr id='irow_"+w_id_split[i]+"'><input type='hidden' name='i_id' value='"+w_id_split[i]+"'>"+
    	         "<td >"+Sr_No+"</td>"+
    	         "<td><input type='text' value='"+add_inj+"' name='i_name' class='form-control color-border' readonly='readonly' style='color:black'></td>"+
    	         "<td>"+
    	         "<select class='form-control color-border' name='imae'>"+
    	         "<option>0-0-0</option>"+
    	         "<option>1-0-0</option>"+
    	         "<option>0-1-0</option>"+
    	         "<option>1-1-0</option>"+
    	         "<option>0-0-1</option>"+
    	         "<option>1-0-1</option>"+
    	         "<option>0-1-1</option>"+
    	         "<option>1-1-1</option>"+
    	         "</select>"+
    	         "</td>"+
    	         "<td><input type='number' name='idose' class='form-control color-border'></td>"+
    	         "<td><input type='number' name='iduration' class='form-control color-border'></td>"+
    	         "<td>"+
    	         "<button type='button'  class='btn btn-default btn-sm' onclick='inj_del("+w_id_split[i]+")'>"+
    	         "<span class='glyphicon glyphicon-remove'></span> Remove"+ 
    	         "</button></td>"+
    	         "</tr>";
    	         
    	         $("#injection_table tbody").append(mark);
    	         Sr_No++;
    	         
    	         
    				}
    			
    		}else{
    				var add_inj=$("#add_inj_"+whole_inj_id).text();
    				//alert(add_med);
    				
    				
    			var mark="<tr id='irow_"+whole_inj_id+"'><input type='hidden' name='i_id' value='"+whole_inj_id+"'>"+
    	         "<td >"+Sr_No+"</td>"+
    	         "<td><input type='text' name='i_name' value="+add_inj+" class='form-control color-border' readonly='readonly' style='color:black'></td>"+
    	         "<td>"+
    	         "<select class='form-control color-border' name='imae'>"+
    	         "<option>0-0-0</option>"+
    	         "<option>1-0-0</option>"+
    	         "<option>0-1-0</option>"+
    	         "<option>1-1-0</option>"+
    	         "<option>0-0-1</option>"+
    	         "<option>1-0-1</option>"+
    	         "<option>0-1-1</option>"+
    	         "<option>1-1-1</option>"+
    	         "</select>"+
    	         "</td>"+
    	         "<td><input type='number' name='idose' class='form-control color-border'></td>"+
    	         "<td><input type='number' name='iduration' class='form-control color-border'></td>"+
    	         "<td>"+
    	         "<button type='button'  class='btn btn-default btn-sm' onclick='med_del("+whole_inj_id+")'>"+
    	         "<span class='glyphicon glyphicon-remove'></span> Remove"+ 
    	         "</button></td>"+
    	         "</tr>";
    	         
    	         $("#injection_table tbody").append(mark);
    	         Sr_No++;		
    			}
    		}
    		 
    		 
        
    		 
    		 if(whole_inj_id!='')
    		 {
    		/******* Remove Collect Id ***********/
    	 if(whole_inj_id.includes(',')){
    		 
    		var w_id_split=whole_inj_id.split(",");
    		var i=0;
    		for(i=0;i<w_id_split.length;i++)
    			{
    			 $("#inj_but_"+w_id_split[i]).css("background-color","white");
    	    	 $("#inj_id_"+w_id_split[i]).val(0);
    	         $("#add_inj_"+w_id_split[i]).remove();
    			}
    	 }
    		
    	}else{
    		$("#inj_but_"+whole_med_id).css("background-color","white");
       	 $("#inj_id_"+whole_med_id).val(0);
            $("#add_inj_"+whole_med_id).remove();
    	}
          $("#whole_inj_id").val(""); 
          $("#searchInjectionModal").modal('hide');
        }
        
    
    
    
    function med_del(val)
    {
    	var id=val;
    	//alert(id);
    	$("#row_"+id).remove();
    	renumberRows1()
    }
   
    function renumberRows1() {
        $('#medicine_table tr').each(function(index, el){
            $(this).children('td').first().text((index++)-1);
        });
    }
    
    
    function inj_del(val)
    {
    	var id=val;
    	//alert(id);
    	$("#irow_"+id).remove();
    	renumberRows2()
    }
   
    function renumberRows2() {
        $('#injection_table tr').each(function(index, el){
            $(this).children('td').first().text((index++)-1);
        });
    }

    </script>
    
    <style type="text/css">
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>

<%
request.getSession().setAttribute("opd_ses", "view_appoint_ses");
%>
    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <jsp:include page="../public/topmenu.jsp"></jsp:include>
            <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>
            <jsp:include page="subheader_opd.jsp"></jsp:include>
        </div>
        
        
        
           <%
           String d_id=request.getParameter("d_id");
           //System.out.println("d_id====="+d_id);
           %>                           
                                            
        
        
        
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-3 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Doctor View :</h4>
                            </div>
                            
                     <form action="insert_outpatient_appointment" method="post" >
                     <input type='hidden' name='p_id' value=<%=d_id%>>
                     
                            <table class="table table-bordered table-padding">
   							 <thead class="table-padding">
                                <tr class="table-padding">
                                    <th colspan="2" class="table-padding">Patient Name :</th>
                                    <th colspan="1" class="table-padding">Mobile No :</th>
                                    <th colspan="1" class="table-padding">Age :</th>
                                    <th colspan="1" class="table-padding">Weight :</th>
                                    <th colspan="1" class="table-padding">Staff Name :</th>
                                    <!-- <th>Sugar</th>
                                    <th>Blood_Pressure</th>
                                    <th>LMP</th>
                                    <th>Diagnosed</th>
                                    <th>Obstestric_History</th>
                                    <th>Other_Input</th>
                                    <th>Date</th> -->
                                </tr>
                                </thead>
                                <tbody class="table-padding">
                               <%
                              
                               
                                try {
                                	String sql="";
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                         			sql="Select * from patient_appointment where id='"+d_id+"'";

                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr class="table-padding">
                        				 <td colspan="2"  class="table-padding"><input type="text" name="patient_name" id="patient_name" value="<%=rs.getString("patient_name")%>"  class="form-control color-input" placeholder="Patient Name" required="required" autocomplete="off" readonly="readonly"></td>
                        				 <td colspan="1" class="table-padding" style="width: 15%"><input type="text" name="mobile_no" id="mobile_no" value="<%=rs.getString("mobile_no")%>"  class="form-control color-input" placeholder="Patient Name" required="required" autocomplete="off" readonly="readonly"></td>
                        				 <td colspan="1" class="table-padding" style="width: 15%"><input type="text" name="age" id="age" value="<%=rs.getString("age")%>"  class="form-control color-input" placeholder="Patient Name" required="required" autocomplete="off" readonly="readonly"></td>
                        				 <td colspan="1" class="table-padding" style="width: 15%"><input type="text" name="weight" id="weight"   class="form-control color-border" placeholder="Weight" autocomplete="off" ></td>
                        				 <td colspan="1" class="table-padding"><input type="text" name="staff_name" id="staff_name" value="<%=rs.getString("staff_name")%>"  class="form-control color-input" placeholder="Patient Name" required="required" autocomplete="off" readonly="readonly"></td>
                        				
                        				</tr>
                        	
                        			
                        		 </tbody>
                        		 
                        		<thead class="table-padding">
                                <tr class="table-padding">
                                    <th colspan="1" class="table-padding">Sugar :</th>
                                    <th colspan="1" class="table-padding">Blood Pressure :</th>
                                    <th colspan="1" class="table-padding">LMP :</th>
                                    <th colspan="1" class="table-padding">Diagnosed :</th>
                                    <th colspan="1" class="table-padding">Obstestric History :</th>
                                    <th colspan="1" class="table-padding">Other Input :</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="table-padding">
                        				 <td colspan="1" class="table-padding" ><input type="text" name="sugar" id="sugar" value="<%=rs.getString("sugar")%>"  class="form-control color-border" placeholder="Sugar" autocomplete="off" ></td>
                        				 <td colspan="1" class="table-padding" ><input type="text" name="blood_pressure" id="blood_pressure" value="<%=rs.getString("blood_pressure")%>"  class="form-control color-border" placeholder="Blood Pressure" autocomplete="off" ></td>
                        				 <td colspan="1" class="table-padding" ><input type="text" name="lmp" id="lmp" value="<%=rs.getString("lmp")%>" class="form-control color-border" placeholder="LMP" autocomplete="off" ></td>
                        				 <td colspan="1" class="table-padding" ><input type="text" name="diagnosed" id="diagnosed" value="<%=rs.getString("diagnosed")%>"  class="form-control color-border" placeholder="Diagnosed" autocomplete="off" ></td>
                        				 <td colspan="1" class="table-padding" ><input type="text" name="obstestric_history" id="obstestric_history" value="<%=rs.getString("obstestric_history")%>" class="form-control color-border" placeholder="Obstestric History" autocomplete="off" ></td>
                        				 <td colspan="1" class="table-padding" ><input type="text" name="other_input" id="other_input" value="<%=rs.getString("other_input")%>"  class="form-control color-border" placeholder="Other Input" autocomplete="off" ></td>
                        				
                        		</tr>
                                </tbody>
                                
                                
                            
                            
                            <%}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 
                        		 
                        		 <thead class="table-padding">
                                <tr class="table-padding">
                                    <th colspan="1" class="table-padding">Consultation Fees :</th>
                                    <th colspan="1" class="table-padding">Other Charges:</th>
                                    <th colspan="2" class="table-padding">Disease:</th>
                                    <th colspan="2" class="table-padding">Fever:</th>
                                    
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="table-padding">
                        				 <td colspan="1" class="table-padding" ><input type="text" name="consultation_fees" id="consultation_fees"   class="form-control color-border" placeholder="Consultation Fees" autocomplete="off" ></td>
                        				 <td colspan="1" class="table-padding" ><input type="text" name="other_charges" id="other_charges"   class="form-control color-border" placeholder="Other Charges" autocomplete="off" ></td>
                        				 <td colspan="2" class="table-padding" ><input type="text" name="disease" id="disaese"   class="form-control color-border" placeholder="Disease" autocomplete="off" ></td>
                        			     <td colspan="2" class="table-padding" ><input type="text" name="fever" id="fever"   class="form-control color-border" placeholder="Fever" autocomplete="off" ></td>
                        				
                        		</tr>
                                </tbody>
                        		 <thead class="table-padding">
                                <tr class="table-padding">
                                    <th colspan="3" class="table-padding">Test:&nbsp;<input type="button" value="+" style="color: black;" onclick="search_test_funct()"></th>
                                    <th colspan="3" class="table-padding">Regular Medicine:</th>
                                    
                                    
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="table-padding">
                        				 <td colspan="3" class="table-padding" ><input type="text" name="test" id="consultation_fees"   class="form-control color-border" placeholder="Test" autocomplete="off" ></td>
                        				 <td colspan="3" class="table-padding" ><input type="text" name="regular_medicine" id="other_charges"   class="form-control color-border" placeholder="Regular Medicine" autocomplete="off" ></td>
                        				 
                        		</tr>
                                </tbody>
                        		 </table>
                        		 <!--@@@@@@@@@@@@@@@ Search Medicine @@@@@@@@@@@@@@-->
                        		 <table class="table table-bordered table-padding" id="medicine_table">
   							 		<thead class="table-padding">
                                		<tr class="table-padding">
                                    	<th colspan="6" class="table-padding">Search Medicine : <input type="button" value="+" style="color: black;" onclick="search_medicine_funct()"></th>
                                		</tr>
                                	</thead>
                                	
                               	    <thead class="table-padding">
                                		<tr>
                                		<th width="1%">Sr.no</th>
                                		<th width="30%">Medicine Name</th>
                               		    <th width="15%">Meal time(M-A-E)</th>
                                		<th width="10%">Dose</th>
                                		<th width="10%">Duration</th>
                                		<th width="8%">Cancel</th>
                                		</tr>
                                    </thead>
                                	<tbody>
                                	</tbody>
                                </table>
                        		<!--@@@@@@@@@@@@@@@ End Search Medicine @@@@@@@@@@@@@@-->
                        		
                        		 <!--@@@@@@@@@@@@@@@ Search Injection @@@@@@@@@@@@@@-->
                        		 <table class="table table-bordered table-padding" id="injection_table">
   							 		<thead class="table-padding">
                                		<tr class="table-padding">
                                    	<th colspan="6" class="table-padding">Search Injection : <input type="button" value="+" style="color: black;" onclick="search_injection_funct()"></th>
                                		</tr>
                                	</thead>
                                	
                               	    <thead class="table-padding">
                                		<tr>
                                		<th width="1%">Sr.no</th>
                                		<th width="30%">Injection Name</th>
                               		    <th width="15%">Injection time(M-A-E)</th>
                                		<th width="10%">Dose</th>
                                		<th width="10%">Duration</th>
                                		<th width="8%">Cancel</th>
                                		</tr>
                                    </thead>
                                	<tbody>
                                	</tbody>
                                </table>
                        		<!--@@@@@@@@@@@@@@@ End Search Medicine @@@@@@@@@@@@@@-->
                        		 
                        		 <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Save</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                        		 
                        		</form> 
                           <!--  <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div> -->
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      
      <!--Deleted Entries application  -->
      <div class="product-status mg-b-30" id="show_table_deleted_entries">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Deleted Entries</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			
                           			 sql="Select * from master_staff_delete_entries ";

                        			
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr>
                        				 <td><%=i%></td>
                        				 <td><%=rs.getString("staff_name")%></td>
                        				 <td><%=rs.getString("staff_id")%></td>
                        				 <td><%=rs.getString("dob")%></td>
                        				 <td><%=rs.getString("age")%></td>
                        				 <td><%=rs.getString("address")%></td>
                        				 <td><%=rs.getString("state")%></td>
                        				 <td><%=rs.getString("email_id")%></td>
                        				 <td><%=rs.getString("mobile_no")%></td>
                        				 <td><%=rs.getString("blood_group")%></td>
                        				 <td><%=rs.getString("qualification")%></td>
                        				 <td><%=rs.getString("designation")%></td>
                        				 <td><%=rs.getString("department")%></td>
                        				 <td><%=rs.getString("date")%></td>
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
            
  
 
  
  
   
  
  
  
  <!--@@@@@@@@@@@@@@ Search Medicine  @@@@@@@@@@@@@@@-->
  
  <!-- Modal -->
  <div class="modal fade" id="searchMedicineModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="mySmallModalLabel">Medicine</h4>
        </div>
        <div class="modal-body">
        <div class="row">
                        <div class="input-mark-inner">
                               <input  type="text" name="ser_medcine_name" id="ser_medcine_name" class="form-control" placeholder="Medicine Name"  autocomplete="off" onkeyup="medicine_modal()">
                         </div>
        </div>
        <br>
    <table class="table table-bordered" id="searchmedcineID">
    <tbody></tbody>
       <!--  <tr><td style="padding:2px"><input type="button" value=" " style="border-radius: 20px">&nbsp;&nbsp;abc</td></tr>
        <tr><td style="padding:2px"><input type="button" value=" " style="border-radius: 20px">&nbsp;&nbsp;abc</td></tr>
   -->  
   </table>       
      </div>
        
        <hr>
        <div class="row">
        
        <div class="col-md-12" id="ser_med_add">
        </div>
        
        </div>
        
        <input type="hidden" id="whole_med_id">
       <!--<table class="table table-bordered" >
    	   <tbody>
    	   </tbody>
   		   </table>-->
   		
   		
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="add_medicine()">Add Medicine</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!--@@@@@@@@@@@@@@ End Search Medicine @@@@@@@@@@@@@@@@@@-->




  
  
  <!--@@@@@@@@@@@@@@ Search Medicine  @@@@@@@@@@@@@@@-->
  
  <!-- Modal -->
  <div class="modal fade" id="searchInjectionModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="mySmallModalLabel">Injection</h4>
        </div>
        <div class="modal-body">
        <div class="row">
                        <div class="input-mark-inner">
                               <input  type="text" name="ser_medcine_name" id="ser_injection_name" class="form-control" placeholder="Injection Name"  autocomplete="off" onkeyup="injection_modal()">
                         </div>
        </div>
        <br>
    <table class="table table-bordered" id="searchinjectionID">
    <tbody></tbody>
       <!--  <tr><td style="padding:2px"><input type="button" value=" " style="border-radius: 20px">&nbsp;&nbsp;abc</td></tr>
        <tr><td style="padding:2px"><input type="button" value=" " style="border-radius: 20px">&nbsp;&nbsp;abc</td></tr>
   -->  
   </table>       
      </div>
        
        <hr>
        <div class="row">
        
        <div class="col-md-12" id="ser_inj_add">
        </div>
        
        </div>
        
        <input type="hidden" id="whole_inj_id">
       <!--<table class="table table-bordered" >
    	   <tbody>
    	   </tbody>
   		   </table>-->
   		
   		
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="add_injection()">Add Medicine</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!--@@@@@@@@@@@@@@ End Search Medicine @@@@@@@@@@@@@@@@@@-->
 
</div>

    

    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
</body>

</html>