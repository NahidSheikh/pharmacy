
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="generatedclasses.ConnectionClass"%>
<html>
	<head>
		<meta charset="utf-8">
		<title>Invoice</title>
		<link rel="stylesheet" href="pres_css/style.css">
		<link rel="license" href="https://www.opensource.org/licenses/mit-license/">
		<script src="pres_css/script.js"></script>
		
	</head>
	<body>
		<header>
			<h1>Invoice</h1>
			<address contenteditable>
				<img alt="" src="img/favicon.ico">
			</address>
			<address contenteditable>
			<%
                              
                               
                                try {
                                	String sql="";
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                         			sql="Select * from master_hospital Limit 1";

                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        			<p><%=rs.getString("hospital_name")%></p>
                        			<p><%=rs.getString("hospital_address")%>, <%=rs.getString("state")%></p>
                        			<p>Mb No: +91 <%=rs.getString("mobile_no_first")%> , +91 <%=rs.getString("mobile_no_second")%></p>
                        			<p>Email-Id:- <%=rs.getString("email_first")%> , +91 <%=rs.getString("mobile_no_second")%></p>
                        			<p>Registration No:- <%=rs.getString("registration_no")%></p>
                        			
                        			
                        			<%}
                                }catch(Exception e){
                                	e.printStackTrace();
                                }
			
			%>
				
			</address>
			
		</header>
											<hr>
		
		<article>
			<h1>Recipient</h1>
			<!-- <address contenteditable>
				<p>Some Company<br>c/o Some Guy</p>
			</address> -->
			<table >
				<tr>
					<td colspan="1" style="border: none;"><span><b>Patient name</b></span></td>
					<td colspan="5" style="border: none;"><span>101138</span></td>
				</tr>
				<tr>
					<th colspan="1"><span>Mobile No</span></th>
					<td colspan="1"><span>+91 1011380000</span></td>
					<th colspan="1"><span>Age</span></th>
					<td colspan="1"><span>+91 </span></td>
					<th colspan="1"><span>Weight</span></th>
					<td colspan="1"><span>+91 1011380000</span></td>
				</tr>
				<tr>
					<th colspan="1"><span>Appointment Date</span></th>
					<td colspan="1"><span>+91 1011380000</span></td>
					<th colspan="1"><span>Doctor Name</span></th>
					<td colspan="3"><span> </span></td>
					
				</tr>
			</table>
			<table class="inventory">
				<thead>
					<tr>
						<th><span contenteditable>Item</span></th>
						<th><span contenteditable>Description</span></th>
						<th><span contenteditable>Rate</span></th>
						<th><span contenteditable>Quantity</span></th>
						<th><span contenteditable>Price</span></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><a class="cut">-</a><span contenteditable>Front End Consultation</span></td>
						<td><span contenteditable>Experience Review</span></td>
						<td><span data-prefix>$</span><span contenteditable>150.00</span></td>
						<td><span contenteditable>4</span></td>
						<td><span data-prefix>$</span><span>600.00</span></td>
					</tr>
				</tbody>
			</table>
			<a class="add">+</a>
			<table class="balance">
				<tr>
					<th><span contenteditable>Total</span></th>
					<td><span data-prefix>$</span><span>600.00</span></td>
				</tr>
				<tr>
					<th><span contenteditable>Amount Paid</span></th>
					<td><span data-prefix>$</span><span contenteditable>0.00</span></td>
				</tr>
				<tr>
					<th><span contenteditable>Balance Due</span></th>
					<td><span data-prefix>$</span><span>600.00</span></td>
				</tr>
			</table>
		</article>
		<!-- <aside>
			<h1><span contenteditable>Additional Notes</span></h1>
			<div contenteditable>
				<p>A finance charge of 1.5% will be made on unpaid balances after 30 days.</p>
			</div>
		</aside> -->
	</body>
	
	<script type="text/javascript">
	var x=memo();
	function memo()
	{
		//alert("hiiii");
		//window.print();
	}
	</script>
</html>