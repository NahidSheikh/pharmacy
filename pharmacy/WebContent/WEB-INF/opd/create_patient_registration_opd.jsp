<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hospital Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
		$("#submit_form").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#show_table_deleted_entries").hide();
		$("#updatestaffmodal").hide();

    	
    });
    
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
    
    function deletefunc(val){
    	var id=val;
    	var staff_name=$("#staff_name_"+id).text();   
    	var dob=$("#dob_"+id).text();
    	var age=$("#age_"+id).text();
    	var address=$("#address_"+id).text();
    	var state=$("#state_"+id).text();
    	var mobile_no=$("#mobile_no_"+id).text();
    	var blood_group=$("#blood_group_"+id).text();
    	var gender=$("#gender_"+id).text();
    	var marital_status=$("#marital_status_"+id).text();
    	var staff_id=$("#staff_id_"+id).text();

    	
    	//$("#male_u").attr('checked');
    	if(gender=='Male')
    		{
        	$("#male_u" ).attr( 'checked', 'checked' )
    		}
    	if(gender=='Female')
		{
    	$("#female_u" ).attr( 'checked', 'checked' )
		}
    	if(gender=='Transgender')
		{
    	$("#transgender_u" ).attr( 'checked', 'checked' )
		}
    	//$("#female_u" ).attr( 'checked', 'checked' )
    	//$("#transgender_u" ).attr( 'checked', 'checked' )

		if(marital_status=='Married')
    		{
        	$("#married_u" ).attr( 'checked', 'checked' )
    		}
		if(marital_status=='Unmarried')
		{
    	$("#unmarried_u" ).attr( 'checked', 'checked' )
		}


    	
    	$("#staff_name_u").val(staff_name);
    	$("#dob_u").val(dob);
    	$("#age_u").val(age);
    	$("#address_u").val(address);
    	$("#state_u").val(state);
    	$("#mobile_no_u").val(mobile_no);
    	$("#blood_group_u").val(blood_group);
    	$("#s_id_del").val(id);
    	$("#s_id_his").val(id);
    	$("#staff_name_del").text(staff_name);
    	$("#s_id_u").val(id);
    	$("#patient_name_app").val(staff_name);
    	$("#mobile_no_app").val(mobile_no);
    	$("#s_id_app").val(id);
    	$("#patient_id_app").val(staff_id);

    	
    	$("#myModaldelete").modal();
    }
    
    function update_staff_master()
    {
    	var staff_name=$("#staff_name_u").val();
    	var dob=$("#dob_u").val();
    	var age=$("#age_u").val();
    	var address=$("#address_u").val();
    	var mobile_no=$("#mobile_no_u").val();
        var gender_u = $("input[name='gender_u']:checked").val();
        var marital_status_u=$("input[name='marital_status_u']:checked").val();
    	var blood_group=$("#blood_group_u").val();
    	var id=$("#s_id_u").val();
    	
    	 $.ajax({
    		  type: "GET",
    		  url: "PatientRegistrationServlet",
    		  data: {staff_name:staff_name, dob: dob, age:age, address:address,
    			     mobile_no:mobile_no,gender_u:gender_u,marital_status_u:marital_status_u,
    			     blood_group:blood_group, id:id  },
    		       success: function(data){
    			   alert("Update Successfully!");
    			    $("#staff_name_"+id).text(staff_name);   
    		        $("#dob_"+id).text(dob);
    		        $("#age_"+id).text(age);
    		        $("#address_"+id).text(address);
    		    	$("#mobile_no_"+id).text(mobile_no);
    		    	$("#gender_"+id).text(gender_u);
    		    	$("#blood_group_"+id).text(blood_group);
                    $("#marital_status_"+id).text(marital_status_u);    		    
              } 
    		}); 

    }
    
    function patient_appointment_func() {
    	
		var patient_name_app=$("#patient_name_app").val();
		var mobile_no_app=$("#mobile_no_app").val();
		var department_app=$("#department_app").val();
		var staff_name_app=$("#staff_name_app").val();
		var appoint_date_app=$("#appoint_date_app").val();
		var time_slot_app=$("#time_slot_app").val();
		var sugar_app=$("#sugar_app").val();
		var blood_pressure_app=$("#blood_pressure_app").val();
		var lmp_app=$("#lmp_app").val();
		var diagnosd_app=$("#diagnosd_app").val();
		var obstestric_app=$("#obstestric_app").val();
		var other_input_app=$("#other_input_app").val();
		var s_id_app=$("#s_id_app").val();
		var patient_id_app=$("#patient_id_app").val();
		
		
		$.ajax({
  		  type: "GET",
  		  url: "PatientAppointmentServlet",
  		  data: {
  			     patient_name_app:patient_name_app, mobile_no_app: mobile_no_app, department_app:department_app, 
  			     staff_name_app:staff_name_app,appoint_date_app:appoint_date_app,time_slot_app:time_slot_app,
  			     sugar_app:sugar_app,blood_pressure_app:blood_pressure_app,lmp_app:lmp_app,
  			     diagnosd_app:diagnosd_app, obstestric_app:obstestric_app, other_input_app:other_input_app,
  			     s_id_app:s_id_app, patient_id_app:patient_id_app
  			     },
  		       success: function(data){
  			   alert("Appointemnt Book Successfully!");
  			 $("#sugar_app").val("");  	
  			 $("#blood_pressure_app").val("");
  			 $("#lmp_app").val("");
  			 $("#diagnosd_app").val("");
  			 $("#obstestric_app").val("");
  			 $("#other_input_app").val("");
            } 
  		});
	}
    
    
    
    function form_change(val){
    	if(val==0)
    		{
    		$("#submit_form").show();
    		$("#show_table").hide();
    		
    		$("#show_table_deleted_entries").hide();

    		}
    	
    	if(val==1)
		{
		$("#submit_form").hide();
		$("#show_table").show();
		$("#show_table_deleted_entries").hide();

		}
    	if(val==2)
		{
		$("#submit_form").hide();
		$("#show_table").hide();
		$("#show_table_deleted_entries").show();

		}
    }
    
    function form_change_modal(val){
    	if(val==1)
    		{
    		$("#updatestaffmodal").show();
    		$("#deletestaffmodal").hide();
    		$("#historystaffmodal").hide();
    		$("#patientappointmentmodal").hide();

    		}
    	
    	if(val==0)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").show();
		$("#historystaffmodal").hide();
		$("#patientappointmentmodal").hide();

		}
    	
    	if(val==3)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#patientappointmentmodal").show();

		}
    	if(val==2)
		{
    	$("#historystaffmodal").show();
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		$("#patientappointmentmodal").hide();

		var s_id_his=$("#s_id_his").val();
		$.ajax({
  		  type: "POST",
  		  url: "StaffServlet",
  		  data: {id:s_id_his },
  		  dataType: "json",
   		  success: function(json){
  			  alert(json);
  			                    /*   var json = JSON.parse(data);
  			                    alert(json); */
  		    	 /* var json1 = JSON.parse(json);
  		    	 $.each(json1, function(index, element) {
  		    		 var info = element.staff_name;
  		    		 alert(info);
  		    		// console.log(info);
  			   
  		    	 }); */
  		    } 
  		});
		
		//$("#tablehis").append(markup);

		}
    }
    
    function depart_doctor_func() {
		var department_app=$("#department_app").val();
		if(department_app=="")
			{
   			$("#staff_name_app").empty();

			var a="";
			var markup="<option value="+a+">Select Staff</option>";
	            $("#staff_name_app").append(markup);
			}else{
		
		$.ajax({
	  		  type: "POST",
	  		  url: "PatientRegistrationServlet",
	  		  data: {department_app:department_app},
	  		  dataType: "json",
	   		  success: function(json){
	   			$("#staff_name_app").empty();
	  		var json1 = jQuery.parseJSON(JSON.stringify(json));
	  		    	 $.each(json1, function(index, element) {
	  		    		 
	  		    		 var markup="<option value="+element.staff_name+">"+element.staff_name+"</option>";
	  		    		            $("#staff_name_app").append(markup);
	  			   
	  		    	 i++;
	  		    	 }); 
	  		    } 
	  		});
			}
	}
    
    function clear_history() {
    	$("#clearhistory").modal();
		
	}
    function page_refresh()
    {
    	window.location.href='create_patient_registration_opd';
    }
    
    </script>
    <style type="text/css">
    
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>

<%
request.getSession().setAttribute("opd_ses", "pat_reg_ses");
%>
    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <jsp:include page="../public/topmenu.jsp"></jsp:include>
            <!-- Mobile Menu start -->
             <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>
            <jsp:include page="subheader_opd.jsp"></jsp:include>
        </div>
        
         <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default">Patient Registration</button>
                                         </div>
        </div> 
        
        <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
        <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change(1)" class="cursor-class">Show Table</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(0)" class="cursor-class">Add New</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(2)" class="cursor-class">Deleted Entries</a>
                                                            </li>

                                                        </ul>
        
        </div>
        <!-- Single pro tab review Start-->
        
                                      
                                            
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            
                           
                             
                            
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit" >
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="basic-login-inner">
                                            <form action="insert_patient_registration_opd" method="post" onsubmit="validateform()">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Patient Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="patient_name"  class="form-control" placeholder="Patient Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group-inner">
                                                                 <textarea  name="address"  class="form-control form-group-inner" placeholder="Address" ></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Gender</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="radio" name="gender"   value="Male"  checked="checked"><span class="gend_color">Male</span> 
                                                                    <input type="radio" name="gender"   value="Female"><span class="gend_color">Female</span>
                                                                    <input type="radio" name="gender"   value="Transgender"><span class="gend_color">Transgender</span>
                                                                    
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Blood Group:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="blood_group" class=" form-control">
																	<option value="">Select Blood Group</option>
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from blood_group ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("blood_name")%>"><%=rs.getString("blood_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      
                                                     
                                                      
                                                       
                                                     
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Date Of Birth:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-5">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="dob"  class="form-control" placeholder="Date Of Birth"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Age:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="age"  class="form-control" placeholder="Age"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                   
                                                     
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label"> Mobile No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no" maxlength="10"  class="form-control" placeholder="Mobile No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Marital Status</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="radio" name="marital_status"   value="Married"  ><span class="gend_color">Married</span> 
                                                                    <input type="radio" name="marital_status"   value="Unmarried" checked="checked"><span class="gend_color">Unmarried</span>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                     
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Save</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-3 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Patient Registration</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Patient_Name</th>
                                    <th>Patient_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>Mobile_No</th>
                                    <th>Gender</th>
                                    <th>Blood_Group</th>
                                    <th>Marital_status</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               String patient_name="";
                               String mobile_no="";
                               if(request.getParameter("patient_name")!=null)
                               {
                            	   patient_name=request.getParameter("patient_name");
                               }
                               if(request.getParameter("mobile_no")!=null)
                               {
                            	   mobile_no=request.getParameter("mobile_no");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(patient_name.trim().equals("") && mobile_no.trim().equals(""))
                        			{
                        			 sql="Select * from patient_registration ";
                        			}else if(!patient_name.trim().equals("") && mobile_no.trim().equals(""))
                        			{
                           			 sql="Select * from patient_registration where patient_name='"+patient_name+"'";
                           			}else if(patient_name.trim().equals("") && !mobile_no.trim().equals(""))
                        			{
                              			 sql="Select * from patient_registration where mobile_no='"+mobile_no+"'";
                              		}else if(!patient_name.trim().equals("") && !mobile_no.trim().equals(""))
                        			{
                             			 sql="Select * from patient_registration where patient_name='"+patient_name+"' and mobile_no='"+mobile_no+"'";
                             		}else{
                           			 sql="Select * from patient_registration ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr onclick="deletefunc(<%=rs.getString("id")%>)">
                        				 <td><%=i%></td>
                        				 <td id="staff_name_<%=rs.getString("id")%>" style="color:yellow;cursor:pointer"><%=rs.getString("patient_name")%></td>
                        				 <td id="staff_id_<%=rs.getString("id")%>" ><%=rs.getString("patient_id")%></td>
                        				 <td id="dob_<%=rs.getString("id")%>"><%=rs.getString("dob")%></td>
                        				 <td id="age_<%=rs.getString("id")%>"><%=rs.getString("age")%></td>
                        				 <td id="address_<%=rs.getString("id")%>"><%=rs.getString("address")%></td>
                        				 <td id="mobile_no_<%=rs.getString("id")%>"><%=rs.getString("mobile_no")%></td>
                        				 <td id="gender_<%=rs.getString("id")%>"><%=rs.getString("gender")%></td>
                        				 <td id="blood_group_<%=rs.getString("id")%>"><%=rs.getString("blood_group")%></td>
                        				 <td id="marital_status_<%=rs.getString("id")%>"><%=rs.getString("marital_status")%></td>
                        				 <td id="date_<%=rs.getString("id")%>"><%=rs.getString("date")%></td>
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      
      <!--Deleted Entries application  -->
      <div class="product-status mg-b-30" id="show_table_deleted_entries">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Deleted Entries</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               if(request.getParameter("staff_name")!=null)
                               {
                            	   patient_name=request.getParameter("staff_name");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(!patient_name.trim().equals(""))
                        			{
                        			 sql="Select * from master_staff_delete_entries where staff_name='"+patient_name+"'";
                        			}else{
                           			 sql="Select * from master_staff_delete_entries ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr>
                        				 <td><%=i%></td>
                        				 <td><%=rs.getString("staff_name")%></td>
                        				 <td><%=rs.getString("staff_id")%></td>
                        				 <td><%=rs.getString("dob")%></td>
                        				 <td><%=rs.getString("age")%></td>
                        				 <td><%=rs.getString("address")%></td>
                        				 <td><%=rs.getString("state")%></td>
                        				 <td><%=rs.getString("email_id")%></td>
                        				 <td><%=rs.getString("mobile_no")%></td>
                        				 <td><%=rs.getString("blood_group")%></td>
                        				 <td><%=rs.getString("qualification")%></td>
                        				 <td><%=rs.getString("designation")%></td>
                        				 <td><%=rs.getString("department")%></td>
                        				 <td><%=rs.getString("date")%></td>
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
            
  
  <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
 					<ul class="nav nav-tabs custon-set-tab">
 					                                        <li class="active"><a data-toggle="tab" onclick="form_change_modal(3)" >Patient_appointment</a>
                                                            </li>
                                                            <li ><a data-toggle="tab" onclick="form_change_modal(1)" >Update</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(0)" style="cursor:pointer">Delete</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(2)" style="cursor:pointer">History</a>
                                                            </li>

                    </ul> 
                       
            </div>
        <div class="modal-body" style="height:350px;overflow:auto">
        
        <div id="patientappointmentmodal">
        
        <form  method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_app" id="s_id_app">
        <input type="hidden" name="patient_id_app" id="patient_id_app">
        
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Book Appointment</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Patient_Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="patient_name_app" id="patient_name_app" class="form-control" placeholder="Patient Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Department:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="department_app" id="department_app" class=" form-control" onchange="depart_doctor_func()">
 																<option value="">Select Department</option>
 																
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from master_opd_lab ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("lab_name")%>"><%=rs.getString("lab_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>																	
																</select>                                                               
																 </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Appoint_Date:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="appoint_date_app" id="appoint_date_app" class="form-control" placeholder="Appointment Date" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Sugar:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="sugar_app" id="sugar_app" class="form-control" placeholder="Sugar" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">LMP:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="lmp_app" id="lmp_app" class="form-control" placeholder="LMP" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Obstestric_History:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="obstestric_app" id="obstestric_app" class="form-control" placeholder="Obstestric History" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                         
                                                    </div>
                                                    <div class="col-lg-6">
                                                     
                                                    
                                                     
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label"> Mobile-No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no_app" id="mobile_no_app" maxlength="10"  class="form-control" placeholder="Mobile No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Select_Staff:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="staff_name_app" id="staff_name_app" class=" form-control" >
 																<option value="">Select Staff</option>
 																																	
																</select>                                                               
																 </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Time-Slot:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="time_slot_app" id="time_slot_app" class="form-control" placeholder="Time Slot" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Blood_Pressure:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="blood_pressure_app" id="blood_pressure_app" class="form-control" placeholder="Blood-Pressure" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Diagnosed:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="diagnosd_app" id="diagnosd_app" class="form-control" placeholder="Diagnosed" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Other Input:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="other_input_app" id="other_input_app" class="form-control" placeholder="Other Input" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="button" onclick="patient_appointment_func()" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Appointment</button>
<!--                                                             <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
        
        
        <div id="updatestaffmodal">
        
        <form  method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_u" id="s_id_u">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Update Patient Registration</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Patient_Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="staff_name" id="staff_name_u" class="form-control" placeholder="Staff Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group-inner">
                                                                 <textarea  name="address" id="address_u"  class="form-control form-group-inner" placeholder="Address" ></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Gender</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="radio" name="gender_u"   value="Male"  id="male_u"><span class="gend_color">Male</span> 
                                                                    <input type="radio" name="gender_u"   value="Female" id="female_u"><span class="gend_color">Female</span>
                                                                    <input type="radio" name="gender_u"   value="Transgender" id="transgender_u"><span class="gend_color">Transgender</span>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Blood_Group:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="blood_group" id="blood_group_u" class=" form-control">
																	<option value="">Select Blood Group</option>
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from blood_group ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("blood_name")%>"><%=rs.getString("blood_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                       
                                                     
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Date_Of_Birth:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-5">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="dob" id="dob_u"  class="form-control" placeholder="Date Of Birth"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Age:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="age" id="age_u"  class="form-control" placeholder="Age"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                    
                                                     
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label"> Mobile-No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no" id="mobile_no_u" maxlength="10"  class="form-control" placeholder="Mobile No(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Marital_Status</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="radio" name="marital_status_u" id="married_u"  value="Married"  ><span class="gend_color">Married</span> 
                                                                    <input type="radio" name="marital_status_u"  id="unmarried_u" value="Unmarried" ><span class="gend_color">Unmarried</span>
                                                                </div>
                                                            </div>
                                                      </div> 
                                                      
                                                      
                                                      
                                                      
                                                     
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="button" onclick="update_staff_master()" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Update</button>
<!--                                                             <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     <!--@@@@@@@@@@@@@@@ Delete Modal  @@@@@@@@@@@@@@@-->
                     <div id="deletestaffmodal">
        
        <form  action="delete_staff_master" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_del" id="s_id_del">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Delete Staff Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <br>
                                                    <p>Do you want to delete whole detail of staff <span id="staff_name_del" style="color:yellow;font-size:18px"></span>!</p>
                                                   </div> 
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Yes</button>
<!--                                                        <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     
                     <div id="historystaffmodal">
        
        <form  action="" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_his" id="s_id_his">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>History Staff Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <table id="tablehis" class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                              
                        		 </tbody>
                            </table> </div> 
                                                </div>
                                                
                                                </form>
                     </div>
                     
                     
                     
                     
           </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--           <button type="button" class="btn btn-primary">Save changes</button>
 -->        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  
  
   <!-- Modal -->
  <div class="modal fade" id="clearhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Search OPD Patient</h4>
        </div>
        <div class="modal-body">
													<div class="row">
													<div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Patient Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input list="staff-list" type="text" name="patient_name"  class="form-control" placeholder="Patient Name"  autocomplete="off">
                                                                <datalist id="staff-list">
  																<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from patient_registration group by patient_name";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("patient_name")%>"><%=rs.getString("patient_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</datalist>
                                                                
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     
                                                     <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Mobile No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input  type="number" name="mobile_no"  class="form-control" placeholder="Mobile No"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                      </div>   
                                                        
                                                        
                                   </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Search</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  
 
</div>

    

    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
</body>

</html>