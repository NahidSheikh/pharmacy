<%@page import="commoncontroller.searchclasses"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hospital Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
        
        <!--  datepicker  -->
		
	    <link rel="stylesheet" href="css2/datepicker.css">
		<script src="selectjs/datepicker.js"></script>
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
		$("#submit_form").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#show_table_deleted_entries").hide();
		$("#updatestaffmodal").hide();

    	
    });
    
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
    
    
    
    
    
    
    
    
    function form_change(val){
    	if(val==0)
    		{
    		$("#submit_form").show();
    		$("#show_table").hide();
    		
    		$("#show_table_deleted_entries").hide();

    		}
    	
    	if(val==1)
		{
		$("#submit_form").hide();
		$("#show_table").show();
		$("#show_table_deleted_entries").hide();

		}
    	if(val==2)
		{
		$("#submit_form").hide();
		$("#show_table").hide();
		$("#show_table_deleted_entries").show();

		}
    }
    
    function form_change_modal(val){
    	if(val==1)
    		{
    		$("#updatestaffmodal").show();
    		$("#deletestaffmodal").hide();
    		$("#historystaffmodal").hide();
    		$("#patientappointmentmodal").hide();

    		}
    	
    	if(val==0)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").show();
		$("#historystaffmodal").hide();
		$("#patientappointmentmodal").hide();

		}
    	
    	if(val==3)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#patientappointmentmodal").show();

		}
    	if(val==2)
		{
    	$("#historystaffmodal").show();
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		$("#patientappointmentmodal").hide();

		var s_id_his=$("#s_id_his").val();
		
		
		//$("#tablehis").append(markup);

		}
    }
    
   
    
    function clear_history() {
    	$("#clearhistory").modal();
		
	}
    function page_refresh()
    {
    	window.location.href='create_stock_info';
    }
    
    function total_stock() {
		window.location.href='create_stock_info';
	}
    
    
    
    function low_stock() {
		window.location.href='create_low_stock_info';
	}
    
    function out_of_stock() {
		window.location.href='create_out_of_stock_info';

	}
    
    </script>
    <style type="text/css">
    
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>

<%
request.getSession().setAttribute("invent_ses", "stock_info");
%>
    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <jsp:include page="../public/topmenu.jsp"></jsp:include>
            <!-- Mobile Menu start -->
             <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>
            <jsp:include page="subheader_inventory.jsp"></jsp:include>
        </div>
        
        <!--  <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default">View Appointment</button>
                                         </div>
        </div> 
        
        <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
        <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change(1)" class="cursor-class">Today's Appointment</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(0)" class="cursor-class">Tomorrow's Appointment</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(2)" class="cursor-class">Deleted Entries</a>
                                                            </li>

                                                        </ul>
        
        </div> -->
        <!-- Single pro tab review Start-->
        
                                      
                                            
       
        
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-2 col-sm-2 col-xs-12 mg-b-15">
                        
                            <h4>Inventory</h4>
                            </div>
                            
                            <div class="col-md-10 col-md-10 col-sm-10 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i>Refresh</button>
                                          </div>
                                          
                                           <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="total_stock()" >Total Stock</button>
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="low_stock()" style="background-color: #1b2a47;color: white">Low Stock</button>
                                          </div>
                                           <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="out_of_stock()">Out Of Stock</button>
                                          </div>
                                           <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="add_new()">3 Month Expire Stock</button>
                                          </div>
                                           <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="add_new()"> Expired Stock</button>
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Medicine Name</th>
                                    <th>Medicine Company</th>
                                    <th>Package</th>
                                    <th>Medicine Type</th>
                                    <th>Medicine Category</th>
                                    <th>HSN</th>
                                    <th>Low Stock</th>
                                    <th>Available Quantity</th>
                                    
                                    
                                    
                                    
                                    
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               
                              
                               String medicine_name="";
                               String batch_id="";
                               
                               String table_name="";
                               String first_column="";
                               String second_column="";
                               
                               
                               int t_quant=0;
                               int pend_quant=0;
                               int available_quant=0;
                               
                           	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

                           	String date_split[]=todaystring.split("/");
                           	String mm_c=date_split[1];
                           	String yyyy_c=date_split[2];






                               if(request.getParameter("medicine_name")!=null)
                               {
                            	   medicine_name=request.getParameter("medicine_name");
                               }
                               if(request.getParameter("batch_id")!=null)
                               {
                            	   batch_id=request.getParameter("batch_id");
                               }
                              
                               
                               table_name="master_medicine";
                               first_column="medicine_name";
                               second_column="hsn";
                              
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			sql=searchclasses.TwoFieldSearch(medicine_name, batch_id, table_name, first_column, second_column);
                        			//System.out.println("sql======="+sql);
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{
                        				 available_quant=0;
                        				try{
                        					
                        					System.out.println(rs.getString("medicine_name"));
                        					new ConnectionClass();
                                			Connection con1= ConnectionClass.getconnection();
                                			String sql1="select * from pharmacy_medicine_inward_order_list where m_name='"+rs.getString("medicine_name")+"'";
                        					PreparedStatement pst1=con1.prepareStatement(sql1);
                        					ResultSet rs1=pst1.executeQuery();
                        					while(rs1.next())
                        					{
                        						String expiry_date[]=rs1.getString("m_exp_date").split("/");
                        						String mm_e=expiry_date[0];
                        						String yyyy_e=expiry_date[1];
                        						if(Integer.parseInt(yyyy_e)>=Integer.parseInt(yyyy_c))
                        						{
                            						if(Integer.parseInt(mm_e)>=Integer.parseInt(mm_c))
                            						{
                            							available_quant=available_quant+Integer.parseInt(rs1.getString("pending_quantity"));
                            						}

                        						}

                        					}
                        					rs1.close();
                        					pst1.close();
                        					con1.close();

                        				}catch(Exception e)
                        				{
                        					e.getMessage();
                        				}
                        				
                        				
                        				
                        				
                        				
                        				/* t_quant=t_quant+Integer.parseInt(rs.getString("total_quantity"));
                        				pend_quant=pend_quant+Integer.parseInt(rs.getString("pending_quantity")); */
                        				
                        				if(available_quant<=Integer.parseInt(rs.getString("low_stock")))
                        				{
                            				pend_quant=pend_quant+available_quant;

                        			%>
                        				<tr onclick="deletefunc(<%=rs.getString("id")%>)">
                       				     <td><%=i%></td>
                        				 <td id="patient_name_<%=rs.getString("id")%>" style="color:yellow;cursor:pointer"><%=rs.getString("medicine_name")%></td>
                        				 <td id="staff_id_<%=rs.getString("id")%>" ><%=rs.getString("company_name")%></td>
                        				 <td id="age_<%=rs.getString("id")%>"><%=rs.getString("pack")%></td>
                        				 <td id="mobile_no_<%=rs.getString("id")%>"><%=rs.getString("medicine_type")%></td>
                        				 <td id="gender_<%=rs.getString("id")%>"><%=rs.getString("medicine_category")%></td>
                        				 <td id="blood_group_<%=rs.getString("id")%>"><%=rs.getString("hsn")%></td>
                        				 <td id="marital_status_<%=rs.getString("id")%>"><%=rs.getString("low_stock")%></td>

                        				 <td id="marital_status_<%=rs.getString("id")%>" style="color: red"><%=available_quant%></td>
                        				
                                        </tr>
                        	
                        			<%i++;
                        				}
                        				}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                        		 
                        		 <thead>
                                <tr>
                                    <th></th>
                                    <th>Total</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><%=pend_quant %></th>
                                    
                                    
                                    
                                    
                                    
                                    
                                </tr>
                                </thead>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      
      <!--Deleted Entries application  -->
      
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
            
  
 
  
  
   <!-- Modal -->
  <div class="modal fade" id="clearhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Search Total Stock </h4>
        </div>
        <div class="modal-body">
													<div class="row">
													<div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Medicine Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input list="staff-list" type="text" name="medicine_name"  class="form-control" placeholder="Medicine Name"  autocomplete="off">
                                                                <datalist id="staff-list">
  																<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select medicine_name from master_medicine group by medicine_name";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("medicine_name")%>"><%=rs.getString("medicine_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</datalist>
                                                                
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     
                                                     <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">HSN:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input  type="text" name="batch_id"  class="form-control" placeholder="HSN"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     
                                                     
                                                      </div>   
                                                        
                                                        
                                   </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Search</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  
 
</div>

    

    <!-- jquery
		============================================ -->
<!--     <script src="js/vendor/jquery-1.12.4.min.js"></script>
 -->    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
    
    
     <script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		/* date_input.datepicker({
			format: 'dd/mm/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		}) */
		
		var from_date=$('input[name="from_date"]');
		from_date.datepicker({
			format: 'dd/mm/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
		
		var to_date=$('input[name="to_date"]');
		to_date.datepicker({
			format: 'dd/mm/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
		
	})
	
	
</script>
</body>

</html>