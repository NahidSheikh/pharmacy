<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="generatedclasses.ConnectionClass"%>
<html>
	<head>
		<meta charset="utf-8">
		<title>Invoice</title>
		<link rel="stylesheet" href="pres_css/style.css">
		<link rel="license" href="https://www.opensource.org/licenses/mit-license/">
		<script src="pres_css/script.js"></script>
		
	</head>
	<body>
	<div style="border: 1px solid black">
		<header>
<!-- 			<h1>Invoice</h1>
 -->			<address>
				<img alt="" src="img/favicon.ico">
			</address>
			<br>
			<address>
			<%
                              
                               
                                try {
                                	String sql="";
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                         			sql="Select * from master_hospital Limit 1";

                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        			<p><%=rs.getString("hospital_name")%></p>
                        			<p><%=rs.getString("hospital_address")%>, <%=rs.getString("state")%></p>
                        			<p>Mb No: +91 <%=rs.getString("mobile_no_first")%> , +91 <%=rs.getString("mobile_no_second")%></p>
                        			<p>Email-Id:- <%=rs.getString("email_first")%> , +91 <%=rs.getString("mobile_no_second")%></p>
                        			<p>Registration No:- <%=rs.getString("registration_no")%></p>
                        			
                        			
                        			<%}
                                }catch(Exception e){
                                	e.printStackTrace();
                                }
			
			%>
				
			</address>
			
		</header>
				<article style="margin-top: -60px;border-top: 2px solid black;">
				
<%

                         String vendor_name="";
						 String vendor_id="";
						 String address="";
						 String bill_no="";
						 String sub_total="";
						 String total_tax="";
						 String net_amount="";


							String todaystring=new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime());

																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from pharmacy_purchase_order where id='3' ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{
																			vendor_name=rs.getString("vendor_name");
																			vendor_id=rs.getString("vendor_id");
																			address=rs.getString("mailing_address");
																			bill_no=rs.getString("bill_no");
																			sub_total=rs.getString("sub_total");
																			total_tax=rs.getString("total_tax");
																			net_amount=rs.getString("net_amount");
																		}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>				
				<table>
				<tr>
				<td colspan="3">Vendor Name: &nbsp;<%=vendor_name %> </td>
				<td colspan="1">Vendor Id: &nbsp;<%=vendor_id %></td>
				</tr>
				<tr>
				<td colspan="2">Address: &nbsp;<%=address %> </td>
				<td colspan="1">Bill No: &nbsp;<%=bill_no %></td>
				<td colspan="1">Date: &nbsp;<%=todaystring %></td>
				
				</tr>
				</table>
		</article>
				<article style="border-top: 2px solid black;">
	   <center><span style="font-size: 13px;text-decoration: underline;"><b>Purchase Order</b></span></center> 
			<!-- <address contenteditable>
				<p>Some Company<br>c/o Some Guy</p>
			</address> -->
			
			<table class="inventory">
			
			
				<thead>
					<tr>
						<th>Sr.No</th>
					
						<th>Medicine Name</th>
						<th>Company</th>
						<th>Package</th>
						<!-- <th><span>Medicine Type</span></th>
						<th><span>Medicine Category</span></th> -->
						<th>HSN</th>
						<th>Rate</th>
						<th>Quantity</th>
						<th>Amount</th>
						<th>CGST</th>
						<th>SGST</th>
						<th>IGST</th>
						<th>Total Amount</th>
						
						
					</tr>
				</thead>
				<tbody>
				
				<%
                            DecimalFormat df=new DecimalFormat("#.##"); 
                               int quantity=0;
                               double amount=0;
                               double cgst=0;
                               double sgst=0;
                               double igst=0;
                               double t_amount=0;

                               
                                try {
                                	int i=1; 
                                	String sql="";
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                         			sql="Select * from pharmacy_purchase_order_list where ref_id='3'";

                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{
                        				quantity=quantity+Integer.parseInt(rs.getString("m_quantity"));
                        				amount=amount+Double.parseDouble(rs.getString("m_amount"));
                        				cgst=cgst+Double.parseDouble(rs.getString("m_cgst"));
                        				sgst=sgst+Double.parseDouble(rs.getString("m_sgst"));
                        				igst=igst+Double.parseDouble(rs.getString("m_igst"));
                        				t_amount=t_amount+Double.parseDouble(rs.getString("m_total_amount"));

                        			%>
					 <tr>
					 	<td style="width: 2%"><%=i%></td>
						<td style="width: 20%"><%=rs.getString("m_name")%></td>
						<td><%=rs.getString("m_company")%></td>
						<td><%=rs.getString("m_pack")%></td>
						<td><%=rs.getString("m_hsn")%></td>
						<td><%=rs.getString("m_rate")%></td>
						
						<td><%=rs.getString("m_quantity")%></td>
						<td><%=rs.getString("m_amount")%></td>
						<td><%=rs.getString("m_cgst")%></td>
						<td><%=rs.getString("m_sgst")%></td>
						<td><%=rs.getString("m_igst")%></td>
						<td><%=rs.getString("m_total_amount")%></td>
						
					</tr> 
					
					<%i++;}
                                }catch(Exception e){
                                	e.printStackTrace();
                                }
			
			%>
				<tr>
						<td></td>
					
						<td></td>
						<td></td>
						<td></td>
						<!-- <th><span>Medicine Type</span></th>
						<th><span>Medicine Category</span></th> -->
						<td></td>
						<td></td>
						<th><b><%=quantity%></b></th>
						<th><%=df.format(amount) %></th>
						<th><%=df.format(cgst) %></th>
						<th><%=df.format(sgst) %></th>
						<th><%=df.format(igst) %></th>
						<th><%=df.format(t_amount) %></th>
						
						
					</tr>
				</tbody>
			</table>
<!-- 			<a class="add">+</a>
 -->			<table class="balance">
				<tr>
					<th><span>Sub-Total</span></th>
					<td><span><%=sub_total %></span></td>
				</tr>
				<tr>
					<th><span>Total Tax</span></th>
					<td><span><%=total_tax %></span></td>
				</tr>
				<tr>
					<th><span>Net Amount</span></th>
					<td><span><%=net_amount %></span></td>
				</tr>
			</table>
		</article>
		</div>
		<!-- <aside>
			<h1><span contenteditable>Additional Notes</span></h1>
			<div contenteditable>
				<p>A finance charge of 1.5% will be made on unpaid balances after 30 days.</p>
			</div>
		</aside> -->
	</body>
	
	<script type="text/javascript">
	var x=memo();
	function memo()
	{
		//alert("hiiii");
		//window.print();
		
		//var printContents = document.getElementById(divName).innerHTML;
	     var originalContents = document.body.innerHTML;

	     //document.body.innerHTML = printContents;

	     window.print();

	     document.body.innerHTML = originalContents;
	     
	     		//alert("nahid");
	}
	</script>
</html>