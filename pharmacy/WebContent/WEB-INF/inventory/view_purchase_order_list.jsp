<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hospital Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
		$("#submit_form").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#show_table_deleted_entries").hide();
		$("#updatestaffmodal").hide();

    	
    });
    
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
    
    function deletefunc(val){
    	var id=val;
    	window.location.href='view_purchase_order_list?d_id='+id+'';

    	
    }
    
    
    
    
    
    
    
    function form_change(val){
    	if(val==0)
    		{
    		$("#submit_form").show();
    		$("#show_table").hide();
    		
    		$("#show_table_deleted_entries").hide();

    		}
    	
    	if(val==1)
		{
		$("#submit_form").hide();
		$("#show_table").show();
		$("#show_table_deleted_entries").hide();

		}
    	if(val==2)
		{
		$("#submit_form").hide();
		$("#show_table").hide();
		$("#show_table_deleted_entries").show();

		}
    }
    
    function form_change_modal(val){
    	if(val==1)
    		{
    		$("#updatestaffmodal").show();
    		$("#deletestaffmodal").hide();
    		$("#historystaffmodal").hide();
    		$("#patientappointmentmodal").hide();

    		}
    	
    	if(val==0)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").show();
		$("#historystaffmodal").hide();
		$("#patientappointmentmodal").hide();

		}
    	
    	if(val==3)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#patientappointmentmodal").show();

		}
    	if(val==2)
		{
    	$("#historystaffmodal").show();
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		$("#patientappointmentmodal").hide();

		var s_id_his=$("#s_id_his").val();
		
		
		//$("#tablehis").append(markup);

		}
    }
    
   
    
    function clear_history() {
    	window.location.href='create_purchase_order';
		
	}
    function page_refresh()
    {
    	window.location.href='view_appointment_master_opd';
    }
    
    function add_new() {
		window.location.href='create_purchase_order_inventory';
	}
    
    </script>
    <style type="text/css">
    
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>

<%
request.getSession().setAttribute("opd_ses", "view_appoint_ses");
%>
    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <jsp:include page="../public/topmenu.jsp"></jsp:include>
            <!-- Mobile Menu start -->
             <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>
            <jsp:include page="subheader_inventory.jsp"></jsp:include>
        </div>
        
        <!--  <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default">View Appointment</button>
                                         </div>
        </div> 
        
        <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
        <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change(1)" class="cursor-class">Today's Appointment</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(0)" class="cursor-class">Tomorrow's Appointment</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(2)" class="cursor-class">Deleted Entries</a>
                                                            </li>

                                                        </ul>
        
        </div> -->
        <!-- Single pro tab review Start-->
        
                                      
                                            
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            
                           
                             
                            
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit" >
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="basic-login-inner">
                                            <form action="insert_patient_registration_opd" method="post" onsubmit="validateform()">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Patient Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="patient_name"  class="form-control" placeholder="Patient Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group-inner">
                                                                 <textarea  name="address"  class="form-control form-group-inner" placeholder="Address" ></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Gender</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="radio" name="gender"   value="Male"  checked="checked"><span class="gend_color">Male</span> 
                                                                    <input type="radio" name="gender"   value="Female"><span class="gend_color">Female</span>
                                                                    <input type="radio" name="gender"   value="Transgender"><span class="gend_color">Transgender</span>
                                                                    
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Blood Group:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="blood_group" class=" form-control">
																	<option value="">Select Blood Group</option>
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from blood_group ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("blood_name")%>"><%=rs.getString("blood_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      
                                                     
                                                      
                                                       
                                                     
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Date Of Birth:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-5">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="dob"  class="form-control" placeholder="Date Of Birth"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Age:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="age"  class="form-control" placeholder="Age"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                   
                                                     
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label"> Mobile No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no" maxlength="10"  class="form-control" placeholder="Mobile No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Marital Status</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="radio" name="marital_status"   value="Married"  ><span class="gend_color">Married</span> 
                                                                    <input type="radio" name="marital_status"   value="Unmarried" checked="checked"><span class="gend_color">Unmarried</span>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                     
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Save</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-3 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Medicine List</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Back</button>
                                            
                                          </div>
                                         <!--  <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i>Refresh</button>
                                          </div>
                                          
                                           <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="add_new()">Add New</button>
                                          </div> -->
                                          
                                    </div>
                                    
                                    
                                <form action="" method="post" >
                     
                            <table class="table table-bordered table-padding">
   							 <thead class="table-padding">
                                <tr class="table-padding">
                                    <th colspan="1" class="table-padding">Vendor Name :</th>
                                    <th colspan="1" class="table-padding">Vendor Id :</th>
                                    <th colspan="1" class="table-padding">Bill No :</th>
                                    <th colspan="1" class="table-padding">Total Tax :</th>
                                    <th colspan="1" class="table-padding">Sub-total:</th>
                                    <th colspan="1" class="table-padding">Net-Amount:</th>
                                    
                    
                                </tr>
                                </thead>
                                
                                
                                <%
                                String p_id="";
                                p_id=request.getParameter("p_id");
                                
                                try{
									new ConnectionClass();
									Connection con= ConnectionClass.getconnection();
									String sql="Select * from pharmacy_purchase_order where id='"+p_id+"' ";
									PreparedStatement pst=con.prepareStatement(sql);
									ResultSet rs=pst.executeQuery();
									if(rs.next())
									{%>
								<tbody class="table-padding">
                               
                        				<tr class="table-padding">
                        				<td colspan="1" class="table-padding" ><input type="text" name="mailing_address" value="<%=rs.getString("vendor_name") %>" id="mailing_address"   class="form-control color-input" placeholder="Mailing Address" required="required" autocomplete="off" readonly="readonly"></td>
                        				<td colspan="1" class="table-padding" ><input type="text" name="mailing_address" value="<%=rs.getString("vendor_id") %>" id="mailing_address"   class="form-control color-input" placeholder="Mailing Address" required="required" autocomplete="off" readonly="readonly"></td>
                        				<td colspan="1" class="table-padding" ><input type="text" name="mailing_address" value="<%=rs.getString("bill_no") %>" id="mailing_address"   class="form-control color-input" placeholder="Mailing Address" required="required" autocomplete="off" readonly="readonly"></td>
                        				<td colspan="1" class="table-padding" ><input type="text" name="mailing_address" value="<%=rs.getString("total_tax") %>" id="mailing_address"   class="form-control color-input" placeholder="Mailing Address" required="required" autocomplete="off" readonly="readonly"></td>
                        				<td colspan="1" class="table-padding" ><input type="text" name="mailing_address" value="<%=rs.getString("sub_total") %>" id="mailing_address"   class="form-control color-input" placeholder="Mailing Address" required="required" autocomplete="off" readonly="readonly"></td>
                        				<td colspan="1" class="table-padding" ><input type="text" name="mailing_address" value="<%=rs.getString("net_amount") %>" id="mailing_address"   class="form-control color-input" placeholder="Mailing Address" required="required" autocomplete="off" readonly="readonly"></td>
                        				
                        				</tr>
                        		 </tbody>										
									<%}
									rs.close();
									pst.close();
									con.close();
								}catch(Exception e){
									e.printStackTrace();
								}
								
								%>
                                
                        		 </table>
                        		 </form>    
                                    
                                    
                                    
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Medicine Name</th>
                                    <th>Rate</th>
                                    <th>Quantity</th>
                                    <th>Free Quantity</th>
                                    <th>Amount</th>
                                    <th>Tax Name</th>
                                    <th>Tax Amount</th>
                                    <th>CGST</th>
                                    <th>SGST</th>
                                    <th>IGST</th>
                                    <th>Total Amount</th>
                                    <th>Expiry Date</th>
                                    
                                    
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               
                              
                              
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			
                           			 sql="Select * from pharmacy_purchase_order_list where ref_id='"+p_id+"' ";

                        		
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr >
                        				 <td><%=i%></td>
                        				 <td id="patient_name_<%=rs.getString("id")%>" style="color:yellow;cursor:pointer"><%=rs.getString("m_name")%></td>
                        				 <td id="staff_id_<%=rs.getString("id")%>" ><%=rs.getString("m_rate")%></td>
                        				 <td id="dob_<%=rs.getString("id")%>"><%=rs.getString("m_quantity")%></td>
                        				 <td id="age_<%=rs.getString("id")%>"><%=rs.getString("m_free_quantity")%></td>
                        				 <td id="address_<%=rs.getString("id")%>"><%=rs.getString("m_amount")%></td>
                        				 <td id="mobile_no_<%=rs.getString("id")%>"><%=rs.getString("m_tax_name")%></td>
                        				 <td id="gender_<%=rs.getString("id")%>"><%=rs.getString("m_tax_amount")%></td>
                        				 <td id="blood_group_<%=rs.getString("id")%>"><%=rs.getString("m_cgst")%></td>
                        				 <td id="marital_status_<%=rs.getString("id")%>"><%=rs.getString("m_sgst")%></td>
                        				 <td id="marital_status_<%=rs.getString("id")%>"><%=rs.getString("m_igst")%></td>
                        				 <td id="marital_status_<%=rs.getString("id")%>"><%=rs.getString("m_total_amount")%></td>
                        				 <td id="marital_status_<%=rs.getString("id")%>"><%=rs.getString("m_exp_date")%></td>
                        				 
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      
      <!--Deleted Entries application  -->
      <div class="product-status mg-b-30" id="show_table_deleted_entries">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Deleted Entries</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                           
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
            
  
 
  
  
   <!-- Modal -->
  <div class="modal fade" id="clearhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Search Purchase Order </h4>
        </div>
        <div class="modal-body">
													<div class="row">
													<div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Vendor Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input list="staff-list" type="text" name="vendor_name"  class="form-control" placeholder="Vendor Name"  autocomplete="off">
                                                                <datalist id="staff-list">
  																<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select vendor_name from master_vendor group by vendor_name";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("vendor_name")%>"><%=rs.getString("vendor_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</datalist>
                                                                
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     
                                                     <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Bill No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input  type="text" name="bill_no"  class="form-control" placeholder="Bill No"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                      </div>   
                                                        
                                                        
                                   </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Search</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  
 
</div>

    

    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
</body>

</html>