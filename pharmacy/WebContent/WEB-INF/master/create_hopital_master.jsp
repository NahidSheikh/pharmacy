<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pharmacy Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
		$("#show_table").hide();

    	
    });
    
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
    
    function deletefunc(val){
    	var id=val;
    	$("#del_id").val(id);
    	$("#myModaldelete").modal();
    }
    
    function form_change(val){
    	if(val==0)
    		{
    		$("#submit_form").show();
    		$("#show_table").hide();
    		}
    	
    	if(val==1)
		{
		$("#submit_form").hide();
		$("#show_table").show();
		}
    }
    
    function clear_history() {
    	$("#clearhistory").modal();
		
	}
    
    </script>
    <style type="text/css">
    
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>
<%
request.getSession().setAttribute("master_ses", "Hospital_ses");
%>

    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
                  <jsp:include page="../public/topmenu.jsp"></jsp:include>
           
            <!-- Mobile Menu start -->
                  <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>
           
           <jsp:include page="subheader_master.jsp"></jsp:include>
        </div>
        
         <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default "> Pharmacy Master</button>
                                         </div>
        </div> 
        
        <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
        <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change(0)">Add Information</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(1)">Show History</a>
                                                            </li>

                                                        </ul>
        
        </div>
        <!-- Single pro tab review Start-->
        
         <%
             int count=commonclaasses.hospital_master();
                 // System.out.println("count==========="+count);
         %>
                                            
                                            
      <%if(count==0){ %>
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                              
                            <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit" >
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                            <form action="insert_hospital_master" method="post" onsubmit="validateform()">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Pharmacy Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="hospital_name"  class="form-control" placeholder="Pharmacy Name" required="required">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">State :</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
 																<select name="state" class="form-control">
																	<option>Select state</option>
																	<option>Gujarat</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Gujarat</option>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Name(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="person_name_second"  class="form-control" placeholder="Contact Person Name(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="mobile_no_second" maxlength="10" class="form-control" placeholder="Mobile No(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                     
                                                     
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="email" name="email_second"  class="form-control" placeholder="Email(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Fax No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="fax_no"  class="form-control" placeholder="Fax No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Registration:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="registration_no"  class="form-control" placeholder="Registration No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Bank Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="bank_name"  class="form-control" placeholder="Bank Name" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Account No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="account_no"  class="form-control" placeholder="Account No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Financial Year From:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="fin_year_from"  class="form-control" placeholder="Financial Year From" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                 <textarea  name="hospital_address"  class="form-control" placeholder="Address" ></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Name(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="person_name_first"  class="form-control" placeholder="Contact Person Name(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="mobile_no_first" maxlength="10"  class="form-control" placeholder="Mobile No(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="email" name="email_first"  class="form-control" placeholder="Email(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                    
                                                    
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Website:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="website"  class="form-control" placeholder="Website" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                         <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Pan No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="pan_no"  class="form-control" placeholder="Pan No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">GST No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="gst_no"  class="form-control" placeholder="GST No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">IFSC No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="ifsc_no"  class="form-control" placeholder="IFSC No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Branch Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="branch_no"  class="form-control" placeholder="Branch Name" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Financial Year To:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="fin_year_to"  class="form-control" placeholder="Financial Year To" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Save</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%}else{ %>
        
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <!-- <ul class="tab-review-design">
                                <li class="active"><a >Hospital Master</a></li>
                            </ul> -->
                            <!-- <div class="row">
                            <div class="col-md-4">
                            <ul id="myTab4" class="tab-review-design">
                                 <li ><span style="font-size: 20px">Hospital Master</span></li>
                            </ul>
                            </div>
                            <div class="col-md-8">
                            
                            <ul id="myTab4" class="tab-review-design">
                                 <li class="active" ><a href="#description">Add Info</a></li>
                                <li><a href="#reviews" > History</a></li>
                               
                            </ul>
                            </div>
                            
                            </div> -->
                           
                             
                            <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                            <form action="update_hospital_master" method="post" onsubmit="validateform()">
                                                <% 
                                                try{
													new ConnectionClass();
													Connection con= ConnectionClass.getconnection();
													String sql="select * from master_hospital";
													PreparedStatement pst=con.prepareStatement(sql);
													ResultSet rs=pst.executeQuery();
													if(rs.next())
													{
														
													
		%>
                                                
                                                <input type="hidden" name="h_id"  value="<%=rs.getString("id")%>">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Pharmacy Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="hospital_name" value="<%=rs.getString("hospital_name")%>"  class="form-control" placeholder="Hospital Name" required="required">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">State :</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
 																<select name="state" class="form-control">
 																<option value="<%=rs.getString("state")%>">Select state</option>
																	<option value="default">Select state</option>
																	<option>Gujarat</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Gujarat</option>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Name(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="person_name_second" value="<%=rs.getString("person_name_second")%>"  class="form-control" placeholder="Contact Person Name(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="mobile_no_second" value="<%=rs.getString("mobile_no_second")%>" maxlength="10" class="form-control" placeholder="Mobile No(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                     
                                                     
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="email" name="email_second" value="<%=rs.getString("email_second")%>" class="form-control" placeholder="Email(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Fax No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="fax_no" value="<%=rs.getString("fax_no")%>" class="form-control" placeholder="Fax No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Registration:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="registration_no" value="<%=rs.getString("registration_no")%>" class="form-control" placeholder="Registration No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Bank Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="bank_name" value="<%=rs.getString("bank_name")%>" class="form-control" placeholder="Bank Name" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Account No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="account_no" value="<%=rs.getString("account_no")%>" class="form-control" placeholder="Account No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Financial Year From:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="fin_year_from" value="<%=rs.getString("fin_year_from")%>" class="form-control" placeholder="Financial Year From" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                 <textarea  name="hospital_address"  class="form-control" placeholder="Address" ><%=rs.getString("hospital_address")%></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Name(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="person_name_first" value="<%=rs.getString("person_name_first")%>" class="form-control" placeholder="Contact Person Name(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="mobile_no_first" value="<%=rs.getString("mobile_no_first")%>" maxlength="10"  class="form-control" placeholder="Mobile No(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="email" name="email_first" value="<%=rs.getString("email_first")%>" class="form-control" placeholder="Email(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                    
                                                    
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Website:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="website" value="<%=rs.getString("website")%>" class="form-control" placeholder="Website" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                         <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Pan No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="pan_no" value="<%=rs.getString("pan_no")%>" class="form-control" placeholder="Pan No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">GST No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="gst_no" value="<%=rs.getString("gst_no")%>" class="form-control" placeholder="GST No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">IFSC No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="ifsc_no" value="<%=rs.getString("ifsc_no")%>" class="form-control" placeholder="IFSC No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Branch Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="branch_no" value="<%=rs.getString("branch_no")%>" class="form-control" placeholder="Branch Name" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Financial Year To:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="fin_year_to" value="<%=rs.getString("fin_year_to")%>" class="form-control" placeholder="Financial Year To" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Update</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                <%}
													rs.close();
													pst.close();
													con.close();
													
                                                }catch(Exception e)
                                                {
                                                	e.getMessage();
                                                }%>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                                <div style="overflow: auto;">
                           <div class="col-md-3 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>History Of Pharmacy</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()"><i class="fa fa-trash-o"></i>Clear History</button>
                                                </div>
                                    </div>
                            
                            <table>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Date</th>
                                    <th>Pharmacy_Name</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Name(First)</th>
                                    <th>Name(Second)</th>
                                    <th>Mobile(First)</th>
                                    <th>Mobile(Second)</th>
                                    <th>Email(First)</th>
                                    <th>Email(Second)</th>
                                    <th>Website</th>
                                    <th>Fax_No</th>
                                    <th>Pan_No</th>
                                    <th>Registration</th>
                                    <th>GST_No</th>
                                    <th>Bank_Name</th>
                                    <th>IFSC_No</th>
                                    <th>Account_No</th>
                                    <th>Branch_Name</th>
                                    <th>Setting</th>
                                </tr>
                               <%
                                try {
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			String sql="Select * from master_hospital_history ";
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr>
                        				 <td><%=i%></td>
                        				 <td><%=rs.getString("date")%></td>
                        				 <td><%=rs.getString("hospital_name")%></td>
                        				 <td><%=rs.getString("hospital_address")%></td>
                        				 <td><%=rs.getString("state")%></td>
                        				 <td><%=rs.getString("person_name_first")%></td>
                        				 <td><%=rs.getString("person_name_second")%></td>
                        				 <td><%=rs.getString("mobile_no_first")%></td>
                        				 <td><%=rs.getString("mobile_no_second")%></td>
                        				 <td><%=rs.getString("email_first")%></td>
                        				 <td><%=rs.getString("email_second")%></td>
                        				 <td><%=rs.getString("website")%></td>
                        				 <td><%=rs.getString("fax_no")%></td>
                        				 <td><%=rs.getString("pan_no")%></td>
                        				 <td><%=rs.getString("registration_no")%></td>
                        				 <td><%=rs.getString("gst_no")%></td>
                        				 <td><%=rs.getString("bank_name")%></td>
                        				 <td><%=rs.getString("ifsc_no")%></td>
                        				 <td><%=rs.getString("account_no")%></td>
                        				 <td><%=rs.getString("branch_no")%></td>
                        				 <td>
                                        <button data-toggle="tooltip" title="Trash" class="pd-setting-ed" onclick="deletefunc(<%=rs.getString("id")%>)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                         </td>
                        				 
                                   
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                            </table>
                            
                            <div class="custom-pagination">
								<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
      
      <!-- Modal -->
  <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="delete_hospital_master" method="post">
      <input type="hidden" name="del_id" id="del_id">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Delete History</h4>
        </div>
        <div class="modal-body">
        <p>Do you want to delete selected row!</p>       
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  
  
   <!-- Modal -->
  <div class="modal fade" id="clearhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="delete_all_record_hospital_master" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Clear All History</h4>
        </div>
        <div class="modal-body">
        <p>Do you want to clear all history!</p>       
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  
 
</div>

    </div>

    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
</body>

</html>