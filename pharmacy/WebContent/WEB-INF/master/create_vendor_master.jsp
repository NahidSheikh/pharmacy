<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pharmacy Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
		$("#submit_form").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#show_table_deleted_entries").hide();

    	
    });
    
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
    
    function deletefunc(val){
    	var id=val;
    	var staff_name=$("#vendor_name_"+id).text();   
    	var vendor_address=$("#vendor_address_"+id).text();
    	var state=$("#state_"+id).val();
        var mobile_no_first=$("#mobile_no_first_"+id).text(); 
        var mobile_no_second=$("#mobile_no_second_"+id).val();
        var email_first=$("#email_first_"+id).text();
        var email_second=$("#email_second_"+id).val();
        var website=$("#website_"+id).val();
    	var fax_no=$("#fax_no_"+id).val();
    	var pan_no=$("#pan_no_"+id).val();
    	var registration_no=$("#registration_no_"+id).val();
    	var gst_no=$("#gst_no_"+id).val();
    	var bank_name=$("#bank_name_"+id).val();
    	var ifsc_no=$("#ifsc_no_"+id).val();
    	var account_no=$("#account_no_"+id).val();
    	var branch_no=$("#branch_no_"+id).val();
    	var opening_balance=$("#opening_balance_"+id).text();
    	var opening_date=$("#opening_date_"+id).val();
    	var balance_type=$("#balance_type_"+id).val();
    	 if(balance_type=='Debit')
		{
    	$("#balance_type_ud" ).attr( 'checked', 'checked' )
		} 
    	 if(balance_type=='Credit')
 		{
     	$("#balance_type_uc" ).attr( 'checked', 'checked' )
 		} 
    	 
    	 
    	$("#vendor_name_u").val(staff_name);
    	$("#vendor_address_u").val(vendor_address);
    	$("#state_u").val(state);
    	$("#mobile_no_first_u").val(mobile_no_first);
    	$("#mobile_no_second_u").val(mobile_no_second);
    	$("#email_first_u").val(email_first);
    	$("#email_second_u").val(email_second);
    	$("#website_u").val(website);
    	$("#fax_no_u").val(fax_no);
    	$("#pan_no_u").val(pan_no);
    	$("#registration_no_u").val(registration_no);
    	$("#gst_no_u").val(gst_no);
    	$("#bank_name_u").val(bank_name);
    	$("#ifsc_no_u").val(ifsc_no);
    	$("#account_no_u").val(account_no);
    	$("#branch_no_u").val(branch_no);
    	$("#opening_balance_u").val(opening_balance);
    	$("#opening_date_u").val(opening_date);
    	

    	
    	
    	$("#s_id_del").val(id);
    	$("#s_id_his").val(id);
    	$("#staff_name_del").text(staff_name);
    	$("#s_id_u").val(id);
    	
    	/************** History Staff Master ************/
    	var s_id_his=$("#s_id_his").val();
		$.ajax({
  		  type: "POST",
  		  url: "StaffServlet",
  		  data: {id:s_id_his },
  		  dataType: "json",
   		  success: function(json){
   	  	  $("#tablehis tbody").empty();
  		var json1 = jQuery.parseJSON(JSON.stringify(json));
  		var i=1;
  		    	 $.each(json1, function(index, element) {
  		    		 
  		    		 var markup="<tr>"+
  		    		            "<td>"+i+"</td>"+
  		    		            "<td>"+element.staff_name+"</td>"+
  		    		            "<td>"+element.staff_id+"</td>"+
  		    		            "<td>"+element.dob+"</td>"+
  		    		            "<td>"+element.age+"</td>"+
  		    		            "<td>"+element.address+"</td>"+
  		    		            "<td>"+element.state+"</td>"+
  		    		            "<td>"+element.email_id+"</td>"+
  		    		            "<td>"+element.mobile_no+"</td>"+
  		    		            "<td>"+element.blood_group+"</td>"+
  		    		            "<td>"+element.qualification+"</td>"+
  		    		            "<td>"+element.designation+"</td>"+
  		    		            "<td>"+element.department+"</td>"+
  		    		            "<td>"+element.date+"</td>"+
  		    		            "</tr>";
  		    		            $("#tablehis tbody").append(markup);
  			   
  		    	 i++;
  		    	 }); 
  		    } 
  		});
    	/************** End History Staff Master ************/
    	$("#myModaldelete").modal();
    }
    
    function update_staff_master()
    {
    	var vendor_name_u=$("#vendor_name_u").val();
    	var state_u=$("#state_u").val();
    	var mobile_no_first_u=$("#mobile_no_first_u").val();
    	var mobile_no_second_u=$("#mobile_no_second_u").val();
    	var email_first_u=$("#email_first_u").val();
    	var email_second_u=$("#email_second_u").val();
    	var website_u=$("#website_u").val();
    	var fax_no_u=$("#fax_no_u").val();
    	var pan_no_u=$("#pan_no_u").val();
    	var registration_no_u=$("#registration_no_u").val();
    	var gst_no_u=$("#gst_no_u").val();
    	
    	var bank_name_u=$("#bank_name_u").val();
    	var ifsc_no_u=$("#ifsc_no_u").val();
    	var account_no_u=$("#account_no_u").val();
    	var branch_no_u=$("#branch_no_u").val();
    	var opening_balance_u=$("#opening_balance_u").val();
    	var opening_date_u=$("#opening_date_u").val();
    	var vendor_address_u=$("#vendor_address_u").val();
    	
        var balance_type_u = $("input[name='balance_type_u']:checked").val();

     	var id=$("#s_id_u").val();
    	
    	$.ajax({
    		  type: "GET",
    		  url: "VendorServlet",
    		  data: {vendor_name_u:vendor_name_u, state_u: state_u, mobile_no_first_u:mobile_no_first_u,
    			  mobile_no_second_u:mobile_no_second_u, email_first_u:email_first_u, email_second_u:email_second_u, 
    			  website_u:website_u, fax_no_u:fax_no_u, pan_no_u:pan_no_u, registration_no_u:registration_no_u,
    			  gst_no_u:gst_no_u, bank_name_u:bank_name_u, ifsc_no_u:ifsc_no_u, account_no_u:account_no_u,
    			  branch_no_u:branch_no_u, opening_balance_u:opening_balance_u, opening_date_u:opening_date_u,id:id,
    			  vendor_address_u:vendor_address_u, balance_type_u:balance_type_u 
    		  },
    		       success: function(data){
    			   alert("Update Successfully!");
    			   $("#vendor_name_"+id).text(vendor_name_u);   
    		       $("#vendor_address_"+id).text(vendor_address_u);
    		       $("#state_"+id).val(state_u);
    		       $("#mobile_no_first_"+id).text(mobile_no_first_u); 
    		       $("#mobile_no_second_"+id).val(mobile_no_second_u);
    		       $("#email_first_"+id).text(email_first_u);
    		       $("#email_second_"+id).val(email_second_u);
    		       $("#website_"+id).val(website_u);
    		       $("#fax_no_"+id).val(fax_no_u);
    		       $("#pan_no_"+id).val(pan_no_u);
    		       $("#registration_no_"+id).val(registration_no_u);
    		       $("#gst_no_"+id).val(gst_no_u);
    		       $("#bank_name_"+id).val(bank_name_u);
    		       $("#ifsc_no_"+id).val(ifsc_no_u);
    		       $("#account_no_"+id).val(account_no_u);
    		       $("#branch_no_"+id).val(branch_no_u);
    		       $("#opening_balance_"+id).text(opening_balance_u);
    		       $("#opening_date_"+id).val(opening_date_u);
    		       $("#balance_type_"+id).val(balance_type_u);
    		    } 
    		});
    	/************** History Staff Master ************/
    	var s_id_his=$("#s_id_his").val();
		$.ajax({
  		  type: "POST",
  		  url: "StaffServlet",
  		  data: {id:s_id_his },
  		  dataType: "json",
   		  success: function(json){
   	  	  $("#tablehis tbody").empty();
  		var json1 = jQuery.parseJSON(JSON.stringify(json));
  		var i=1;
  		    	 $.each(json1, function(index, element) {
  		    		 
  		    		 var markup="<tr>"+
  		    		            "<td>"+i+"</td>"+
  		    		            "<td>"+element.staff_name+"</td>"+
  		    		            "<td>"+element.staff_id+"</td>"+
  		    		            "<td>"+element.dob+"</td>"+
  		    		            "<td>"+element.age+"</td>"+
  		    		            "<td>"+element.address+"</td>"+
  		    		            "<td>"+element.state+"</td>"+
  		    		            "<td>"+element.email_id+"</td>"+
  		    		            "<td>"+element.mobile_no+"</td>"+
  		    		            "<td>"+element.blood_group+"</td>"+
  		    		            "<td>"+element.qualification+"</td>"+
  		    		            "<td>"+element.designation+"</td>"+
  		    		            "<td>"+element.department+"</td>"+
  		    		            "<td>"+element.date+"</td>"+
  		    		            "</tr>";
  		    		            $("#tablehis tbody").append(markup);
  			   
  		    	 i++;
  		    	 }); 
  		    } 
  		});
    	/************** End History Staff Master ************/

    }
    
    function form_change(val){
    	if(val==0)
    		{
    		$("#submit_form").show();
    		$("#show_table").hide();
    		
    		$("#show_table_deleted_entries").hide();

    		}
    	
    	if(val==1)
		{
		$("#submit_form").hide();
		$("#show_table").show();
		$("#show_table_deleted_entries").hide();

		}
    	if(val==2)
		{
		$("#submit_form").hide();
		$("#show_table").hide();
		$("#show_table_deleted_entries").show();

		}
    }
    
    function form_change_modal(val){
    	if(val==1)
    		{
    		$("#updatestaffmodal").show();
    		$("#deletestaffmodal").hide();
    		$("#historystaffmodal").hide();

    		}
    	
    	if(val==0)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").show();
		$("#historystaffmodal").hide();


		}
    	if(val==2)
		{
    	$("#historystaffmodal").show();
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		
		//$("#tablehis").append(markup);

		}
    }
    
    
    function clear_history() {
    	$("#clearhistory").modal();
		
	}
    function page_refresh()
    {
    	window.location.href='create_vendor_master';
    }
    
    </script>
    <style type="text/css">
    
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>

<%
request.getSession().setAttribute("master_ses", "vendor_ses");
%>
    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <jsp:include page="../public/topmenu.jsp"></jsp:include>
            <!-- Mobile Menu start -->
             <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>
            <jsp:include page="subheader_master.jsp"></jsp:include>
        </div>
        
         <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default">Vendor Master</button>
                                         </div>
        </div> 
        
        <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
        <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change(1)" class="cursor-class">Show Table</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(0)" class="cursor-class">Add New</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(2)" class="cursor-class">Deleted Entries</a>
                                                            </li>

                                                        </ul>
        
        </div>
        <!-- Single pro tab review Start-->
        
         <%
             int count=0;
                 // System.out.println("count==========="+count);
         %>
                                            
                                            
      <%if(count==0){ %>
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            
                           
                             
                            
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit" >
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="basic-login-inner">
                                           <form action="insert_vendor_master" method="post" onsubmit="validateform()">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Vendor Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                 <div class="input-mark-inner">
                                                                    <input type="text" name="vendor_name"  class="form-control" placeholder="Vendor Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">State :</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="state" class="form-control">
																	<option>Select state</option>
																	<option>Gujarat</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Gujarat</option>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no_second" maxlength="10" class="form-control" placeholder="Mobile No(Second)" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                     
                                                     
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="email" name="email_second"  class="form-control" placeholder="Email(Second)" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Fax No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="fax_no"  class="form-control" placeholder="Fax No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Registration:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="registration_no"  class="form-control" placeholder="Registration No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Bank Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="bank_name"  class="form-control" placeholder="Bank Name" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Account No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="account_no"  class="form-control" placeholder="Account No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Opening Balance:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="opening_balance"  class="form-control" placeholder="Opening Balance" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Opening Date:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="opening_date" value="01/04/2020" class="form-control" placeholder="Opening Date" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                 <textarea  name="vendor_address"  class="form-control" placeholder="Address" autocomplete="off"></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                     
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no_first" maxlength="10"  class="form-control" placeholder="Mobile No(First)" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="email" name="email_first"  class="form-control" placeholder="Email(First)" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                    
                                                    
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Website:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="website"  class="form-control" placeholder="Website" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                         <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Pan No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="pan_no"  class="form-control" placeholder="Pan No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">GST No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="gst_no"  class="form-control" placeholder="GST No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">IFSC No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="ifsc_no"  class="form-control" placeholder="IFSC No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Branch Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="branch_no"  class="form-control" placeholder="Branch Name" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Balance Type</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="radio" name="balance_type"   value="Debit"><span class="gend_color">Debit</span> 
                                                                    <input type="radio" name="balance_type"   value="Credit" checked="checked"><span class="gend_color">Credit</span>
                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Save</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%}else{ %>
        
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <!-- <ul class="tab-review-design">
                                <li class="active"><a >Hospital Master</a></li>
                            </ul> -->
                            <!-- <div class="row">
                            <div class="col-md-4">
                            <ul id="myTab4" class="tab-review-design">
                                 <li ><span style="font-size: 20px">Hospital Master</span></li>
                            </ul>
                            </div>
                            <div class="col-md-8">
                            
                            <ul id="myTab4" class="tab-review-design">
                                 <li class="active" ><a href="#description">Add Info</a></li>
                                <li><a href="#reviews" > History</a></li>
                               
                            </ul>
                            </div>
                            
                            </div> -->
                           
                             
                            <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                            <form action="update_hospital_master" method="post" onsubmit="validateform()">
                                                <% 
                                                try{
													new ConnectionClass();
													Connection con= ConnectionClass.getconnection();
													String sql="select * from master_hospital";
													PreparedStatement pst=con.prepareStatement(sql);
													ResultSet rs=pst.executeQuery();
													if(rs.next())
													{
														
													
		%>
                                                
                                                <input type="hidden" name="h_id"  value="<%=rs.getString("id")%>">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Hospital Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="hospital_name" value="<%=rs.getString("hospital_name")%>"  class="form-control" placeholder="Hospital Name" required="required">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">State :</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
 																<select name="state" class="form-control">
 																<option value="<%=rs.getString("state")%>"><%=rs.getString("state")%></option>
																	<option value="">Select state</option>
																	<option value="Gujarat">Gujarat</option>
																	<option value="Maharastra">Maharastra</option>
																	<option value="Rajastan">Rajastan</option>
																	<option value="Maharastra">Maharastra</option>
																	<option value="Rajastan">Rajastan</option>
																	<option value="Gujarat">Gujarat</option>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Name(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="person_name_second" value="<%=rs.getString("person_name_second")%>"  class="form-control" placeholder="Contact Person Name(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="mobile_no_second" value="<%=rs.getString("mobile_no_second")%>" maxlength="10" class="form-control" placeholder="Mobile No(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                     
                                                     
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="email" name="email_second" value="<%=rs.getString("email_second")%>" class="form-control" placeholder="Email(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Fax No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="fax_no" value="<%=rs.getString("fax_no")%>" class="form-control" placeholder="Fax No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Registration:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="registration_no" value="<%=rs.getString("registration_no")%>" class="form-control" placeholder="Registration No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Bank Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="bank_name" value="<%=rs.getString("bank_name")%>" class="form-control" placeholder="Bank Name" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Account No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="account_no" value="<%=rs.getString("account_no")%>" class="form-control" placeholder="Account No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                 <textarea  name="hospital_address"  class="form-control" placeholder="Address" ><%=rs.getString("hospital_address")%></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Name(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="person_name_first" value="<%=rs.getString("person_name_first")%>" class="form-control" placeholder="Contact Person Name(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="mobile_no_first" value="<%=rs.getString("mobile_no_first")%>" maxlength="10"  class="form-control" placeholder="Mobile No(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="email" name="email_first" value="<%=rs.getString("email_first")%>" class="form-control" placeholder="Email(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                    
                                                    
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Website:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="website" value="<%=rs.getString("website")%>" class="form-control" placeholder="Website" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                         <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Pan No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="pan_no" value="<%=rs.getString("pan_no")%>" class="form-control" placeholder="Pan No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">GST No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="gst_no" value="<%=rs.getString("gst_no")%>" class="form-control" placeholder="GST No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">IFSC No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="ifsc_no" value="<%=rs.getString("ifsc_no")%>" class="form-control" placeholder="IFSC No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Branch Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="branch_no" value="<%=rs.getString("branch_no")%>" class="form-control" placeholder="Branch Name" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Update</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                <%}
													rs.close();
													pst.close();
													con.close();
													
                                                }catch(Exception e)
                                                {
                                                	e.getMessage();
                                                }%>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Vendor Master</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Vendor_Name</th>
                                    <th>Vendor_Id</th>
                                    <th>Address</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Opening Balance</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               String staff_name="";
                               if(request.getParameter("staff_name")!=null)
                               {
                            	   staff_name=request.getParameter("staff_name");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(!staff_name.trim().equals(""))
                        			{
                        			 sql="Select * from master_vendor where vendor_name='"+staff_name+"'";
                        			}else{
                           			 sql="Select * from master_vendor ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr onclick="deletefunc(<%=rs.getString("id")%>)">
                        				 <td><%=i%></td>
                        				 <td id="vendor_name_<%=rs.getString("id")%>" style="color:yellow;cursor:pointer"><%=rs.getString("vendor_name")%></td>
                        				 <td id="vendor_id_<%=rs.getString("id")%>" ><%=rs.getString("vendor_id")%></td>
                        				 <td id="vendor_address_<%=rs.getString("id")%>"><%=rs.getString("vendor_address")%></td>
                        				 <td id="mobile_no_first_<%=rs.getString("id")%>"><%=rs.getString("mobile_no_first")%></td>
                        				 <td id="email_first_<%=rs.getString("id")%>"><%=rs.getString("email_first")%></td>
                        				 <td id="opening_balance_<%=rs.getString("id")%>"><%=rs.getString("opening_balance")%></td>
                        				 <td id="date_<%=rs.getString("id")%>"><%=rs.getString("date")%></td>
                        				 <input id="state_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("state")%>">
                        				 <input id="mobile_no_second_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("mobile_no_second")%>">
                        				 <input id="email_second_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("email_second")%>">
                        				 <input id="website_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("website")%>">
                        				 <input id="fax_no_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("fax_no")%>">
                        				 <input id="pan_no_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("pan_no")%>">
                        				 <input id="registration_no_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("registration_no")%>">
                        				 <input id="gst_no_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("gst_no")%>">
                        				 <input id="bank_name_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("bank_name")%>">
                        				 <input id="ifsc_no_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("ifsc_no")%>">
                        				 <input id="account_no_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("account_no")%>">
                        				 <input id="branch_no_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("branch_no")%>">
                        				 <input id="opening_date_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("opening_date")%>">
                        				 <input id="balance_type_<%=rs.getString("id")%>" type="hidden" value="<%=rs.getString("balance_type")%>">
                        				 
                        				  </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      
      <!--Deleted Entries application  -->
      <div class="product-status mg-b-30" id="show_table_deleted_entries">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Deleted Entries</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               if(request.getParameter("staff_name")!=null)
                               {
                            	   staff_name=request.getParameter("staff_name");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(!staff_name.trim().equals(""))
                        			{
                        			 sql="Select * from master_staff_delete_entries where staff_name='"+staff_name+"'";
                        			}else{
                           			 sql="Select * from master_staff_delete_entries ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr>
                        				 <td><%=i%></td>
                        				 <td><%=rs.getString("staff_name")%></td>
                        				 <td><%=rs.getString("staff_id")%></td>
                        				 <td><%=rs.getString("dob")%></td>
                        				 <td><%=rs.getString("age")%></td>
                        				 <td><%=rs.getString("address")%></td>
                        				 <td><%=rs.getString("state")%></td>
                        				 <td><%=rs.getString("email_id")%></td>
                        				 <td><%=rs.getString("mobile_no")%></td>
                        				 <td><%=rs.getString("blood_group")%></td>
                        				 <td><%=rs.getString("qualification")%></td>
                        				 <td><%=rs.getString("designation")%></td>
                        				 <td><%=rs.getString("department")%></td>
                        				 <td><%=rs.getString("date")%></td>
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
      
      
     

  
      
      <!-- Modal -->
  <!-- <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="delete_hospital_master" method="post">
      <input type="hidden" name="del_id" id="del_id">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Delete History</h4>
           <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change_modal(1)">Show Table</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(0)">Add New</a>
                                                            </li>

                                                        </ul>
        </div>
        <div class="modal-body" >
        <p>Do you want to delete selected row!</p>       
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
        </form>
      </div>
      /.modal-content
    </div>
    /.modal-dialog
  </div> -->
  <!-- /.modal -->
  
  <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
 					<ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change_modal(1)" >Update</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(0)" style="cursor:pointer">Delete</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(2)" style="cursor:pointer">History</a>
                                                            </li>

                    </ul> 
                       
            </div>
        <div class="modal-body" style="height:350px;overflow:auto">
        <div id="updatestaffmodal">
        
        <form  method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_u" id="s_id_u">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Update Vendor Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Vendor_Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="vendor_name" id="vendor_name_u" class="form-control" placeholder="Vendor Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">State :</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="state" id="state_u" class="form-control">
																	<option>Select state</option>
																	<option>Gujarat</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Gujarat</option>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no_second" id="mobile_no_second_u" maxlength="10" class="form-control" placeholder="Mobile No(Second)" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="email" name="email_second" id="email_second_u"  class="form-control" placeholder="Email(Second)" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Fax No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="fax_no" id="fax_no_u"  class="form-control" placeholder="Fax No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Registration:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="registration_no" id="registration_no_u"  class="form-control" placeholder="Registration No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Bank Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="bank_name" id="bank_name_u"  class="form-control" placeholder="Bank Name" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Account No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="account_no" id="account_no_u"  class="form-control" placeholder="Account No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                     
                                                         <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Opening Balance:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="opening_balance" id="opening_balance_u"  class="form-control" placeholder="Opening Balance" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Opening Date:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="opening_date" id="opening_date_u" value="01/04/2020" class="form-control" placeholder="Opening Date" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                    
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                 <textarea  name="vendor_address"  id="vendor_address_u"  class="form-control" placeholder="Address" autocomplete="off"></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                     
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no_first" id="mobile_no_first_u" maxlength="10"  class="form-control" placeholder="Mobile No(First)" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="email" name="email_first" id="email_first_u"  class="form-control" placeholder="Email(First)" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                    
                                                    
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Website:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="website" id="website_u"  class="form-control" placeholder="Website" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                         <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Pan No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="pan_no"  id="pan_no_u"  class="form-control" placeholder="Pan No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">GST No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="gst_no" id="gst_no_u"  class="form-control" placeholder="GST No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">IFSC No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="ifsc_no" id="ifsc_no_u"  class="form-control" placeholder="IFSC No" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Branch Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="branch_no" id="branch_no_u"  class="form-control" placeholder="Branch Name" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Balance Type</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="radio" name="balance_type_u" id="balance_type_ud"  value="Debit"><span class="gend_color">Debit</span> 
                                                                    <input type="radio" name="balance_type_u"  id="balance_type_uc" value="Credit" checked="checked"><span class="gend_color">Credit</span>
                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                        
                                                    
                                                      
                                                      
                                                     
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="button" onclick="update_staff_master()" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Update</button>
<!--                                                             <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     <!--@@@@@@@@@@@@@@@ Delete Modal  @@@@@@@@@@@@@@@-->
                     <div id="deletestaffmodal">
        
        <form  action="delete_staff_master" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_del" id="s_id_del">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Delete Staff Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <br>
                                                    <p>Do you want to delete whole detail of staff <span id="staff_name_del" style="color:yellow;font-size:18px"></span>!</p>
                                                   </div> 
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Yes</button>
<!--                                                        <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     
          <div id="historystaffmodal">
        
        <form  action="" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_his" id="s_id_his">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>History Staff Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <table id="tablehis" class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                              
                        		 </tbody>
                            </table> </div> 
                                                </div>
                                                
                                                </form>
                     </div>
                     
                     
                     
                     
           </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--           <button type="button" class="btn btn-primary">Save changes</button>
 -->        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  
  
   <!-- Modal -->
  <div class="modal fade" id="clearhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Search Staff</h4>
        </div>
        <div class="modal-body">
													<div class="row">
													<div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Vendor Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input list="staff-list" type="text" name="staff_name"  class="form-control" placeholder="Vendor Name"  autocomplete="off">
                                                                <datalist id="staff-list">
  																<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from master_vendor group by vendor_name";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("vendor_name")%>"><%=rs.getString("vendor_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</datalist>
                                                                
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     
                                                     
                                                      </div>   
                                                        
                                   </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Search</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  
 
</div>

    

    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
</body>

</html>