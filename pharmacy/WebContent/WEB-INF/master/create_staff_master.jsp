<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hospital Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
		$("#submit_form").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#show_table_deleted_entries").hide();

    	
    });
    
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
    
    function deletefunc(val){
    	var id=val;
    	var staff_name=$("#staff_name_"+id).text();   
    	var dob=$("#dob_"+id).text();
    	var age=$("#age_"+id).text();
    	var address=$("#address_"+id).text();
    	var state=$("#state_"+id).text();
    	var email_id=$("#email_id_"+id).text();
    	var mobile_no=$("#mobile_no_"+id).text();
    	var qualification=$("#qualification_"+id).text();
    	var designation=$("#designation_"+id).text();
    	var department=$("#department_"+id).text();
    	var blood_group=$("#blood_group_"+id).text();
    	
    	$("#staff_name_u").val(staff_name);
    	$("#dob_u").val(dob);
    	$("#age_u").val(age);
    	$("#address_u").val(address);
    	$("#state_u").val(state);
    	$("#email_id_u").val(email_id);
    	$("#mobile_no_u").val(mobile_no);
    	$("#qualification_u").val(qualification);
    	$("#designation_u").val(designation);
    	$("#department_u").val(department);
    	$("#blood_group_u").val(blood_group);
    	
    	
    	$("#s_id_del").val(id);
    	$("#s_id_his").val(id);
    	$("#staff_name_del").text(staff_name);
    	$("#s_id_u").val(id);
    	
    	/************** History Staff Master ************/
    	var s_id_his=$("#s_id_his").val();
		$.ajax({
  		  type: "POST",
  		  url: "StaffServlet",
  		  data: {id:s_id_his },
  		  dataType: "json",
   		  success: function(json){
   	  	  $("#tablehis tbody").empty();
  		var json1 = jQuery.parseJSON(JSON.stringify(json));
  		var i=1;
  		    	 $.each(json1, function(index, element) {
  		    		 s
  		    		 var markup="<tr>"+
  		    		            "<td>"+i+"</td>"+
  		    		            "<td>"+element.staff_name+"</td>"+
  		    		            "<td>"+element.staff_id+"</td>"+
  		    		            "<td>"+element.dob+"</td>"+
  		    		            "<td>"+element.age+"</td>"+
  		    		            "<td>"+element.address+"</td>"+
  		    		            "<td>"+element.state+"</td>"+
  		    		            "<td>"+element.email_id+"</td>"+
  		    		            "<td>"+element.mobile_no+"</td>"+
  		    		            "<td>"+element.blood_group+"</td>"+
  		    		            "<td>"+element.qualification+"</td>"+
  		    		            "<td>"+element.designation+"</td>"+
  		    		            "<td>"+element.department+"</td>"+
  		    		            "<td>"+element.date+"</td>"+
  		    		            "</tr>";
  		    		            $("#tablehis tbody").append(markup);
  			   
  		    	 i++;
  		    	 }); 
  		    } 
  		});
    	/************** End History Staff Master ************/
    	$("#myModaldelete").modal();
    }
    
    function update_staff_master()
    {
    	var staff_name=$("#staff_name_u").val();
    	var dob=$("#dob_u").val();
    	var age=$("#age_u").val();
    	var address=$("#address_u").val();
    	var state=$("#state_u").val();
    	var email_id=$("#email_id_u").val();
    	var mobile_no=$("#mobile_no_u").val();
    	var qualification=$("#qualification_u").val();
    	var designation=$("#designation_u").val();
    	var department=$("#department_u").val();
    	var blood_group=$("#blood_group_u").val();
    	var id=$("#s_id_u").val();
    	
    	$.ajax({
    		  type: "GET",
    		  url: "StaffServlet",
    		  data: {staff_name:staff_name, dob: dob, age:age, address:address,
    			  state:state, email_id:email_id, mobile_no:mobile_no, qualification:qualification,
    			  designation:designation, department:department, blood_group:blood_group, id:id  },
    		       success: function(data){
    			   alert("Update Successfully!");
    			    $("#staff_name_"+id).text(staff_name);   
    		        $("#dob_"+id).text(dob);
    		        $("#age_"+id).text(age);
    		        $("#address_"+id).text(address);
    		    	$("#state_"+id).text(state);
    		    	$("#email_id_"+id).text(email_id);
    		    	$("#mobile_no_"+id).text(mobile_no);
    		    	$("#qualification_"+id).text(qualification);
    		    	$("#designation_"+id).text(designation);
    		    	$("#department_"+id).text(department);
    		    	$("#blood_group_"+id).text(blood_group);
    		    } 
    		});
    	/************** History Staff Master ************/
    	var s_id_his=$("#s_id_his").val();
		$.ajax({
  		  type: "POST",
  		  url: "StaffServlet",
  		  data: {id:s_id_his },
  		  dataType: "json",
   		  success: function(json){
   	  	  $("#tablehis tbody").empty();
  		var json1 = jQuery.parseJSON(JSON.stringify(json));
  		var i=1;
  		    	 $.each(json1, function(index, element) {
  		    		 
  		    		 var markup="<tr>"+
  		    		            "<td>"+i+"</td>"+
  		    		            "<td>"+element.staff_name+"</td>"+
  		    		            "<td>"+element.staff_id+"</td>"+
  		    		            "<td>"+element.dob+"</td>"+
  		    		            "<td>"+element.age+"</td>"+
  		    		            "<td>"+element.address+"</td>"+
  		    		            "<td>"+element.state+"</td>"+
  		    		            "<td>"+element.email_id+"</td>"+
  		    		            "<td>"+element.mobile_no+"</td>"+
  		    		            "<td>"+element.blood_group+"</td>"+
  		    		            "<td>"+element.qualification+"</td>"+
  		    		            "<td>"+element.designation+"</td>"+
  		    		            "<td>"+element.department+"</td>"+
  		    		            "<td>"+element.date+"</td>"+
  		    		            "</tr>";
  		    		            $("#tablehis tbody").append(markup);
  			   
  		    	 i++;
  		    	 }); 
  		    } 
  		});
    	/************** End History Staff Master ************/

    }
    
    function form_change(val){
    	if(val==0)
    		{
    		$("#submit_form").show();
    		$("#show_table").hide();
    		
    		$("#show_table_deleted_entries").hide();

    		}
    	
    	if(val==1)
		{
		$("#submit_form").hide();
		$("#show_table").show();
		$("#show_table_deleted_entries").hide();

		}
    	if(val==2)
		{
		$("#submit_form").hide();
		$("#show_table").hide();
		$("#show_table_deleted_entries").show();

		}
    }
    
    function form_change_modal(val){
    	if(val==1)
    		{
    		$("#updatestaffmodal").show();
    		$("#deletestaffmodal").hide();
    		$("#historystaffmodal").hide();

    		}
    	
    	if(val==0)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").show();
		$("#historystaffmodal").hide();


		}
    	if(val==2)
		{
    	$("#historystaffmodal").show();
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		
		//$("#tablehis").append(markup);

		}
    }
    
    
    function clear_history() {
    	$("#clearhistory").modal();
		
	}
    function page_refresh()
    {
    	window.location.href='create_staff_master';
    }
    
    </script>
    <style type="text/css">
    
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>

<%
request.getSession().setAttribute("master_ses", "staff_ses");
%>
    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <jsp:include page="../public/topmenu.jsp"></jsp:include>
            <!-- Mobile Menu start -->
             <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>
            <jsp:include page="subheader_master.jsp"></jsp:include>
        </div>
        
         <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default">Staff Master</button>
                                         </div>
        </div> 
        
        <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
        <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change(1)" class="cursor-class">Show Table</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(0)" class="cursor-class">Add New</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(2)" class="cursor-class">Deleted Entries</a>
                                                            </li>

                                                        </ul>
        
        </div>
        <!-- Single pro tab review Start-->
        
         <%
             int count=0;
                 // System.out.println("count==========="+count);
         %>
                                            
                                            
      <%if(count==0){ %>
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            
                           
                             
                            
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit" >
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="basic-login-inner">
                                            <form action="insert_staff_master" method="post" onsubmit="validateform()">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Staff Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="staff_name"  class="form-control" placeholder="Staff Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group-inner">
                                                                 <textarea  name="address"  class="form-control form-group-inner" placeholder="Address" ></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Email-id:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="email" name="email_id"  class="form-control" placeholder="Email(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Blood Group:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="blood_group" class=" form-control">
																	<option value="">Select Blood Group</option>
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from blood_group ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("blood_name")%>"><%=rs.getString("blood_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Designation:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="designation" class=" form-control">
																	<option value="">Select Designation</option>
																	<option value="Administrator">Administrator</option>
																	<option value="Doctor">Doctor</option>
																	<option value="Nurse">Nurse</option>
																	<option value="Lab Assistant">Lab Assistant</option>
																	<option value="Labor">Labor</option>
																	<option value="Other">Other</option>
																</select>                                                               
																 </div>
                                                            </div>
                                                      </div>
                                                     
                                                      
                                                       
                                                     
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Date Of Birth:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-5">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="dob"  class="form-control" placeholder="Date Of Birth"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Age:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="age"  class="form-control" placeholder="Age"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">State :</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="state" class=" form-control">
																	<option value="">Select state</option>
																	<option value="Gujarat">Gujarat</option>
																	<option value="Maharastra">Maharastra</option>
																	<option value="Rajastan">Rajastan</option>
																	<option value="Maharastra">Maharastra</option>
																	<option value="Rajastan">Rajastan</option>
																	<option value="Gujarat">Gujarat</option>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label"> Mobile No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no" maxlength="10"  class="form-control" placeholder="Mobile No(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Qualification:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="qualification" class=" form-control">
																	<option value="">Select Qualification</option>
																	<option value="10th">10th</option>
																	<option value="12th">12th</option>
																	<option value="Graduate">Graduate</option>
																	<option value="Post-Graduation">Post-Graduation</option>
																	<option value="Other">Other</option>
																</select>                                                               
																 </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Department:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="department" class=" form-control">
 																<option value="">Select Department</option>
 																
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from master_opd_lab ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("lab_name")%>"><%=rs.getString("lab_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>																	
																</select>                                                               
																 </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                     
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Save</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%}else{ %>
        
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <!-- <ul class="tab-review-design">
                                <li class="active"><a >Hospital Master</a></li>
                            </ul> -->
                            <!-- <div class="row">
                            <div class="col-md-4">
                            <ul id="myTab4" class="tab-review-design">
                                 <li ><span style="font-size: 20px">Hospital Master</span></li>
                            </ul>
                            </div>
                            <div class="col-md-8">
                            
                            <ul id="myTab4" class="tab-review-design">
                                 <li class="active" ><a href="#description">Add Info</a></li>
                                <li><a href="#reviews" > History</a></li>
                               
                            </ul>
                            </div>
                            
                            </div> -->
                           
                             
                            <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                            <form action="update_hospital_master" method="post" onsubmit="validateform()">
                                                <% 
                                                try{
													new ConnectionClass();
													Connection con= ConnectionClass.getconnection();
													String sql="select * from master_hospital";
													PreparedStatement pst=con.prepareStatement(sql);
													ResultSet rs=pst.executeQuery();
													if(rs.next())
													{
														
													
		%>
                                                
                                                <input type="hidden" name="h_id"  value="<%=rs.getString("id")%>">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Hospital Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="hospital_name" value="<%=rs.getString("hospital_name")%>"  class="form-control" placeholder="Hospital Name" required="required">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">State :</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
 																<select name="state" class="form-control">
 																<option value="<%=rs.getString("state")%>"><%=rs.getString("state")%></option>
																	<option value="">Select state</option>
																	<option value="Gujarat">Gujarat</option>
																	<option value="Maharastra">Maharastra</option>
																	<option value="Rajastan">Rajastan</option>
																	<option value="Maharastra">Maharastra</option>
																	<option value="Rajastan">Rajastan</option>
																	<option value="Gujarat">Gujarat</option>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Name(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="person_name_second" value="<%=rs.getString("person_name_second")%>"  class="form-control" placeholder="Contact Person Name(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="mobile_no_second" value="<%=rs.getString("mobile_no_second")%>" maxlength="10" class="form-control" placeholder="Mobile No(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                     
                                                     
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(Second):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="email" name="email_second" value="<%=rs.getString("email_second")%>" class="form-control" placeholder="Email(Second)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Fax No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="fax_no" value="<%=rs.getString("fax_no")%>" class="form-control" placeholder="Fax No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Registration:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="registration_no" value="<%=rs.getString("registration_no")%>" class="form-control" placeholder="Registration No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Bank Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="bank_name" value="<%=rs.getString("bank_name")%>" class="form-control" placeholder="Bank Name" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Account No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="account_no" value="<%=rs.getString("account_no")%>" class="form-control" placeholder="Account No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                 <textarea  name="hospital_address"  class="form-control" placeholder="Address" ><%=rs.getString("hospital_address")%></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Name(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="person_name_first" value="<%=rs.getString("person_name_first")%>" class="form-control" placeholder="Contact Person Name(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Mobile(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="mobile_no_first" value="<%=rs.getString("mobile_no_first")%>" maxlength="10"  class="form-control" placeholder="Mobile No(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Email(First):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="email" name="email_first" value="<%=rs.getString("email_first")%>" class="form-control" placeholder="Email(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                    
                                                    
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label"> Website:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="website" value="<%=rs.getString("website")%>" class="form-control" placeholder="Website" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                         <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Pan No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="pan_no" value="<%=rs.getString("pan_no")%>" class="form-control" placeholder="Pan No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">GST No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="gst_no" value="<%=rs.getString("gst_no")%>" class="form-control" placeholder="GST No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">IFSC No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="ifsc_no" value="<%=rs.getString("ifsc_no")%>" class="form-control" placeholder="IFSC No" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
														            <label class="form-label">Branch Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <input type="text" name="branch_no" value="<%=rs.getString("branch_no")%>" class="form-control" placeholder="Branch Name" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Update</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                <%}
													rs.close();
													pst.close();
													con.close();
													
                                                }catch(Exception e)
                                                {
                                                	e.getMessage();
                                                }%>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%}%>
        
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Staff Master</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               String staff_name="";
                               if(request.getParameter("staff_name")!=null)
                               {
                            	   staff_name=request.getParameter("staff_name");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(!staff_name.trim().equals(""))
                        			{
                        			 sql="Select * from master_staff where staff_name='"+staff_name+"'";
                        			}else{
                           			 sql="Select * from master_staff ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr onclick="deletefunc(<%=rs.getString("id")%>)">
                        				 <td><%=i%></td>
                        				 <td id="staff_name_<%=rs.getString("id")%>" style="color:yellow;cursor:pointer"><%=rs.getString("staff_name")%></td>
                        				 <td id="staff_id_<%=rs.getString("id")%>" ><%=rs.getString("staff_id")%></td>
                        				 <td id="dob_<%=rs.getString("id")%>"><%=rs.getString("dob")%></td>
                        				 <td id="age_<%=rs.getString("id")%>"><%=rs.getString("age")%></td>
                        				 <td id="address_<%=rs.getString("id")%>"><%=rs.getString("address")%></td>
                        				 <td id="state_<%=rs.getString("id")%>"><%=rs.getString("state")%></td>
                        				 <td id="email_id_<%=rs.getString("id")%>"><%=rs.getString("email_id")%></td>
                        				 <td id="mobile_no_<%=rs.getString("id")%>"><%=rs.getString("mobile_no")%></td>
                        				 <td id="blood_group_<%=rs.getString("id")%>"><%=rs.getString("blood_group")%></td>
                        				 <td id="qualification_<%=rs.getString("id")%>"><%=rs.getString("qualification")%></td>
                        				 <td id="designation_<%=rs.getString("id")%>"><%=rs.getString("designation")%></td>
                        				 <td id="department_<%=rs.getString("id")%>"><%=rs.getString("department")%></td>
                        				 <td id="date_<%=rs.getString("id")%>"><%=rs.getString("date")%></td>
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      
      <!--Deleted Entries application  -->
      <div class="product-status mg-b-30" id="show_table_deleted_entries">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Deleted Entries</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               if(request.getParameter("staff_name")!=null)
                               {
                            	   staff_name=request.getParameter("staff_name");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(!staff_name.trim().equals(""))
                        			{
                        			 sql="Select * from master_staff_delete_entries where staff_name='"+staff_name+"'";
                        			}else{
                           			 sql="Select * from master_staff_delete_entries ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr>
                        				 <td><%=i%></td>
                        				 <td><%=rs.getString("staff_name")%></td>
                        				 <td><%=rs.getString("staff_id")%></td>
                        				 <td><%=rs.getString("dob")%></td>
                        				 <td><%=rs.getString("age")%></td>
                        				 <td><%=rs.getString("address")%></td>
                        				 <td><%=rs.getString("state")%></td>
                        				 <td><%=rs.getString("email_id")%></td>
                        				 <td><%=rs.getString("mobile_no")%></td>
                        				 <td><%=rs.getString("blood_group")%></td>
                        				 <td><%=rs.getString("qualification")%></td>
                        				 <td><%=rs.getString("designation")%></td>
                        				 <td><%=rs.getString("department")%></td>
                        				 <td><%=rs.getString("date")%></td>
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
      
      
     

  
      
      <!-- Modal -->
  <!-- <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="delete_hospital_master" method="post">
      <input type="hidden" name="del_id" id="del_id">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Delete History</h4>
           <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change_modal(1)">Show Table</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(0)">Add New</a>
                                                            </li>

                                                        </ul>
        </div>
        <div class="modal-body" >
        <p>Do you want to delete selected row!</p>       
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
        </form>
      </div>
      /.modal-content
    </div>
    /.modal-dialog
  </div> -->
  <!-- /.modal -->
  
  <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
 					<ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change_modal(1)" >Update</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(0)" style="cursor:pointer">Delete</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(2)" style="cursor:pointer">History</a>
                                                            </li>

                    </ul> 
                       
            </div>
        <div class="modal-body" style="height:350px;overflow:auto">
        <div id="updatestaffmodal">
        
        <form  method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_u" id="s_id_u">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Update Staff Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Staff-Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="staff_name" id="staff_name_u" class="form-control" placeholder="Staff Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Address:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group-inner">
                                                                 <textarea  name="address" id="address_u"  class="form-control form-group-inner" placeholder="Address" ></textarea>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Email-id:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="email" name="email_id" id="email_id_u" class="form-control" placeholder="Email(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Blood_Group:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="blood_group" id="blood_group_u" class=" form-control">
																	<option value="">Select Blood Group</option>
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from blood_group ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("blood_name")%>"><%=rs.getString("blood_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Designation:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="designation"  id="designation_u" class=" form-control">
																	<option value="">Select Designation</option>
																	<option value="Administrator">Administrator</option>
																	<option value="Doctor">Doctor</option>
																	<option value="Nurse">Nurse</option>
																	<option value="Lab Assistant">Lab Assistant</option>
																	<option value="Labor">Labor</option>
																	<option value="Other">Other</option>
																</select>                                                               
																 </div>
                                                            </div>
                                                      </div>
                                                     
                                                      
                                                       
                                                     
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Date_Of_Birth:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-5">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="dob" id="dob_u"  class="form-control" placeholder="Date Of Birth"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Age:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="age" id="age_u"  class="form-control" placeholder="Age"  autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">State :</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="state" id="state_u" class=" form-control">
																	<option>Select state</option>
																	<option>Gujarat</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Maharastra</option>
																	<option>Rajastan</option>
																	<option>Gujarat</option>
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                     
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label"> Mobile-No:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="mobile_no" id="mobile_no_u" maxlength="10"  class="form-control" placeholder="Mobile No(First)" >
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Qualification:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="qualification" id="qualification_u" class=" form-control">
																	<option value="">Select Qualification</option>
																	<option value="10th">10th</option>
																	<option value="12th">12th</option>
																	<option value="Graduate">Graduate</option>
																	<option value="Post-Graduation">Post-Graduation</option>
																	<option value="Other">Other</option>
																</select>                                                               
																 </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Department:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
 																<select name="department" id="department_u" class=" form-control">
																	<option value="">Select Department</option>
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from master_opd_lab ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("lab_name")%>"><%=rs.getString("lab_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																	
																</select>                                                               
																 </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                     
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="button" onclick="update_staff_master()" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Update</button>
<!--                                                             <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     <!--@@@@@@@@@@@@@@@ Delete Modal  @@@@@@@@@@@@@@@-->
                     <div id="deletestaffmodal">
        
        <form  action="delete_staff_master" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_del" id="s_id_del">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Delete Staff Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <br>
                                                    <p>Do you want to delete whole detail of staff <span id="staff_name_del" style="color:yellow;font-size:18px"></span>!</p>
                                                   </div> 
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Yes</button>
<!--                                                        <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     
          <div id="historystaffmodal">
        
        <form  action="" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_his" id="s_id_his">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>History Staff Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <table id="tablehis" class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                              
                        		 </tbody>
                            </table> </div> 
                                                </div>
                                                
                                                </form>
                     </div>
                     
                     
                     
                     
           </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--           <button type="button" class="btn btn-primary">Save changes</button>
 -->        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  
  
   <!-- Modal -->
  <div class="modal fade" id="clearhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Search Staff</h4>
        </div>
        <div class="modal-body">
													<div class="row">
													<div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Staff Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input list="staff-list" type="text" name="staff_name"  class="form-control" placeholder="Staff Name"  autocomplete="off">
                                                                <datalist id="staff-list">
  																<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from master_staff group by staff_name";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("staff_name")%>"><%=rs.getString("staff_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</datalist>
                                                                
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     
                                                     
                                                      </div>   
                                                        
                                   </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Search</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  
 
</div>

    

    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
</body>

</html>