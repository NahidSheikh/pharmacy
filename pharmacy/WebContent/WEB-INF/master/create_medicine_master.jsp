<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hospital Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
		$("#submit_form").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#show_table_deleted_entries").hide();

    	
    });
    
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
    
    function deletefunc(val){
    	var id=val;
    	var medicine_name=$("#medicine_name_"+id).text();   
    	var pack=$("#pack_"+id).val();
    	var medicine_type=$("#medicine_type_"+id).text();   
    	var company_name=$("#company_name_"+id).text();   
    	var medicine_category=$("#medicine_category_"+id).text();   
    	var unit=$("#unit_"+id).val(); 
    	alert(unit);
    	var hsn=$("#hsn_"+id).text();   
    	var low_stock=$("#low_stock_"+id).text();   

    	$("#medicine_name_u").val(medicine_name);
    	$("#pack_u").val(pack);
    	$("#medicine_type_u").val(medicine_type);
    	$("#company_name_u").val(company_name);
    	$("#medicine_category_u").val(medicine_category);
    	$("#unit_u").val(unit);
    	$("#hsn_u").val(hsn);
    	$("#low_stock_u").val(low_stock);


    	$("#s_id_del").val(id);
    	$("#s_id_his").val(id);
    	$("#staff_name_del").text(medicine_name);


    	$("#s_id_u").val(id);
    	$("#myModaldelete").modal();
    }
    
    function update_staff_master()
    {
    	var medicine_name=$("#medicine_name_u").val();
    	var pack=$("#pack_u").val();
    	var medicine_type=$("#medicine_type_u").val();
    	var company_name=$("#company_name_u").val();
    	var medicine_category=$("#medicine_category_u").val();
    	var unit=$("#unit_u").val();
    	var hsn=$("#hsn_u").val();
    	var low_stock=$("#low_stock_u").val();
    	
    	var id=$("#s_id_u").val();
    	
    	$.ajax({
    		  type: "GET",
    		  url: "MedicineServlet",
    		  data: {medicine_name:medicine_name, pack:pack, medicine_type:medicine_type, company_name:company_name,
    			 	 medicine_category:medicine_category, unit:unit, hsn:hsn, low_stock:low_stock,
    			  	 id:id},
    		       success: function(data){
    			   alert("Update Successfully!");
    			    $("#medicine_name_"+id).text(medicine_name);   
    		        $("#pack_unit_"+id).text(pack+unit);
    		        $("#medicine_type_"+id).text(medicine_type);
    		        $("#company_name_"+id).text(company_name);
    		        $("#medicine_category_"+id).text(medicine_category);
    		        $("#unit_"+id).text(unit);
    		        $("#hsn_"+id).text(hsn);
    		        $("#low_stock_"+id).text(low_stock);
    		        $("#pack_"+id).val(pack);
    		        $("#unit_"+id).val(unit);




    		    } 
    		});


    }
    
    function form_change(val){
    	if(val==0)
    		{
    		$("#submit_form").show();
    		$("#show_table").hide();
    		
    		$("#show_table_deleted_entries").hide();

    		}
    	
    	if(val==1)
		{
		$("#submit_form").hide();
		$("#show_table").show();
		$("#show_table_deleted_entries").hide();

		}
    	if(val==2)
		{
		$("#submit_form").hide();
		$("#show_table").hide();
		$("#show_table_deleted_entries").show();

		}
    }
    
    function form_change_modal(val){
    	if(val==1)
    		{
    		$("#updatestaffmodal").show();
    		$("#deletestaffmodal").hide();
    		$("#historystaffmodal").hide();

    		}
    	
    	if(val==0)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").show();
		$("#historystaffmodal").hide();


		}
    	if(val==2)
		{
    	$("#historystaffmodal").show();
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		var s_id_his=$("#s_id_his").val();
		$.ajax({
  		  type: "POST",
  		  url: "StaffServlet",
  		  data: {id:s_id_his },
  		  dataType: "json",
   		  success: function(json){
  			  alert(json);
  			                    /*   var json = JSON.parse(data);
  			                    alert(json); */
  		    	 /* var json1 = JSON.parse(json);
  		    	 $.each(json1, function(index, element) {
  		    		 var info = element.staff_name;
  		    		 alert(info);
  		    		// console.log(info);
  			   
  		    	 }); */
  		    } 
  		});
		
		//$("#tablehis").append(markup);

		}
    }
    
    
    function clear_history() {
    	$("#clearhistory").modal();
		
	}
    function page_refresh()
    {
    	window.location.href='create_medicine_master';
    }
    
    </script>
    <style type="text/css">
    
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>

<%
request.getSession().setAttribute("master_ses", "Medicine_ses");
%>
    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <jsp:include page="../public/topmenu.jsp"></jsp:include>
            <!-- Mobile Menu start -->
             <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>     
        <jsp:include page="subheader_master.jsp"></jsp:include>
        </div>
        
         <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default">Medicine Master</button>
                                         </div>
        </div> 
        
        <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
        <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change(1)" class="cursor-class">Show Table</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(0)" class="cursor-class">Add New</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(2)" class="cursor-class">Deleted Entries</a>
                                                            </li>

                                                        </ul>
        
        </div>
        <!-- Single pro tab review Start-->
        
         <%
             int count=0;
                 // System.out.println("count==========="+count);
         %>
                                            
                                            
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            
                           
                             
                            
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit" >
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="basic-login-inner">
                                            <form action="insert_medicine_master" method="post" onsubmit="validateform()">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Medicine Name</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="medicine_name"  class="form-control" placeholder="Medicine Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Package</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="number" name="pack"  class="form-control" placeholder="Package" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Medicine Type:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <select name="medicine_type"   class="form-control"  required="required">
                                                                    <option value="default">Select Medicine Type</option>
                                                                    <option value="Schedule H">Schedule H</option>
                                                                    <option value="Schedule H1">Schedule H1</option>
                                                                    <option value="Other">Other</option>
                                                                    
                                                                    </select>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Company Name</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="company_name"  class="form-control" placeholder="Company Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                       
                                                    </div>
                                                    <div class="col-lg-6">
                                                     
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Medicine_Category:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <select name="medicine_category"   class="form-control"  required="required">
                                                                    <option value="default">Select Medicine Category</option>
                                                                    <%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from medicine_category ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("CatName")%>"><%=rs.getString("CatName")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
                                                                     <option value="OTHER">OTHER</option>
                                                                    
                                                                    </select>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Unit:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <select name="unit"   class="form-control"  required="required">
                                                                    <option value="default">Select Unit</option>
                                                                    <option value="S">S</option>
                                                                    <option value="ML">ML</option>
                                                                    <option value="GM">GM</option>
                                                                    
                                                                    </select>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">HSN Code</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="hsn"  class="form-control" placeholder="HSN Code" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Low Stock Alert</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="number" name="low_stock"  class="form-control" placeholder="Low Stock Alert" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       
                                                      
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Save</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
       
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Medicine </h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Medicine Name</th>
                                    <th>Medicine Category</th>
                                    <th>Pack</th>
                                    <th>Medicine Type</th>
                                    <th>HSN Code</th>
                                    <th>Company Name</th>
                                    <th>Low Stock Alert</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               String medicine_name="";
                               if(request.getParameter("medicine_name")!=null)
                               {
                            	   medicine_name=request.getParameter("medicine_name");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(!medicine_name.trim().equals(""))
                        			{
                        			 sql="Select * from master_medicine where medicine_name='"+medicine_name+"'";
                        			}else{
                           			 sql="Select * from master_medicine ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr onclick="deletefunc(<%=rs.getString("id")%>)">
                        				 <td><%=i%></td>
                        				 <td id="medicine_name_<%=rs.getString("id")%>" style="color:yellow;cursor:pointer"><%=rs.getString("medicine_name")%></td>
                        				 <td id="medicine_category_<%=rs.getString("id")%>" ><%=rs.getString("medicine_category")%></td>
                        				 <td id="pack_unit_<%=rs.getString("id")%>"><%=rs.getString("pack")%><%=rs.getString("unit")%></td>
                        				 <td id="medicine_type_<%=rs.getString("id")%>" ><%=rs.getString("medicine_type")%></td>
                        				 <td id="hsn_<%=rs.getString("id")%>" ><%=rs.getString("hsn")%></td>
                        				 <td id="company_name_<%=rs.getString("id")%>" ><%=rs.getString("company_name")%></td>
                        				 <td id="low_stock_<%=rs.getString("id")%>" ><%=rs.getString("low_stock")%></td>
                        				 
                        				 <td id="date_<%=rs.getString("id")%>"><%=rs.getString("date")%></td>
                        				 <input type="hidden" id="pack_<%=rs.getString("id")%>" value="<%=rs.getString("pack")%>">
                        				 <input type="hidden" id="unit_<%=rs.getString("id")%>" value="<%=rs.getString("unit")%>">
                        				 
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      
      <!--Deleted Entries application  -->
      <div class="product-status mg-b-30" id="show_table_deleted_entries">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Deleted Entries</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Medicine Name</th>
                                    <th>Medicine Type</th>
                                    <th>Medicine Id</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               if(request.getParameter("staff_name")!=null)
                               {
                            	   medicine_name=request.getParameter("staff_name");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(!medicine_name.trim().equals(""))
                        			{
                        			 sql="Select * from master_medicine_delete_entries where medicine_name='"+medicine_name+"'";
                        			}else{
                           			 sql="Select * from master_medicine_delete_entries ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr>
                        				 <td><%=i%></td>
                        				 <td><%=rs.getString("medicine_name")%></td>
                        				 <td><%=rs.getString("medicine_type")%></td>
                        				 <td><%=rs.getString("medicine_id")%></td>
                        				 <td><%=rs.getString("date")%></td>
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
      
      
     

  
      
      <!-- Modal -->
  <!-- <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="delete_hospital_master" method="post">
      <input type="hidden" name="del_id" id="del_id">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Delete History</h4>
           <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change_modal(1)">Show Table</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(0)">Add New</a>
                                                            </li>

                                                        </ul>
        </div>
        <div class="modal-body" >
        <p>Do you want to delete selected row!</p>       
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
        </form>
      </div>
      /.modal-content
    </div>
    /.modal-dialog
  </div> -->
  <!-- /.modal -->
  
  <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
 					<ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change_modal(1)" >Update</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(0)" style="cursor:pointer">Delete</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(2)" style="cursor:pointer">History</a>
                                                            </li>

                    </ul> 
                       
            </div>
        <div class="modal-body" style="height:350px;overflow:auto">
        <div id="updatestaffmodal">
        
        <form  method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_u" id="s_id_u">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Update Medicine Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Medicine_Name</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="medicine_name" id="medicine_name_u"  class="form-control" placeholder="Medicine Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Package</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="number" name="pack" id="pack_u"  class="form-control" placeholder="Package" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Medicine_Type:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <select name="medicine_type" id="medicine_type_u"   class="form-control"  required="required">
                                                                    <option value="default">Select Medicine Type</option>
                                                                    <option value="Schedule H">Schedule H</option>
                                                                    <option value="Schedule H1">Schedule H1</option>
                                                                    <option value="Other">Other</option>
                                                                    
                                                                    </select>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Company_Name</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="company_name" id="company_name_u"  class="form-control" placeholder="Company Name" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                       
                                                    </div>
                                                    <div class="col-lg-6">
                                                     
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Medicine_Category:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <select name="medicine_category" id="medicine_category_u"   class="form-control"  required="required">
                                                                    <option value="default">Select Medicine Category</option>
                                                                    <%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from medicine_category ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("CatName")%>"><%=rs.getString("CatName")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
                                                                     <option value="OTHER">OTHER</option>
                                                                    
                                                                    </select>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                     <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Unit:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <select name="unit"  id="unit_u"   class="form-control"  required="required">
                                                                    <option value="default">Select Unit</option>
                                                                    <option value="S">S</option>
                                                                    <option value="ML">ML</option>
                                                                    <option value="GM">GM</option>
                                                                    
                                                                    </select>
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">HSN Code</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="text" name="hsn" id="hsn_u"  class="form-control" placeholder="HSN Code" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Low_Stock_Alert</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input type="number" name="low_stock" id="low_stock_u"   class="form-control" placeholder="Low Stock Alert" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                    
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="button" onclick="update_staff_master()" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Update</button>
<!--                                                             <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     <!--@@@@@@@@@@@@@@@ Delete Modal  @@@@@@@@@@@@@@@-->
                     <div id="deletestaffmodal">
        
        <form  action="delete_medicine_master" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_del" id="s_id_del">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Delete Medicine</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <br>
                                                    <p>Do you want to delete whole detail of Medicine <span id="staff_name_del" style="color:yellow;font-size:18px"></span>!</p>
                                                   </div> 
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Yes</button>
<!--                                                        <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     
                     <div id="historystaffmodal">
        
        <form  action="" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_his" id="s_id_his">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>History Staff Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <table id="tablehis" class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                              
                        		 </tbody>
                            </table> </div> 
                                                </div>
                                                
                                                </form>
                     </div>
                     
                     
                     
                     
           </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--           <button type="button" class="btn btn-primary">Save changes</button>
 -->        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  
  
   <!-- Modal -->
  <div class="modal fade" id="clearhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Search Medicine</h4>
        </div>
        <div class="modal-body">
													<div class="row">
													<div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Medicine Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input list="staff-list" type="text" name="medicine_name"  class="form-control" placeholder="Medicine Name"  autocomplete="off">
                                                                <datalist id="staff-list">
  																<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from master_medicine group by medicine_name";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("medicine_name")%>"><%=rs.getString("medicine_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</datalist>
                                                                
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     
                                                     
                                                      </div>   
                                                        
                                   </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Search</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  
 
</div>

    

    <!-- jquery
		============================================ -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
</body>

</html>