<%@page import="commoncontroller.commonclaasses"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="generatedclasses.ConnectionClass"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hospital Master</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="css2/nalika-icon.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="css/main.css">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="css2/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
        
        
         <!-- select js
		============================================ -->
        <link rel="stylesheet" href="selectjs/style.css">
		<script src="selectjs/selectjquery.js"></script>
		<script src="selectjs/choosen.js"></script>
    
    <!--Jquery  -->
    <script>
    $(document).ready(function(){
    	
    	$(".chosen").chosen();

		$("#submit_form").hide();
		$("#deletestaffmodal").hide();
		$("#historystaffmodal").hide();
		$("#show_table_deleted_entries").hide();

    	
    });
    
    </script>
    
    
    
    
    <!--  JavaScript  -->
    <script>
    
    function deletefunc(val){
    	var id=val;
    	var tax=$("#tax_"+id).text();   
    	var tax_percentage=$("#tax_percentage_"+id).val();
    	var tax_name=$("#tax_name_"+id).val();

    	
    	
    	$("#tax_percentage_u").val(tax_percentage);
    	$("#tax_name_u").val(tax_name);
    	
    	
    	
    	$("#s_id_del").val(id);
    	$("#s_id_his").val(id);
    	$("#staff_name_del").text(tax);


    	$("#s_id_u").val(id);
    	$("#myModaldelete").modal();
    }
    
    function update_staff_master()
    {
    	var tax_percentage=$("#tax_percentage_u").val();
    	var tax_name=$("#tax_name_u").val();
    	var id=$("#s_id_u").val();
    	
    	$.ajax({
    		  type: "GET",
    		  url: "TaxMasterServlet",
    		  data: {tax_percentage:tax_percentage, tax_name:tax_name, id:id  },
    		       success: function(data){
    			   //alert(data);
    			    $("#tax_percentage_"+id).val(tax_percentage);   
    		        $("#tax_name_"+id).val(tax_name);
    		        $("#tax_"+id).text(data);
     			   alert("Update Successfully!");

    		       
    		    } 
    		});


    }
    
    function form_change(val){
    	if(val==0)
    		{
    		$("#submit_form").show();
    		$("#show_table").hide();
    		
    		$("#show_table_deleted_entries").hide();

    		}
    	
    	if(val==1)
		{
		$("#submit_form").hide();
		$("#show_table").show();
		$("#show_table_deleted_entries").hide();

		}
    	if(val==2)
		{
		$("#submit_form").hide();
		$("#show_table").hide();
		$("#show_table_deleted_entries").show();

		}
    }
    
    function form_change_modal(val){
    	if(val==1)
    		{
    		$("#updatestaffmodal").show();
    		$("#deletestaffmodal").hide();
    		$("#historystaffmodal").hide();

    		}
    	
    	if(val==0)
		{
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").show();
		$("#historystaffmodal").hide();


		}
    	if(val==2)
		{
    	$("#historystaffmodal").show();
		$("#updatestaffmodal").hide();
		$("#deletestaffmodal").hide();
		var s_id_his=$("#s_id_his").val();
		$.ajax({
  		  type: "POST",
  		  url: "StaffServlet",
  		  data: {id:s_id_his },
  		  dataType: "json",
   		  success: function(json){
  			  alert(json);
  			                    /*   var json = JSON.parse(data);
  			                    alert(json); */
  		    	 /* var json1 = JSON.parse(json);
  		    	 $.each(json1, function(index, element) {
  		    		 var info = element.staff_name;
  		    		 alert(info);
  		    		// console.log(info);
  			   
  		    	 }); */
  		    } 
  		});
		
		//$("#tablehis").append(markup);

		}
    }
    
    
    function clear_history() {
    	$("#clearhistory").modal();
		
	}
    function page_refresh()
    {
    	window.location.href='create_tax_master';
    }
    
    </script>
    <style type="text/css">
    
    .btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 16px;
    line-height: 1.5;
    border-radius: 3px;
}
    </style>
   
</head>

<body>

<%
request.getSession().setAttribute("account_ses", "create_group_ses");
%>
    <jsp:include page="../public/sidebar.jsp"></jsp:include>
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="dashboard"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <jsp:include page="../public/topmenu.jsp"></jsp:include>
            <!-- Mobile Menu start -->
             <jsp:include page="../public/mobile_sidebar.jsp"></jsp:include>
            <jsp:include page="subheader_account.jsp"></jsp:include>
        </div>
        
         <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default">Tax Master</button>
                                         </div>
        </div> 
        
        <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
        <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change(1)" class="cursor-class">Show Table</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(0)" class="cursor-class">Add New</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change(2)" class="cursor-class">Deleted Entries</a>
                                                            </li>

                                                        </ul>
        
        </div>
        <!-- Single pro tab review Start-->
        
         <%
             int count=0;
                 // System.out.println("count==========="+count);
         %>
                                            
                                            
        <div class="single-pro-review-area mt-t-30 mg-b-30" id="submit_form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            
                           
                             
                            
                            <!-- <ul class="tab-review-design">
                                <li class="active"><span>Hospital Master</span></li>
                            </ul> -->
                            <div id="myTabContent" class="tab-content custom-product-edit" >
                                
                                
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="basic-login-inner">
                                            <form action="insert_group_account" method="post" onsubmit="validateform()">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Under Group:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
																<select name="under_group" id="under_group"  class=" form-control color-border chosen">
																	<option value="default">Select Group</option>
																	<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from account_groups ";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("group_name")%>"><%=rs.getString("group_name")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
															</select>                                                                 </div>
                                                            </div>
                                                      </div>
                                                       
                                                    </div>
                                                    <div class="col-lg-6">
                                                     
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Group Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                 <input type="text" name="group_name" class="form-control color-border" placeholder="Group Name" required="required" autocomplete="off">                                                              </div>
                                                            </div>
                                                      </div>
                                                      
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light">Save</button>
                                                            <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
       
        <div class="product-status mg-b-30" id="show_table">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Tax Master</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Tax Name</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               String test_name="";
                               if(request.getParameter("test_name")!=null)
                               {
                            	   test_name=request.getParameter("test_name");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(!test_name.trim().equals(""))
                        			{
                        			 sql="Select * from master_tax where tax='"+test_name+"'";
                        			}else{
                           			 sql="Select * from master_tax ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr onclick="deletefunc(<%=rs.getString("id")%>)">
                        				 <td><%=i%></td>
                        				 <td id="tax_<%=rs.getString("id")%>" style="color:yellow;cursor:pointer"><%=rs.getString("tax")%></td>
                        				 <td id="date_<%=rs.getString("id")%>"><%=rs.getString("date")%></td>
                                         <input type="hidden" id="tax_percentage_<%=rs.getString("id")%>" value="<%=rs.getString("tax_percentage")%>"> 
                                         <input type="hidden" id="tax_name_<%=rs.getString("id")%>" value="<%=rs.getString("tax_name")%>"> 
                                        
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      
      <!--Deleted Entries application  -->
      <div class="product-status mg-b-30" id="show_table_deleted_entries">
            <div class="container-fluid">
                <div class="row">
               
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap">
                         <%
                            if(request.getSession().getAttribute("successmsg")!=null)
                            {
                            String successmsg=request.getSession().getAttribute("successmsg").toString();
                            request.getSession().removeAttribute("successmsg");
                           %>
                            <ul class="tab-review-design">
                                <li class="active"><span><%=successmsg %></span></li>
                            </ul>
                            <% }
                            %>
                                <div style="overflow: auto;">
                           <div class="col-md-2 col-md-3 col-sm-3 col-xs-12 mg-b-15">
                        
                            <h4>Deleted Entries</h4>
                            </div>
                            
                            <div class="col-md-6 col-md-6 col-sm-6 col-xs-12 mg-b-15">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-sm" onclick="clear_history()">Search</button>
                                            
                                          </div>
                                          <div class="btn-group">
                                             <button class="btn btn-default btn-sm" onclick="page_refresh()"><i class="fa fa-refresh"></i> Refresh</button>
                                            
                                          </div>
                                          
                                    </div>
                            <table class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Test Name</th>
                                    <th>Test Price</th>
                                    <th>Test Id</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                               <%
                               if(request.getParameter("staff_name")!=null)
                               {
                            	   test_name=request.getParameter("staff_name");
                               }
                               
                                try {
                                	String sql="";
                                	int i=1;
                        			new ConnectionClass();
                        			Connection con= ConnectionClass.getconnection();
                        			if(!test_name.trim().equals(""))
                        			{
                        			 sql="Select * from master_test_lab_delete_entries where test_name='"+test_name+"'";
                        			}else{
                           			 sql="Select * from master_test_lab_delete_entries ";

                        			}
                        			PreparedStatement pst=con.prepareStatement(sql);
                        			ResultSet rs=pst.executeQuery();
                        			while(rs.next())
                        			{%>
                        				<tr>
                        				 <td><%=i%></td>
                        				 <td><%=rs.getString("test_name")%></td>
                        				 <td><%=rs.getString("test_price")%></td>
                        				 <td><%=rs.getString("test_id")%></td>
                        				 <td><%=rs.getString("date")%></td>
                                        </tr>
                        	
                        			<%i++;}
                        			rs.close();
                        			pst.close();
                        			con.close();
                        		}catch (Exception e) {
                        			e.printStackTrace();
                        		}
                        		 %>
                        		 </tbody>
                            </table>
                            
                            
                            <div class="custom-pagination">
							<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
      </div>
      
      <jsp:include page="../public/footer.jsp"></jsp:include>
      
      
     

  
      
      <!-- Modal -->
  <!-- <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="delete_hospital_master" method="post">
      <input type="hidden" name="del_id" id="del_id">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Delete History</h4>
           <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change_modal(1)">Show Table</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(0)">Add New</a>
                                                            </li>

                                                        </ul>
        </div>
        <div class="modal-body" >
        <p>Do you want to delete selected row!</p>       
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
        </form>
      </div>
      /.modal-content
    </div>
    /.modal-dialog
  </div> -->
  <!-- /.modal -->
  
  <div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
 					<ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" onclick="form_change_modal(1)" >Update</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(0)" style="cursor:pointer">Delete</a>
                                                            </li>
                                                            <li><a data-toggle="tab" onclick="form_change_modal(2)" style="cursor:pointer">History</a>
                                                            </li>

                    </ul> 
                       
            </div>
        <div class="modal-body" style="height:350px;overflow:auto">
        <div id="updatestaffmodal">
        
        <form  method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_u" id="s_id_u">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Update Tax Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                    
                                                      <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Tax_Percentage(%):</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-8">
                                                                <div class="input-mark-inner">
                                                                    <input type="number" id="tax_percentage_u"  class="form-control" placeholder="Tax Percentage" required="required" autocomplete="off">
                                                                </div>
                                                            </div>
                                                      </div>
                                                       
                                                    </div>
                                                    <div class="col-lg-6">
                                                     
                                                       <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group-inner">
														            <label class="form-label">Tax Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                 <select id="tax_name_u" class=" form-control">
																	<option value="default">Select Tax Name</option>
																	<option value="GST">GST</option>
																	<option value="IGST">IGST</option>
																	
																</select>                                                                </div>
                                                            </div>
                                                      </div>
                                                      
                                                      
                                                    
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="button" onclick="update_staff_master()" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Update</button>
<!--                                                             <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     <!--@@@@@@@@@@@@@@@ Delete Modal  @@@@@@@@@@@@@@@-->
                     <div id="deletestaffmodal">
        
        <form  action="delete_test_lab_master" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_del" id="s_id_del">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>Delete Test Lab</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <br>
                                                    <p>Do you want to delete whole detail of Test Lab <span id="staff_name_del" style="color:yellow;font-size:18px"></span>!</p>
                                                   </div> 
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-adress">
                                                            <button type="submit" class="btn bg-btn-cl waves-effect waves-light" style="color:white">Yes</button>
<!--                                                        <button type="reset" class="btn bg-btn-cl waves-effect waves-light">Reset</button>
 -->                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                     </div>
                     
                     
                     <div id="historystaffmodal">
        
        <form  action="" method="post" onsubmit="validateform()">
        <input type="hidden" name="s_id_his" id="s_id_his">
         <div class="row">
        <div class="col-md-12">       
           <h4 class="modal-title"><u>History Staff Master</u></h4>
           </div>
        </div>  
                                                <div class="row">
                                                 <div class="col-md-12">
                                                <table id="tablehis" class="table table-bordered">
   							 <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Staff_Name</th>
                                    <th>Staff_Id</th>
                                    <th>Date_Of_Birth</th>
                                    <th>Age</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Email_id</th>
                                    <th>Mobile_No</th>
                                    <th>Blood_Group</th>
                                    <th>Qualification</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                              
                        		 </tbody>
                            </table> </div> 
                                                </div>
                                                
                                                </form>
                     </div>
                     
                     
                     
                     
           </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--           <button type="button" class="btn btn-primary">Save changes</button>
 -->        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  
  
   <!-- Modal -->
  <div class="modal fade" id="clearhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Search Tax</h4>
        </div>
        <div class="modal-body">
													<div class="row">
													<div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="input-mark-inner">
														            <label class="form-label">Tax Name:</label>                                                       
													            </div>
                                                             </div>
                                                            <div class="col-md-9">
                                                                <div class="input-mark-inner">
                                                                    <input list="staff-list" type="text" name="test_name"  class="form-control" placeholder="Test Name"  autocomplete="off">
                                                                <datalist id="staff-list">
  																<%
																	try{
																		new ConnectionClass();
																		Connection con= ConnectionClass.getconnection();
																		String sql="Select * from master_tax group by tax";
																		PreparedStatement pst=con.prepareStatement(sql);
																		ResultSet rs=pst.executeQuery();
																		while(rs.next())
																		{%>
																		<option value="<%=rs.getString("tax")%>"><%=rs.getString("tax")%></option>
																			
																		<%}
																		rs.close();
																		pst.close();
																		con.close();
																	}catch(Exception e){
																		e.printStackTrace();
																	}
																	
																	%>
																</datalist>
                                                                
                                                                </div>
                                                            </div>
                                                     </div>
                                                     
                                                     
                                                     
                                                      </div>   
                                                        
                                   </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Search</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  
 
</div>

    

    <!-- jquery
		============================================ -->
<!--     <script src="js/vendor/jquery-1.12.4.min.js"></script> -->
    <!-- bootstrap JS
		============================================ -->
    <script src="js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS
		============================================ -->
    <script src="js/sparkline/jquery.sparkline.min.js"></script>
    <script src="js/sparkline/jquery.charts-sparkline.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="js/calendar/moment.min.js"></script>
    <script src="js/calendar/fullcalendar.min.js"></script>
    <script src="js/calendar/fullcalendar-active.js"></script>
    <!-- tab JS
		============================================ -->
    <script src="js/tab.js"></script>
    <!-- payment away JS
		============================================ -->
    <script src="js/card.js"></script>
    <script src="js/jquery.payform.min.js"></script>
    <script src="js/e-payment.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="js/main.js"></script>
</body>

</html>