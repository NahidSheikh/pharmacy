package inventorycontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_purchase_order_inventory extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String vendor_name;
	private String mailing_address;
	private String terms;
	private String date;
	private String to_date;
	private String bill_no;
	private String total_cgst;
	private String total_tax;
	private String total_sgst;
	private String sub_total;
	private String total_igst;
	private String net_amount;
	private String round_off;
	private String memo;

	
	
	
	
	private String m_id[];
	private String m_name[];
	private String m_company[];
	private String m_pack[];
	private String m_type[];
	private String m_category[];
	private String m_hsn[];

	
	private String m_rate[];
	private String m_quantity[];
	private String m_free_quantity[];
	private String m_amount[];
	private String m_tax_name[];
	private String m_tax_amount[];
	private String m_cgst[];
	private String m_sgst[];
	private String m_igst[];
	private String m_total_amount[];
	private String m_exp_date[];
	


	

	public String getVendor_name() {
		return vendor_name;
	}

	public void setVendor_name(String vendor_name) {
		this.vendor_name = vendor_name;
	}

	public String getMailing_address() {
		return mailing_address;
	}

	public void setMailing_address(String mailing_address) {
		this.mailing_address = mailing_address;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTo_date() {
		return to_date;
	}

	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}

	public String getBill_no() {
		return bill_no;
	}

	public void setBill_no(String bill_no) {
		this.bill_no = bill_no;
	}

	public String getTotal_cgst() {
		return total_cgst;
	}

	public void setTotal_cgst(String total_cgst) {
		this.total_cgst = total_cgst;
	}

	public String getTotal_tax() {
		return total_tax;
	}

	public void setTotal_tax(String total_tax) {
		this.total_tax = total_tax;
	}

	public String getTotal_sgst() {
		return total_sgst;
	}

	public void setTotal_sgst(String total_sgst) {
		this.total_sgst = total_sgst;
	}

	public String getSub_total() {
		return sub_total;
	}

	public void setSub_total(String sub_total) {
		this.sub_total = sub_total;
	}

	public String getTotal_igst() {
		return total_igst;
	}

	public void setTotal_igst(String total_igst) {
		this.total_igst = total_igst;
	}

	public String getNet_amount() {
		return net_amount;
	}

	public void setNet_amount(String net_amount) {
		this.net_amount = net_amount;
	}

	public String getRound_off() {
		return round_off;
	}

	public void setRound_off(String round_off) {
		this.round_off = round_off;
	}
	
	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String[] getM_id() {
		return m_id;
	}

	public void setM_id(String[] m_id) {
		this.m_id = m_id;
	}

	public String[] getM_name() {
		return m_name;
	}

	public void setM_name(String[] m_name) {
		this.m_name = m_name;
	}

	
	public String[] getM_company() {
		return m_company;
	}

	public void setM_company(String[] m_company) {
		this.m_company = m_company;
	}

	public String[] getM_pack() {
		return m_pack;
	}

	public void setM_pack(String[] m_pack) {
		this.m_pack = m_pack;
	}

	public String[] getM_type() {
		return m_type;
	}

	public void setM_type(String[] m_type) {
		this.m_type = m_type;
	}

	public String[] getM_category() {
		return m_category;
	}

	public void setM_category(String[] m_category) {
		this.m_category = m_category;
	}

	public String[] getM_hsn() {
		return m_hsn;
	}

	public void setM_hsn(String[] m_hsn) {
		this.m_hsn = m_hsn;
	}

	public String[] getM_rate() {
		return m_rate;
	}

	public void setM_rate(String[] m_rate) {
		this.m_rate = m_rate;
	}

	public String[] getM_quantity() {
		return m_quantity;
	}

	public void setM_quantity(String[] m_quantity) {
		this.m_quantity = m_quantity;
	}

	public String[] getM_free_quantity() {
		return m_free_quantity;
	}

	public void setM_free_quantity(String[] m_free_quantity) {
		this.m_free_quantity = m_free_quantity;
	}

	public String[] getM_amount() {
		return m_amount;
	}

	public void setM_amount(String[] m_amount) {
		this.m_amount = m_amount;
	}

	public String[] getM_tax_name() {
		return m_tax_name;
	}

	public void setM_tax_name(String[] m_tax_name) {
		this.m_tax_name = m_tax_name;
	}

	public String[] getM_tax_amount() {
		return m_tax_amount;
	}

	public void setM_tax_amount(String[] m_tax_amount) {
		this.m_tax_amount = m_tax_amount;
	}

	public String[] getM_cgst() {
		return m_cgst;
	}

	public void setM_cgst(String[] m_cgst) {
		this.m_cgst = m_cgst;
	}

	public String[] getM_sgst() {
		return m_sgst;
	}

	public void setM_sgst(String[] m_sgst) {
		this.m_sgst = m_sgst;
	}

	public String[] getM_igst() {
		return m_igst;
	}

	public void setM_igst(String[] m_igst) {
		this.m_igst = m_igst;
	}

	public String[] getM_total_amount() {
		return m_total_amount;
	}

	public void setM_total_amount(String[] m_total_amount) {
		this.m_total_amount = m_total_amount;
	}

	public String[] getM_exp_date() {
		return m_exp_date;
	}

	public void setM_exp_date(String[] m_exp_date) {
		this.m_exp_date = m_exp_date;
	}

	public String getTodaystring() {
		return todaystring;
	}

	public void setTodaystring(String todaystring) {
		this.todaystring = todaystring;
	}

	
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		String ven_split[]=vendor_name.split("/");
		vendor_name=ven_split[0];
		String vendor_id=ven_split[1];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into pharmacy_purchase_order (vendor_name,vendor_id,mailing_address,terms,from_date,to_date,"
					+ "bill_no,memo,total_cgst,total_sgst,total_igst,round_off,total_tax,sub_total,net_amount,date)"
					+ " Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(vendor_name));
			pst.setString(2,vendor_id);
			pst.setString(3,mailing_address);
			pst.setString(4,terms);
			pst.setString(5,date);
			pst.setString(6,to_date);
			pst.setString(7,bill_no);
			pst.setString(8,memo);
			pst.setString(9,total_cgst);
			pst.setString(10,total_sgst);
			pst.setString(11,total_igst);
			pst.setString(12,round_off);
			pst.setString(13,total_tax);
			pst.setString(14,sub_total);
			pst.setString(15,net_amount);
			pst.setString(16,todaystring);
			pst.executeUpdate();
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			rs.close();
			pst.close();
			con.close();
			add_medicine();
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
				
		return ret;
	}
	
	public void add_medicine()
	{
		if(m_name!=null) {
		try {
			int i=0;
			for(i=0;i<m_name.length;i++) {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into pharmacy_purchase_order_list"
					+ " (ref_id,m_id,m_name,m_rate,m_quantity,m_free_quantity,"
					+ "m_amount,m_tax_name,m_tax_amount,m_cgst,m_sgst,m_igst,m_total_amount,"
					+ "m_exp_date,date,m_company,m_pack,m_type,m_category,m_hsn)"
					+ " Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setLong(1,lastkey);
			pst.setString(2,m_id[i]);
			pst.setString(3,m_name[i]);
			pst.setString(4,m_rate[i]);
			pst.setString(5,m_quantity[i]);
			pst.setString(6,m_free_quantity[i]);
			pst.setString(7,m_amount[i]);
			pst.setString(8,m_tax_name[i]);
			pst.setString(9,m_tax_amount[i]);
			pst.setString(10,m_cgst[i]);
			pst.setString(11,m_sgst[i]);
			pst.setString(12,m_igst[i]);
			pst.setString(13,m_total_amount[i]);
			pst.setString(14,m_exp_date[i]);
			pst.setString(15,todaystring);
			pst.setString(16,m_company[i]);
			pst.setString(17,m_pack[i]);
			pst.setString(18,m_type[i]);
			pst.setString(19,m_category[i]);
			pst.setString(20,m_hsn[i]);

			pst.executeUpdate();
			pst.close();
			con.close();
			}
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		}
	}

}
