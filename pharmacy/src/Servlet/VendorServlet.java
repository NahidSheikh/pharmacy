package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.WordUtils;

import com.google.gson.Gson;

import displaycontroller.DisplayStaffMaster;
import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class VendorServlet
 */
@WebServlet("/VendorServlet")
public class VendorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VendorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//PrintWriter out = response.getWriter();

		String vendor_name_u=request.getParameter("vendor_name_u");
		String state_u=request.getParameter("state_u");
		String mobile_no_first_u=request.getParameter("mobile_no_first_u");
		String mobile_no_second_u=request.getParameter("mobile_no_second_u");
		String email_first_u=request.getParameter("email_first_u");
		String email_second_u=request.getParameter("email_second_u");
		String website_u=request.getParameter("website_u");
		String fax_no_u=request.getParameter("fax_no_u");
		String pan_no_u=request.getParameter("pan_no_u");
		String registration_no_u=request.getParameter("registration_no_u");
		String gst_no_u=request.getParameter("gst_no_u");
		
		String bank_name_u=request.getParameter("bank_name_u");
		String ifsc_no_u=request.getParameter("ifsc_no_u");
		String account_no_u=request.getParameter("account_no_u");
		String branch_no_u=request.getParameter("branch_no_u");
		String opening_balance_u=request.getParameter("opening_balance_u");
		String opening_date_u=request.getParameter("opening_date_u");
		String vendor_address_u=request.getParameter("vendor_address_u");
		String balance_type_u=request.getParameter("balance_type_u");
		String id=request.getParameter("id");
		
		//String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

		try {
			//master_staff_history(id);
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_vendor set vendor_name=?,state=?,mobile_no_second=?,email_second=?,fax_no=?,"
					+ "registration_no=?,bank_name=?,account_no=?,opening_balance=?,opening_date=?,vendor_address=?,"
					+ "mobile_no_first=?,email_first=?,website=?,pan_no=?,gst_no=?,ifsc_no=?,branch_no=?,balance_type=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(vendor_name_u));
			pst.setString(2,state_u);
			pst.setString(3,mobile_no_second_u);
			pst.setString(4,email_second_u);
			pst.setString(5,fax_no_u);
			pst.setString(6,registration_no_u);
			pst.setString(7,bank_name_u);
			pst.setString(8,account_no_u);
			pst.setString(9,opening_balance_u);
			pst.setString(10,opening_date_u);
			pst.setString(11,vendor_address_u);
			pst.setString(12,mobile_no_first_u);
			pst.setString(13,email_first_u);
			pst.setString(14,website_u);
			pst.setString(15,pan_no_u);
			pst.setString(16,gst_no_u);
			pst.setString(17,ifsc_no_u);
			pst.setString(18,branch_no_u);
			pst.setString(19,balance_type_u);
			pst.setString(20,id);
			pst.executeUpdate();
			
			pst.close();
			con.close();
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		String deb="";
		String cre="";

		
		if(balance_type_u.trim().equals("Debit"))
		{
			deb=opening_balance_u;
			cre="-";
		}else {
			deb="-";
			cre=opening_balance_u;
		}
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update hospital_payment set from_name=?,debit=?,credit=?,payment_date=? where ref_id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(vendor_name_u));
			pst.setString(2,deb);
			pst.setString(3,cre);
			pst.setString(4,opening_date_u);
			pst.setString(5,id);
			pst.executeUpdate();
			
			pst.close();
			con.close();
		
		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	
	}

	
	
	
	
}
