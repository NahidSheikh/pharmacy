package Servlet;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class GeneratePasswordServlet
 */
@WebServlet("/GeneratePasswordServlet")
public class GeneratePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GeneratePasswordServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String staff_name=request.getParameter("staff_name");
		String id=request.getParameter("id");
		//System.out.println("id============="+id);
		String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
		String dat[]=todaystring.split("/");
		String str = String.format("%02d",Integer.parseInt(id));
		String pass=dat[0]+dat[1]+dat[2]+str;
		String user="";
		if(staff_name.contains(" ")) {
		String use[]=staff_name.split(" ");
		user=use[0];
		}else {
			user=staff_name;	
		}
		user=user+String.format("%01d", Integer.parseInt(id));	
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_staff set username=?,password=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,user);
			pst.setString(2,pass);
			pst.setString(3,id);
			pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		 String jsondata = new Gson().toJson(user+"/"+pass);
	        response.getWriter().write(jsondata);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id=request.getParameter("id");

		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_staff set username=?,password=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,"-");
			pst.setString(2,"-");
			pst.setString(3,id);
			pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception e) {
			
			e.printStackTrace();
		}	
		
	}

}
