package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import displaycontroller.displayMedicine;
import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class InjectionServlet
 */
@WebServlet("/InjectionServlet")
public class InjectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InjectionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String ser_injection_name=request.getParameter("ser_injection_name");
		//System.out.println("ser_medcine_name=========="+ser_injection_name);
        ArrayList<Object> a=new ArrayList<Object>();
		try {
        	String sql="";
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			sql="select * from master_medicine where medicine_name like '%"+ser_injection_name+"%' and medicine_type='Injection'";
			PreparedStatement pst=con.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
				displayMedicine dsm=new displayMedicine();
				dsm.setMedicine_name(rs.getString("medicine_name"));
				dsm.setId(rs.getString("id"));
				//System.out.println("============="+dsm.getId());
				a.add(dsm);

			}
			rs.close();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
        String jsondata = new Gson().toJson(a);
        response.getWriter().write(jsondata);
	
	}

}
