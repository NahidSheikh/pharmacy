package Servlet;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.text.WordUtils;
import com.google.gson.Gson;
import displaycontroller.displayMedicine;
import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class MedicineServlet
 */
@WebServlet("/MedicineServlet")
public class MedicineServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MedicineServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String medicine_name=request.getParameter("medicine_name");
		String pack=request.getParameter("pack");
		String medicine_type=request.getParameter("medicine_type");
		String company_name=request.getParameter("company_name");
		String medicine_category=request.getParameter("medicine_category");
		String unit=request.getParameter("unit");
		String hsn=request.getParameter("hsn");
		String low_stock=request.getParameter("low_stock");
		String id=request.getParameter("id");
		String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

		try {
			//master_staff_history(id);
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_medicine set medicine_name=?,pack=?,medicine_type=?,company_name=?,"
					+ "medicine_category=?,unit=?,hsn=?,low_stock=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(medicine_name));
			pst.setString(2,pack);
			pst.setString(3,medicine_type);
			pst.setString(4,WordUtils.capitalizeFully(company_name));
			pst.setString(5,medicine_category);
			pst.setString(6,unit);
			pst.setString(7,hsn);
			pst.setString(8,low_stock);
			pst.setString(9,id);
			pst.executeUpdate();
			
			pst.close();
			con.close();
		
		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String ser_medcine_name=request.getParameter("ser_medcine_name");
		//System.out.println("ser_medcine_name=========="+ser_medcine_name);
        ArrayList<Object> a=new ArrayList<Object>();
		try {
        	String sql="";
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			sql="select * from master_medicine where medicine_name like '%"+ser_medcine_name+"%' and medicine_type!='Injection'";
			PreparedStatement pst=con.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
				displayMedicine dsm=new displayMedicine();
				dsm.setMedicine_name(rs.getString("medicine_name"));
				dsm.setId(rs.getString("id"));
				//System.out.println("============="+dsm.getId());
				a.add(dsm);

			}
			rs.close();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
        String jsondata = new Gson().toJson(a);
        response.getWriter().write(jsondata);
	
	}

	
	
	public void master_staff_history(String id)
	{
			/************* history of hospital ****************/
	      String test_name="";
	      String test_price="";
	      String date="";
	      String test_id="";



			try {
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				String sql="Select * from master_medicine where id=?";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.setString(1,id);
				ResultSet rs=pst.executeQuery();
				while(rs.next())
				{
					test_name=rs.getString("medicine_name");
					test_price=rs.getString("medicine_type");
					date=rs.getString("date");
					test_id=rs.getString("medicine_id");
					
				}
				rs.close();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				String sql="Insert into master_medicine_history (medicine_name,medicine_type,medicine_id,ref_id,date) Values(?,?,?,?,?)";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.setString(1,WordUtils.capitalizeFully(test_name));
				pst.setString(2,test_price);
				pst.setString(3,test_id);
				pst.setString(4,id);
				pst.setString(5,date);
				pst.executeUpdate();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
}
