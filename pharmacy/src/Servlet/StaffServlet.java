package Servlet;
import java.io.IOException;
import com.google.gson.Gson;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.WordUtils;

import displaycontroller.DisplayStaffMaster;
import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class StaffServlet
 */
@WebServlet("/StaffServlet")
public class StaffServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StaffServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();

		String staff_name=request.getParameter("staff_name");
		String dob=request.getParameter("dob");
		String age=request.getParameter("age");
		String address=request.getParameter("address");
		String state=request.getParameter("state");
		String email_id=request.getParameter("email_id");
		String mobile_no=request.getParameter("mobile_no");
		String qualification=request.getParameter("qualification");
		String designation=request.getParameter("designation");
		String department=request.getParameter("department");
		String blood_group=request.getParameter("blood_group");
		String id=request.getParameter("id");
		String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

		try {
			master_staff_history(id);
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_staff set staff_name=?,address=?,email_id=?,blood_group=?,designation=?,"
					+ "dob=?,age=?,state=?,mobile_no=?,qualification=?,department=?,"
					+ "date=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(staff_name));
			pst.setString(2,address);
			pst.setString(3,email_id);
			pst.setString(4,blood_group);
			pst.setString(5,designation);
			pst.setString(6,dob);
			pst.setString(7,age);
			pst.setString(8,state);
			pst.setString(9,mobile_no);
			pst.setString(10,qualification);
			pst.setString(11,department);
			pst.setString(12,todaystring);
			pst.setString(13,id);
			pst.executeUpdate();
			
			pst.close();
			con.close();
		
		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id=request.getParameter("id");
       ArrayList<Object> a=new ArrayList<Object>();
		try {
        	String sql="";
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			sql="select * from master_staff_history where ref_id=? ";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,id);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
			DisplayStaffMaster dsm=new DisplayStaffMaster();
			dsm.setStaff_name(rs.getString("staff_name"));
			dsm.setAddress(rs.getString("address"));
			dsm.setEmail_id(rs.getString("email_id"));
			dsm.setBlood_group(rs.getString("blood_group"));
			dsm.setDesignation(rs.getString("designation"));
			dsm.setDob(rs.getString("dob"));
			dsm.setAge(rs.getString("age"));
			dsm.setState(rs.getString("state"));
			dsm.setMobile_no(rs.getString("mobile_no"));
			dsm.setQualification(rs.getString("qualification"));
			dsm.setDepartment(rs.getString("department"));
			dsm.setDate(rs.getString("date"));
			dsm.setStaff_id(rs.getString("staff_id"));
			a.add(dsm);

			}
			rs.close();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
        String jsondata = new Gson().toJson(a);

       // out.write(json);
        response.getWriter().write(jsondata);

	   // response.getWriter().print(a);

	
	}

	
	
	/* @@@@@@@@@@ @@@@@@@@@@@@@@ */
	
	
public void master_staff_history(String id)
{
		/************* history of hospital ****************/
String staff_name="";
String address="";
String email_id="";
String blood_group="";
String designation="";
String dob="";
String age="";
String state="";
String mobile_no="";
String qualification="";
String department="";
String date="";
String staff_id="";



		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Select * from master_staff where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,id);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
				staff_name=rs.getString("staff_name");
				address=rs.getString("address");
				email_id=rs.getString("email_id");
				blood_group=rs.getString("blood_group");
				designation=rs.getString("designation");
				dob=rs.getString("dob");
				age=rs.getString("age");
				state=rs.getString("state");
				mobile_no=rs.getString("mobile_no");
				qualification=rs.getString("qualification");
				department=rs.getString("department");
				date=rs.getString("date");
				staff_id=rs.getString("staff_id");
				
			}
			rs.close();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_staff_history (staff_name,address,email_id,blood_group,designation,"
					+ "dob,age,state,mobile_no,qualification,department,"
					+ "date,ref_id,staff_id) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(staff_name));
			pst.setString(2,address);
			pst.setString(3,email_id);
			pst.setString(4,blood_group);
			pst.setString(5,designation);
			pst.setString(6,dob);
			pst.setString(7,age);
			pst.setString(8,state);
			pst.setString(9,mobile_no);
			pst.setString(10,qualification);
			pst.setString(11,department);
			pst.setString(12,date);
			pst.setString(13,id);
			pst.setString(14,staff_id);
			pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
