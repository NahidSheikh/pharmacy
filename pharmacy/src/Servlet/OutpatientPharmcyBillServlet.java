package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import displaycontroller.DisplayBatchId;
import displaycontroller.DisplayMedicineInfo;
import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class OutpatientPharmcyBillServlet
 */
@WebServlet("/OutpatientPharmcyBillServlet")
public class OutpatientPharmcyBillServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OutpatientPharmcyBillServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String m_name=request.getParameter("m_name");
		String m_id=request.getParameter("med_id");

		String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

       	String date_split[]=todaystring.split("/");
       	String mm_c=date_split[1];
       	String yyyy_c=date_split[2];
       	
       	ArrayList a=new ArrayList();
			try {
	        	String sql="";
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				sql="select * from pharmacy_medicine_inward_order_list where m_name='"+m_name+"' and m_id='"+m_id+"' ";
				PreparedStatement pst=con.prepareStatement(sql);
				ResultSet rs=pst.executeQuery();
				while(rs.next())
				{
					String expiry_date[]=rs.getString("m_exp_date").split("/");
					String mm_e=expiry_date[0];
					String yyyy_e=expiry_date[1];
					
					if(Integer.parseInt(yyyy_e)>=Integer.parseInt(yyyy_c) && Integer.parseInt(mm_e)>=Integer.parseInt(mm_c) && Integer.parseInt(rs.getString("pending_quantity"))>0)
					{
						
							
								DisplayBatchId d=new DisplayBatchId();
								d.setId(rs.getString("id"));
								d.setBatch_id(rs.getString("m_batch_id"));
								d.setExp_date(rs.getString("m_exp_date"));
								d.setPend_quantity(rs.getString("pending_quantity"));
								a.add(d);

					}

				}
				rs.close();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
	        String jsondata = new Gson().toJson(a);
	        response.getWriter().write(jsondata);
	}
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String med_id=request.getParameter("med_id");
		String medicine_name="";
		String pack="";
		String medicine_type="";
		String company_name="";
		String medicine_category="";
		String unit="";
		String hsn="";
		String mrp="";
		String e_date="";
		String b_id="";
		String tax="";



		ArrayList<Object> a=new ArrayList<Object>();
			try {
	        	String sql="";
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				sql="select * from pharmacy_medicine_inward_order_list where id='"+med_id+"' ";
				PreparedStatement pst=con.prepareStatement(sql);
				ResultSet rs=pst.executeQuery();
				if(rs.next())
				{
					DisplayMedicineInfo d=new DisplayMedicineInfo();
					d.setMedicine_name(rs.getString("m_name"));
					d.setPack(rs.getString("m_pack"));
					d.setMedicine_type(rs.getString("m_type"));
					d.setCompany_name(rs.getString("m_company"));
					d.setMedicine_category(rs.getString("m_category"));
					d.setUnit(rs.getString("m_only_pack"));
					d.setHsn(rs.getString("m_hsn"));
					d.setMrp(rs.getString("m_mrp"));
					d.setE_date(rs.getString("m_exp_date"));
					d.setB_id(rs.getString("m_batch_id"));
					d.setTax(rs.getString("m_tax_name"));
					d.setId(rs.getString("id"));
					a.add(d);
					
					
					
					
					

				}
				rs.close();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			String s=medicine_name+":"+pack+":"+medicine_type+":"+company_name+":"+medicine_category+":"+unit+":"+hsn;
	        String jsondata = new Gson().toJson(a);
	        response.getWriter().write(jsondata);
	
	}

}
