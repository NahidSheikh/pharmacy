package Servlet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import generatedclasses.ConnectionClass;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;


/**
 * Servlet implementation class ExcelServlet
 */
@WebServlet("/ExcelServlet")
public class ExcelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExcelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	 // System.out.println("NAHID===========");
	  
	  try {
		
		  XSSFWorkbook workbook = new XSSFWorkbook();
		  XSSFSheet sheet = workbook.createSheet("Reviews");
		  Row headerRow = sheet.createRow(0);
		  Cell headerCell = headerRow.createCell(0);
		  headerCell.setCellValue("Sr. No");
		  headerCell = headerRow.createCell(1);
		  headerCell.setCellValue("Disease Name");
		  headerCell = headerRow.createCell(2);
		  headerCell.setCellValue("Date");
		  
		  new ConnectionClass();
		  Connection con= ConnectionClass.getconnection();
		  String sql="select * from master_disease";
		  PreparedStatement pst=con.prepareStatement(sql);
		  ResultSet rs=pst.executeQuery();
		  int i=1;
		  while(rs.next())
		  {
			  Row row = sheet.createRow(i);
			  Cell cell = row.createCell(0);
	          cell.setCellValue(i);
	          cell = row.createCell(1);
	          cell.setCellValue(rs.getString("disease_name"));
	          cell = row.createCell(2);
	          cell.setCellValue(rs.getString("date"));
	          i++;
		  }
		  rs.close();
		  pst.close();
		  con.close();
		  String excelFilePath = "D:/Reviews-export.xlsx";

		  FileOutputStream outputStream = new FileOutputStream(excelFilePath);
          workbook.write(outputStream);
          workbook.close();
		  
	  }catch(Exception e) {
		  e.printStackTrace();
	  }
	  
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
