package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.WordUtils;

import com.google.gson.Gson;

import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class OutsidePatientRegistration
 */
@WebServlet("/OutsidePatientRegistration")
public class OutsidePatientRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OutsidePatientRegistration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	long lastkey=0;
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	String patient_id="";

	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String patient_name=request.getParameter("patient_name");
		String mobile_no=request.getParameter("mobile_no");
		String gender=request.getParameter("gender");
		String address=request.getParameter("address");

		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into patient_registration (patient_name,mobile_no,gender,address,date)"
					+ " Values(?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(patient_name));
			pst.setString(2,mobile_no);
			pst.setString(3,gender);
			pst.setString(4,address);

			pst.setString(5,todaystring);
			pst.executeUpdate();
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			rs.close();
			pst.close();
			con.close();
			generate_id();
		}catch (Exception e) {
			e.printStackTrace();
		}
			
		String result="";
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="select * from patient_registration where id='"+lastkey+"'";
			PreparedStatement pst=con.prepareStatement(sql);
			
			ResultSet rs=pst.executeQuery();
			if(rs.next())
			{
				result=	rs.getString("patient_name")+"/"+rs.getString("mobile_no")+"/"+rs.getString("patient_id");	
			}
			rs.close();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		String jsondata = new Gson().toJson(result);
        response.getWriter().write(result);
			
	}		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	public void generate_id()
	{
		String year[]=todaystring.split("/");
		patient_id="Outside-"+lastkey+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update patient_registration set patient_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,patient_id);
			pst.setLong(2,lastkey);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
