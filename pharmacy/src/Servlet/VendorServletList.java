package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import displaycontroller.DisplayStaffMaster;
import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class VendorServletList
 */
@WebServlet("/VendorServletList")
public class VendorServletList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VendorServletList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String vendor_id=request.getParameter("vendor_id");
		String mailing_address="";
			try {
	        	String sql="";
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				sql="select * from master_vendor where vendor_id='"+vendor_id+"' ";
				PreparedStatement pst=con.prepareStatement(sql);
				ResultSet rs=pst.executeQuery();
				if(rs.next())
				{
					mailing_address=rs.getString("vendor_address");

				}
				rs.close();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
	        String jsondata = new Gson().toJson(mailing_address);
	        response.getWriter().write(jsondata);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String med_id=request.getParameter("med_id");
		String medicine_name="";
		String pack="";
		String medicine_type="";
		String company_name="";
		String medicine_category="";
		String unit="";
		String hsn="";
		ArrayList<Object> a=new ArrayList<Object>();
			try {
	        	String sql="";
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				sql="select * from master_medicine where id='"+med_id+"' ";
				PreparedStatement pst=con.prepareStatement(sql);
				ResultSet rs=pst.executeQuery();
				if(rs.next())
				{
					medicine_name=rs.getString("medicine_name");
					pack=rs.getString("pack");
					medicine_type=rs.getString("medicine_type");
					company_name=rs.getString("company_name");
					medicine_category=rs.getString("medicine_category");
					unit=rs.getString("unit");
					hsn=rs.getString("hsn");

				}
				rs.close();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			String s=medicine_name+":"+pack+":"+medicine_type+":"+company_name+":"+medicine_category+":"+unit+":"+hsn;
	        String jsondata = new Gson().toJson(s);
	        response.getWriter().write(jsondata);
	
	}

}
