package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.WordUtils;

import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class TaxMasterServlet
 */
@WebServlet("/TaxMasterServlet")
public class TaxMasterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaxMasterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
            PrintWriter out=response.getWriter();
		String tax_percentage=request.getParameter("tax_percentage");
		String tax_name=request.getParameter("tax_name");
		String id=request.getParameter("id");
		String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        String tax="";
		try {
			//master_staff_history(id);
			
			tax=tax_name+"@"+tax_percentage+"%";
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_tax set tax_percentage=?,tax_name=?,tax=?,date=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,tax_percentage);
			pst.setString(2,tax_name);
			pst.setString(3,tax);
			pst.setString(4,todaystring);
			pst.setString(5,id);
			pst.executeUpdate();
			
			pst.close();
			con.close();
			out.print(tax);
		
		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
