package Servlet;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.text.WordUtils;
import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class TestLabServlet
 */
@WebServlet("/TestLabServlet")
public class TestLabServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestLabServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String lab_name=request.getParameter("lab_name");
		String lab_price=request.getParameter("lab_price");
		String id=request.getParameter("id");
		String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

		try {
			master_staff_history(id);
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_test_lab set test_name=?,test_price=?,date=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(lab_name));
			pst.setString(2,lab_price);
			pst.setString(3,todaystring);
			pst.setString(4,id);
			pst.executeUpdate();
			
			pst.close();
			con.close();
		
		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	
	
	public void master_staff_history(String id)
	{
			/************* history of hospital ****************/
	      String test_name="";
	      String test_price="";
	      String date="";
	      String test_id="";



			try {
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				String sql="Select * from master_test_lab where id=?";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.setString(1,id);
				ResultSet rs=pst.executeQuery();
				while(rs.next())
				{
					test_name=rs.getString("test_name");
					test_price=rs.getString("test_price");
					date=rs.getString("date");
					test_id=rs.getString("test_id");
					
				}
				rs.close();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				String sql="Insert into master_test_lab_history (test_name,test_price,test_id,ref_id,date) Values(?,?,?,?,?)";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.setString(1,WordUtils.capitalizeFully(test_name));
				pst.setString(2,test_price);
				pst.setString(3,test_id);
				pst.setString(4,id);
				pst.setString(5,date);
				pst.executeUpdate();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
}
