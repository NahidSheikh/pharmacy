package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.WordUtils;

import com.google.gson.Gson;

import displaycontroller.DisplayStaffMaster;
import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class OpdLabServlet
 */
@WebServlet("/OpdLabServlet")
public class OpdLabServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OpdLabServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();

		String lab_name=request.getParameter("lab_name");
		String lab_price=request.getParameter("lab_price");
		String id=request.getParameter("id");
		String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

		try {
			master_staff_history(id);
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_opd_lab set lab_name=?,lab_price=?,date=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(lab_name));
			pst.setString(2,lab_price);
			pst.setString(3,todaystring);
			pst.setString(4,id);
			pst.executeUpdate();
			
			pst.close();
			con.close();
		
		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id=request.getParameter("id");
       ArrayList<Object> a=new ArrayList<Object>();
		try {
        	String sql="";
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			sql="select * from master_staff_history where ref_id=? ";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,id);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
			DisplayStaffMaster dsm=new DisplayStaffMaster();
			dsm.setStaff_name(rs.getString("staff_name"));
			dsm.setAddress(rs.getString("address"));
			dsm.setEmail_id(rs.getString("email_id"));
			dsm.setBlood_group(rs.getString("blood_group"));
			dsm.setDesignation(rs.getString("designation"));
			dsm.setDob(rs.getString("dob"));
			dsm.setAge(rs.getString("age"));
			dsm.setState(rs.getString("state"));
			dsm.setMobile_no(rs.getString("mobile_no"));
			dsm.setQualification(rs.getString("qualification"));
			dsm.setDepartment(rs.getString("department"));
			dsm.setDate(rs.getString("date"));
			dsm.setStaff_id(rs.getString("staff_id"));
			a.add(dsm);

			}
			rs.close();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
        String jsondata = new Gson().toJson(a);
        response.getWriter().write(jsondata);
	
	}

	
	
	/* @@@@@@@@@@ @@@@@@@@@@@@@@ */
	
	
public void master_staff_history(String id)
{
		/************* history of hospital ****************/
      String lab_name="";
      String lab_price="";
      String date="";
      String lab_id="";



		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Select * from master_opd_lab where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,id);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
				lab_name=rs.getString("lab_name");
				lab_price=rs.getString("lab_price");
				date=rs.getString("date");
				lab_id=rs.getString("lab_id");
				
			}
			rs.close();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_opd_lab_history (lab_name,lab_price,lab_id,ref_id,date) Values(?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(lab_name));
			pst.setString(2,lab_price);
			pst.setString(3,lab_id);
			pst.setString(4,id);
			pst.setString(5,date);
			pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
