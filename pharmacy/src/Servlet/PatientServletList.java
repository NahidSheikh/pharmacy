package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class PatientServletList
 */
@WebServlet("/PatientServletList")
public class PatientServletList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientServletList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String p_id=request.getParameter("p_id");
		String address="";
		String gender="";
		String pat_id="";


			try {
	        	String sql="";
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				sql="select * from patient_registration where patient_id='"+p_id+"' ";
				PreparedStatement pst=con.prepareStatement(sql);
				ResultSet rs=pst.executeQuery();
				if(rs.next())
				{
					address=rs.getString("address");
					gender=rs.getString("gender");
					pat_id=rs.getString("patient_id");
					

				}
				rs.close();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			String data=address+"/"+gender+"/"+pat_id;
	        String jsondata = new Gson().toJson(data);
	        response.getWriter().write(jsondata);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
