package Servlet;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.WordUtils;

import com.google.gson.Gson;

import displaycontroller.DisplayStaffMaster;
import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class PatientRegistrationServlet
 */
@WebServlet("/PatientRegistrationServlet")
public class PatientRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientRegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String staff_name=request.getParameter("staff_name");
		String dob=request.getParameter("dob");
		String age=request.getParameter("age");
		String address=request.getParameter("address");
		String mobile_no=request.getParameter("mobile_no");
		String gender_u=request.getParameter("gender_u");
		String marital_status_u=request.getParameter("marital_status_u");
		String blood_group=request.getParameter("blood_group");
		String id=request.getParameter("id");
		String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

		try {
			master_staff_history(id);
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update patient_registration set patient_name=?,address=?,gender=?,blood_group=?,"
					+ "dob=?,age=?,mobile_no=?,marital_status=?,"
					+ "date=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(staff_name));
			pst.setString(2,address);
			pst.setString(3,gender_u);
			pst.setString(4,blood_group);
			pst.setString(5,dob);
			pst.setString(6,age);
			pst.setString(7,mobile_no);
			pst.setString(8,marital_status_u);
			pst.setString(9,todaystring);
			pst.setString(10,id);
			pst.executeUpdate();
			
			pst.close();
			con.close();
		
		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String department_app=request.getParameter("department_app");
       // System.out.println("department_app======="+department_app);	     
   		ArrayList<Object> a=new ArrayList<Object>();

		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="select staff_name from master_staff where department=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,department_app);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
				DisplayStaffMaster dsm=new DisplayStaffMaster();
				dsm.setStaff_name(rs.getString("staff_name"));
				a.add(dsm);

			}
			rs.close();
			pst.close();
			con.close();
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
        String jsondata = new Gson().toJson(a);
        response.getWriter().write(jsondata);

	}

	
	
	/* @@@@@@@@@@ @@@@@@@@@@@@@@ */
	
	
public void master_staff_history(String id)
{
		/************* history of hospital ****************/
String patient_name="";
String patient_id="";
String address="";
String gender="";
String blood_group="";
String dob="";
String age="";
String mobile_no="";
String marital_status="";
String date="";



		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Select * from patient_registration where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,id);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
				patient_name=rs.getString("patient_name");
				patient_id=rs.getString("patient_id");
				address=rs.getString("address");
				gender=rs.getString("gender");
				blood_group=rs.getString("blood_group");
				dob=rs.getString("dob");
				age=rs.getString("age");
				mobile_no=rs.getString("mobile_no");
				marital_status=rs.getString("marital_status");
				date=rs.getString("date");
				
			}
			rs.close();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into patient_registration_history (patient_name,patient_id,address,gender,blood_group,"
					+ "dob,age,mobile_no,marital_status,"
					+ "date,ref_id) Values(?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(patient_name));
			pst.setString(2,patient_id);
			pst.setString(3,address);
			pst.setString(4,gender);
			pst.setString(5,blood_group);
			pst.setString(6,dob);
			pst.setString(7,age);
			pst.setString(8,mobile_no);
			pst.setString(9,marital_status);
			pst.setString(10,date);
			pst.setString(11,id);
			pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
