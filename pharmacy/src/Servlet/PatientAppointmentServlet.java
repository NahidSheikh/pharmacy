package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.text.WordUtils;

import generatedclasses.ConnectionClass;

/**
 * Servlet implementation class PatientAppointmentServlet
 */
@WebServlet("/PatientAppointmentServlet")
public class PatientAppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientAppointmentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String patient_name_app=request.getParameter("patient_name_app");
		String mobile_no_app=request.getParameter("mobile_no_app");
		String department_app=request.getParameter("department_app");
		String staff_name_app=request.getParameter("staff_name_app");
		String appoint_date_app=request.getParameter("appoint_date_app");
		String time_slot_app=request.getParameter("time_slot_app");
		String sugar_app=request.getParameter("sugar_app");
		String blood_pressure_app=request.getParameter("blood_pressure_app");
		String lmp_app=request.getParameter("lmp_app");
		String diagnosd_app=request.getParameter("diagnosd_app");
		String obstestric_app=request.getParameter("obstestric_app");
		String other_input_app=request.getParameter("other_input_app");
		String s_id_app=request.getParameter("s_id_app");
		String patient_id_app=request.getParameter("patient_id_app");
		String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        long lastid=0;
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into patient_appointment (patient_name,patient_id,mobile_no"
					+ ",department,staff_name,appoint_date,time_slot,sugar,blood_pressure,lmp,"
					+ "diagnosed,obstestric_history,other_input,date,ref_id) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(patient_name_app));
			pst.setString(2,patient_id_app);
			pst.setString(3,mobile_no_app);
			pst.setString(4,department_app);
			pst.setString(5,staff_name_app);
			pst.setString(6,appoint_date_app);
			pst.setString(7,time_slot_app);
			pst.setString(8,sugar_app);
			pst.setString(9,blood_pressure_app);
			pst.setString(10,lmp_app);
			pst.setString(11,diagnosd_app);
			pst.setString(12,obstestric_app);
			pst.setString(13,other_input_app);
			pst.setString(14,todaystring);
			pst.setString(15,s_id_app);
			pst.executeUpdate();
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastid=rs.getLong(1);
			}
			rs.close();
			pst.close();
			con.close();
			generate_id(lastid,todaystring);
		}catch (Exception e) {
			e.printStackTrace();
		}

		
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	
	public void generate_id(long lastid,String todaystring)
	{
		String year[]=todaystring.split("/");

		String app_id="app-"+lastid+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update patient_appointment set appoint_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,app_id);
			pst.setLong(2,lastid);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
