package saleOrderController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_sale_order extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String patient_name;
	private String patient_id;
	private String mobile_number;
	private String gender;
	private String mailing_address;
	private String date;
	private String invoice_no;
	private String bill_no;
	private String total_cgst;
	private String total_tax;
	private String total_sgst;
	private String sub_total;
	private String total_igst;
	private String net_amount;
	private String round_off;
	private String memo;

	
	
	
	
	private String m_id[];
	private String m_name[];
	private String m_batch_id[];
	private String m_company[];
	private String m_pack[];
	private String m_type[];
	private String m_category[];
	private String m_hsn[];
	private String m_batch_lot[];
	private String m_ava_quant[];
	private String m_only_pack[];
	private String m_out_table_id[];


	
	private String m_rate[];
	private String m_quantity[];
	private String m_amount[];
	private String m_tax_name[];
	private String m_tax_amount[];
	private String m_cgst[];
	private String m_sgst[];
	private String m_igst[];
	private String m_total_amount[];
	private String m_exp_date[];

	


	

	
	
	
	

	public String getPatient_name() {
		return patient_name;
	}

	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}

	
	public String getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(String patient_id) {
		this.patient_id = patient_id;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMailing_address() {
		return mailing_address;
	}

	public void setMailing_address(String mailing_address) {
		this.mailing_address = mailing_address;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getBill_no() {
		return bill_no;
	}

	public void setBill_no(String bill_no) {
		this.bill_no = bill_no;
	}

	public String getTotal_cgst() {
		return total_cgst;
	}

	public void setTotal_cgst(String total_cgst) {
		this.total_cgst = total_cgst;
	}

	public String getTotal_tax() {
		return total_tax;
	}

	public void setTotal_tax(String total_tax) {
		this.total_tax = total_tax;
	}

	public String getTotal_sgst() {
		return total_sgst;
	}

	public void setTotal_sgst(String total_sgst) {
		this.total_sgst = total_sgst;
	}

	public String getSub_total() {
		return sub_total;
	}

	public void setSub_total(String sub_total) {
		this.sub_total = sub_total;
	}

	public String getTotal_igst() {
		return total_igst;
	}

	public void setTotal_igst(String total_igst) {
		this.total_igst = total_igst;
	}

	public String getNet_amount() {
		return net_amount;
	}

	public void setNet_amount(String net_amount) {
		this.net_amount = net_amount;
	}

	public String getRound_off() {
		return round_off;
	}

	public void setRound_off(String round_off) {
		this.round_off = round_off;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String[] getM_id() {
		return m_id;
	}

	public void setM_id(String[] m_id) {
		this.m_id = m_id;
	}

	public String[] getM_name() {
		return m_name;
	}

	public void setM_name(String[] m_name) {
		this.m_name = m_name;
	}

	public String[] getM_batch_id() {
		return m_batch_id;
	}

	public void setM_batch_id(String[] m_batch_id) {
		this.m_batch_id = m_batch_id;
	}

	public String[] getM_company() {
		return m_company;
	}

	public void setM_company(String[] m_company) {
		this.m_company = m_company;
	}

	public String[] getM_pack() {
		return m_pack;
	}

	public void setM_pack(String[] m_pack) {
		this.m_pack = m_pack;
	}

	public String[] getM_type() {
		return m_type;
	}

	public void setM_type(String[] m_type) {
		this.m_type = m_type;
	}

	public String[] getM_category() {
		return m_category;
	}

	public void setM_category(String[] m_category) {
		this.m_category = m_category;
	}

	public String[] getM_hsn() {
		return m_hsn;
	}

	public void setM_hsn(String[] m_hsn) {
		this.m_hsn = m_hsn;
	}

	public String[] getM_batch_lot() {
		return m_batch_lot;
	}

	public void setM_batch_lot(String[] m_batch_lot) {
		this.m_batch_lot = m_batch_lot;
	}

	public String[] getM_ava_quant() {
		return m_ava_quant;
	}

	public void setM_ava_quant(String[] m_ava_quant) {
		this.m_ava_quant = m_ava_quant;
	}

	public String[] getM_only_pack() {
		return m_only_pack;
	}

	public void setM_only_pack(String[] m_only_pack) {
		this.m_only_pack = m_only_pack;
	}

	public String[] getM_out_table_id() {
		return m_out_table_id;
	}

	public void setM_out_table_id(String[] m_out_table_id) {
		this.m_out_table_id = m_out_table_id;
	}

	public String[] getM_rate() {
		return m_rate;
	}

	public void setM_rate(String[] m_rate) {
		this.m_rate = m_rate;
	}

	public String[] getM_quantity() {
		return m_quantity;
	}

	public void setM_quantity(String[] m_quantity) {
		this.m_quantity = m_quantity;
	}

	public String[] getM_amount() {
		return m_amount;
	}

	public void setM_amount(String[] m_amount) {
		this.m_amount = m_amount;
	}

	public String[] getM_tax_name() {
		return m_tax_name;
	}

	public void setM_tax_name(String[] m_tax_name) {
		this.m_tax_name = m_tax_name;
	}

	public String[] getM_tax_amount() {
		return m_tax_amount;
	}

	public void setM_tax_amount(String[] m_tax_amount) {
		this.m_tax_amount = m_tax_amount;
	}

	public String[] getM_cgst() {
		return m_cgst;
	}

	public void setM_cgst(String[] m_cgst) {
		this.m_cgst = m_cgst;
	}

	public String[] getM_sgst() {
		return m_sgst;
	}

	public void setM_sgst(String[] m_sgst) {
		this.m_sgst = m_sgst;
	}

	public String[] getM_igst() {
		return m_igst;
	}

	public void setM_igst(String[] m_igst) {
		this.m_igst = m_igst;
	}

	public String[] getM_total_amount() {
		return m_total_amount;
	}

	public void setM_total_amount(String[] m_total_amount) {
		this.m_total_amount = m_total_amount;
	}

	public String[] getM_exp_date() {
		return m_exp_date;
	}

	public void setM_exp_date(String[] m_exp_date) {
		this.m_exp_date = m_exp_date;
	}

	public String getTodaystring() {
		return todaystring;
	}

	public void setTodaystring(String todaystring) {
		this.todaystring = todaystring;
	}

	
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		

		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into pharmacy_outpatient_sale_bill (patient_name,patient_id,mailing_address,bill_date,invoice_no,"
					+ "bill_no,memo,total_cgst,total_sgst,total_igst,round_off,total_tax,sub_total,net_amount,date,gender,mobile_no)"
					+ " Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(patient_name));
			pst.setString(2,patient_id);
			pst.setString(3,mailing_address);
			pst.setString(4,date);
			pst.setString(5,invoice_no);
			pst.setString(6,bill_no);
			pst.setString(7,memo);
			pst.setString(8,total_cgst);
			pst.setString(9,total_sgst);
			pst.setString(10,total_igst);
			pst.setString(11,round_off);
			pst.setString(12,total_tax);
			pst.setString(13,sub_total);
			pst.setString(14,net_amount);
			pst.setString(15,todaystring);
			pst.setString(16,gender);
			pst.setString(17,mobile_number);

			pst.executeUpdate();
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			rs.close();
			pst.close();
			con.close();
			add_medicine();
			hospital_payment();
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			request.getSession().setAttribute("inward_id", lastkey);

			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into pharmacy_outpatient_sale_bill_for_view (patient_name,patient_id,mailing_address,bill_date,invoice_no,"
					+ "bill_no,memo,total_cgst,total_sgst,total_igst,round_off,total_tax,sub_total,net_amount,date,gender,mobile_no,ref_id)"
					+ " Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(patient_name));
			pst.setString(2,patient_id);
			pst.setString(3,mailing_address);
			pst.setString(4,date);
			pst.setString(5,invoice_no);
			pst.setString(6,bill_no);
			pst.setString(7,memo);
			pst.setString(8,total_cgst);
			pst.setString(9,total_sgst);
			pst.setString(10,total_igst);
			pst.setString(11,round_off);
			pst.setString(12,total_tax);
			pst.setString(13,sub_total);
			pst.setString(14,net_amount);
			pst.setString(15,todaystring);
			pst.setString(16,gender);
			pst.setString(17,mobile_number);
			pst.setLong(18,lastkey);


			pst.executeUpdate();
			
			pst.close();
			con.close();
			//add_medicine();
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
				
		return ret;
	}
	
	public void add_medicine()
	{
		//System.out.println("m_name.length====="+m_name.length);
		if(m_name!=null) {
		try {
			int i=0;
			for(i=0;i<m_name.length;i++) {
				
				//System.out.println("ahid");
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into pharmacy_outpatient_sale_bill_list"
					+ " (ref_id,m_id,m_name,m_batch_id,m_rate,m_quantity,m_batch_lot,"
					+ "m_amount,m_tax_name,m_tax_amount,m_cgst,m_sgst,m_igst,m_total_amount,"
					+ "m_exp_date,date,m_company,m_pack,m_type,m_category,m_hsn,m_ava_quant,"
					+ "m_only_pack,m_out_table_id)"
					+ " Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setLong(1,lastkey);
			pst.setString(2,m_id[i]);
			pst.setString(3,m_name[i]);
			pst.setString(4,m_batch_id[i]);

			pst.setString(5,m_rate[i]);
			pst.setString(6,m_quantity[i]);
			pst.setString(7,m_batch_lot[i]);
			pst.setString(8,m_amount[i]);
			pst.setString(9,m_tax_name[i]);
			pst.setString(10,m_tax_amount[i]);
			pst.setString(11,m_cgst[i]);
			pst.setString(12,m_sgst[i]);
			pst.setString(13,m_igst[i]);
			pst.setString(14,m_total_amount[i]);
			pst.setString(15,m_exp_date[i]);
			pst.setString(16,todaystring);
			pst.setString(17,m_company[i]);
			pst.setString(18,m_pack[i]);
			pst.setString(19,m_type[i]);
			pst.setString(20,m_category[i]);
			pst.setString(21,m_hsn[i]);
			pst.setString(22,m_ava_quant[i]);
			pst.setString(23,m_only_pack[i]);
			pst.setString(24,m_out_table_id[i]);


			pst.executeUpdate();
			pst.close();
			con.close();
						
			decrease_stock( m_out_table_id[i],  m_quantity[i]);
			
			new ConnectionClass();
			Connection con1= ConnectionClass.getconnection();
			String sql1="Insert into pharmacy_outpatient_sale_bill_list_for_view"
					+ " (ref_id,m_id,m_name,m_batch_id,m_rate,m_quantity,m_batch_lot,"
					+ "m_amount,m_tax_name,m_tax_amount,m_cgst,m_sgst,m_igst,m_total_amount,"
					+ "m_exp_date,date,m_company,m_pack,m_type,m_category,m_hsn,m_ava_quant,"
					+ "m_only_pack,m_out_table_id)"
					+ " Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst1=con1.prepareStatement(sql1);
			pst1.setLong(1,lastkey);
			pst1.setString(2,m_id[i]);
			pst1.setString(3,m_name[i]);
			pst1.setString(4,m_batch_id[i]);

			pst1.setString(5,m_rate[i]);
			pst1.setString(6,m_quantity[i]);
			pst1.setString(7,m_batch_lot[i]);
			pst1.setString(8,m_amount[i]);
			pst1.setString(9,m_tax_name[i]);
			pst1.setString(10,m_tax_amount[i]);
			pst1.setString(11,m_cgst[i]);
			pst1.setString(12,m_sgst[i]);
			pst1.setString(13,m_igst[i]);
			pst1.setString(14,m_total_amount[i]);
			pst1.setString(15,m_exp_date[i]);
			pst1.setString(16,todaystring);
			pst1.setString(17,m_company[i]);
			pst1.setString(18,m_pack[i]);
			pst1.setString(19,m_type[i]);
			pst1.setString(20,m_category[i]);
			pst1.setString(21,m_hsn[i]);
			pst1.setString(22,m_ava_quant[i]);
			pst1.setString(23,m_only_pack[i]);
			pst1.setString(24,m_out_table_id[i]);
			pst1.executeUpdate();
			pst1.close();
			con1.close();
			}
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		
		
		
		}
	}
	
	
	
	 public void hospital_payment() {
	  
	  
	   try {
	   
	   new ConnectionClass(); Connection con= ConnectionClass.getconnection();
	  String
	  sql="Insert into hospital_payment(name_id,from_name,to_name,debit,payment_type,"
	  + "payment_date,date,ref_id,ref_name) value(?,?,?,?,?,?,?,?,?)";
	  PreparedStatement pst=con.prepareStatement(sql); pst.setString(1,patient_id);
	  pst.setString(2,"Cash Book");
	  pst.setString(3,WordUtils.capitalizeFully(patient_name));
	  pst.setString(4,net_amount);
	  pst.setString(5,"Sale Payment"); 
	  pst.setString(6,date);
	  
	  pst.setString(7,todaystring); 
	  pst.setLong(8,lastkey);
	  pst.setString(9,"Vendor"); 
	  pst.executeUpdate(); 
	  pst.close(); 
	  con.close();
	  
	  }catch (Exception e) 
	   { 
		  e.printStackTrace();
		} 
	  }
	 
	
	public void decrease_stock(String id, String dec_quant) {
		
		int pend_quant=0;
		int stock=0;
		try {
			new ConnectionClass();
			Connection con=ConnectionClass.getconnection();
			String sql="select pending_quantity from pharmacy_medicine_inward_order_list where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1, id);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
				pend_quant=Integer.parseInt(rs.getString("pending_quantity"));
			}
			
			stock=pend_quant-Integer.parseInt(dec_quant);
			
			rs.close();
			pst.close();
			con.close();
		}catch(Exception e)
		{
			e.getMessage();
		}
		
		
	try {
		new ConnectionClass();
		Connection con=ConnectionClass.getconnection();
		String sql="Update pharmacy_medicine_inward_order_list set pending_quantity=? where id=?";
		PreparedStatement pst=con.prepareStatement(sql);
		pst.setInt(1, stock);
		pst.setString(2, id);
		pst.executeUpdate();
		pst.close();
		con.close();
	}catch(Exception e)
	{
		e.getMessage();
	}
	
	
	
	try {
		new ConnectionClass();
		Connection con=ConnectionClass.getconnection();
		String sql="Update pharmacy_medicine_inward_order_list_for_view set pending_quantity=? where id=?";
		PreparedStatement pst=con.prepareStatement(sql);
		pst.setInt(1, stock);
		pst.setString(2, id);
		pst.executeUpdate();
		pst.close();
		con.close();
	}catch(Exception e)
	{
		e.getMessage();
	}
		
	
	}

}

