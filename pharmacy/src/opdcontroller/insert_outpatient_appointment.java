
package opdcontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_outpatient_appointment extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String p_id;
	
	private String patient_name;
	private String mobile_no;
	private String age;
	private String weight;
	private String staff_name;
	
	private String sugar;
	private String blood_pressure;
	private String lmp;
	private String diagnosed;
	private String obstestric_history;
	private String other_input;
	
	private String consultation_fees;
	private String other_charges;
	private String disease;
	private String fever;
	
	private String test;
	private String regular_medicine;
	
	private String []m_id;
	private String []m_name;
	private String []mae;
	private String []dose;
	private String []duration;
	
	private String []i_id;
	private String []i_name;
	private String []imae;
	private String []idose;
	private String []iduration;
	
	


	public String getP_id() {
		return p_id;
	}

	public void setP_id(String p_id) {
		this.p_id = p_id;
	}

	public String getPatient_name() {
		return patient_name;
	}

	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getStaff_name() {
		return staff_name;
	}

	public void setStaff_name(String staff_name) {
		this.staff_name = staff_name;
	}

	public String getSugar() {
		return sugar;
	}

	public void setSugar(String sugar) {
		this.sugar = sugar;
	}

	public String getBlood_pressure() {
		return blood_pressure;
	}

	public void setBlood_pressure(String blood_pressure) {
		this.blood_pressure = blood_pressure;
	}

	public String getLmp() {
		return lmp;
	}

	public void setLmp(String lmp) {
		this.lmp = lmp;
	}

	public String getDiagnosed() {
		return diagnosed;
	}

	public void setDiagnosed(String diagnosed) {
		this.diagnosed = diagnosed;
	}

	public String getObstestric_history() {
		return obstestric_history;
	}

	public void setObstestric_history(String obstestric_history) {
		this.obstestric_history = obstestric_history;
	}

	public String getOther_input() {
		return other_input;
	}

	public void setOther_input(String other_input) {
		this.other_input = other_input;
	}

	public String getConsultation_fees() {
		return consultation_fees;
	}

	public void setConsultation_fees(String consultation_fees) {
		this.consultation_fees = consultation_fees;
	}

	public String getOther_charges() {
		return other_charges;
	}

	public void setOther_charges(String other_charges) {
		this.other_charges = other_charges;
	}

	public String getDisease() {
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}

	public String getFever() {
		return fever;
	}

	public void setFever(String fever) {
		this.fever = fever;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public String getRegular_medicine() {
		return regular_medicine;
	}

	public void setRegular_medicine(String regular_medicine) {
		this.regular_medicine = regular_medicine;
	}

	public String[] getM_id() {
		return m_id;
	}

	public void setM_id(String[] m_id) {
		this.m_id = m_id;
	}

	public String[] getM_name() {
		return m_name;
	}

	public void setM_name(String[] m_name) {
		this.m_name = m_name;
	}

	public String[] getMae() {
		return mae;
	}

	public void setMae(String[] mae) {
		this.mae = mae;
	}

	public String[] getDose() {
		return dose;
	}

	public void setDose(String[] dose) {
		this.dose = dose;
	}

	public String[] getDuration() {
		return duration;
	}

	public void setDuration(String[] duration) {
		this.duration = duration;
	}

	public String[] getI_id() {
		return i_id;
	}

	public void setI_id(String[] i_id) {
		this.i_id = i_id;
	}

	public String[] getI_name() {
		return i_name;
	}

	public void setI_name(String[] i_name) {
		this.i_name = i_name;
	}

	public String[] getImae() {
		return imae;
	}

	public void setImae(String[] imae) {
		this.imae = imae;
	}

	public String[] getIdose() {
		return idose;
	}

	public void setIdose(String[] idose) {
		this.idose = idose;
	}

	public String[] getIduration() {
		return iduration;
	}

	public void setIduration(String[] iduration) {
		this.iduration = iduration;
	}

	String patient_id="";
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		try {
			
			System.out.println("nahid=============");
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update patient_appointment set patient_name=?, mobile_no=?, age=?, weight=?, staff_name=?,"
					+ "sugar=?, blood_pressure=?, lmp=?, diagnosed=?, obstestric_history=?, other_input=?, test=?,"
					+ " regular_medicine=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(patient_name));
			pst.setString(2,mobile_no);
			pst.setString(3,age);
			pst.setString(4,weight);
			pst.setString(5,staff_name);
			pst.setString(6,sugar);
			pst.setString(7,blood_pressure);
			pst.setString(8,lmp);
			pst.setString(9,diagnosed);
			pst.setString(10,obstestric_history);
			pst.setString(11,other_input);
			pst.setString(12,test);
			pst.setString(13,regular_medicine);
			pst.setString(14,p_id);
			pst.executeUpdate();
			pst.close();
			con.close();
			add_med();
			add_inj();
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		return ret;
	}
	
  public void add_med()
  {
		try {

	  int i=0;
	  if(m_name!=null) 
	  {
	  for(i=0;i<m_name.length;i++) {
		  String sql="insert into opd_prescribe_medicine(ref_id,m_name,mae,dose,duration,date)"
		  		+ " values(?,?,?,?,?,?)";
		  new ConnectionClass();
		  Connection con=ConnectionClass.getconnection();
		  PreparedStatement pst=con.prepareStatement(sql);
		  pst.setString(1,p_id);
		  pst.setString(2,m_name[i]);
		  pst.setString(3,mae[i]);
		  pst.setString(4,dose[i]);
		  pst.setString(5,duration[i]);
		  pst.setString(6,todaystring);
		  pst.executeUpdate();
	  }
	  }
	  
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }
  
  
  public void add_inj()
  {
		try {

	  int i=0;
	  if(i_name!=null) 
	  {
	  for(i=0;i<i_name.length;i++) {
		  String sql="insert into opd_prescribe_injection(ref_id,i_name,mae,dose,duration,date)"
		  		+ " values(?,?,?,?,?,?)";
		  new ConnectionClass();
		  Connection con=ConnectionClass.getconnection();
		  PreparedStatement pst=con.prepareStatement(sql);
		  pst.setString(1,p_id);
		  pst.setString(2,i_name[i]);
		  pst.setString(3,imae[i]);
		  pst.setString(4,idose[i]);
		  pst.setString(5,iduration[i]);
		  pst.setString(6,todaystring);
		  pst.executeUpdate();
	  }
	  }
	  
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }

}
