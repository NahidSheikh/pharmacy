package opdcontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_patient_registration_opd extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String patient_name;
	private String address;
	private String gender;
	private String blood_group;
	private String dob;
	private String age;
	private String mobile_no;
	private String marital_status;


	
	

	public String getPatient_name() {
		return patient_name;
	}

	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBlood_group() {
		return blood_group;
	}

	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getMarital_status() {
		return marital_status;
	}

	public void setMarital_status(String marital_status) {
		this.marital_status = marital_status;
	}

	String patient_id="";
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into patient_registration (patient_name,address,gender,blood_group,dob,age,mobile_no,marital_status,date)"
					+ " Values(?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(patient_name));
			pst.setString(2,address);
			pst.setString(3,gender);
			pst.setString(4,blood_group);
			pst.setString(5,dob);
			pst.setString(6,age);
			pst.setString(7,mobile_no);
			pst.setString(8,marital_status);
			pst.setString(9,todaystring);
			

			pst.executeUpdate();
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			rs.close();
			pst.close();
			con.close();
			generate_id();
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
				
		return ret;
	}
	
	public void generate_id()
	{
		String year[]=todaystring.split("/");
		patient_id="OPD-"+lastkey+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update patient_registration set patient_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,patient_id);
			pst.setLong(2,lastkey);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
