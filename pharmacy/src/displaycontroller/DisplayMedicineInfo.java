package displaycontroller;

public class DisplayMedicineInfo {
	
	private String id;
	private String medicine_name;
	private String pack;
	private String medicine_type;
	private String company_name;
	private String medicine_category;
	private String unit;
	private String hsn;
	private String mrp;
	private String e_date;
	private String b_id;
	private String tax;
	public String getMedicine_name() {
		return medicine_name;
	}
	public void setMedicine_name(String medicine_name) {
		this.medicine_name = medicine_name;
	}
	public String getPack() {
		return pack;
	}
	public void setPack(String pack) {
		this.pack = pack;
	}
	public String getMedicine_type() {
		return medicine_type;
	}
	public void setMedicine_type(String medicine_type) {
		this.medicine_type = medicine_type;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public String getMedicine_category() {
		return medicine_category;
	}
	public void setMedicine_category(String medicine_category) {
		this.medicine_category = medicine_category;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getHsn() {
		return hsn;
	}
	public void setHsn(String hsn) {
		this.hsn = hsn;
	}
	public String getMrp() {
		return mrp;
	}
	public void setMrp(String mrp) {
		this.mrp = mrp;
	}
	public String getE_date() {
		return e_date;
	}
	public void setE_date(String e_date) {
		this.e_date = e_date;
	}
	public String getB_id() {
		return b_id;
	}
	public void setB_id(String b_id) {
		this.b_id = b_id;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public DisplayMedicineInfo()
	{
		this.medicine_name=medicine_name;
		this.pack=pack;
		this.medicine_type=medicine_type;
		this.company_name=company_name;
		this.medicine_category=medicine_category;
		this.unit=unit;
		this.hsn=hsn;
		this.mrp=mrp;
		this.e_date=e_date;
		this.b_id=b_id;
		this.tax=tax;
		this.id=id;

	}
	

}
