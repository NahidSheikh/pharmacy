package displaycontroller;

public class DisplayStaffMaster {

	private String staff_name;
	private String address;
	private String email_id;
	private String blood_group;
	private String designation;
	private String dob;
	private String age;
	private String state;
	private String mobile_no;
	private String qualification;
	private String department;
	private String date;
	private String staff_id;
	public String getStaff_name() {
		return staff_name;
	}
	public void setStaff_name(String staff_name) {
		this.staff_name = staff_name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getBlood_group() {
		return blood_group;
	}
	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getStaff_id() {
		return staff_id;
	}
	public void setStaff_id(String staff_id) {
		this.staff_id = staff_id;
	}
	
	
 public DisplayStaffMaster() {
	 this.staff_name=staff_name;
	 this.address=address;
	 this.email_id=email_id;
	 this.blood_group=blood_group;
	 this.designation=designation;
	 this.dob=dob;
	 this.age=age;
	 this.state=state;
	 this.mobile_no=mobile_no;
	 this.qualification=qualification;
	 this.department=department;
	 this.date=date;
	 this.staff_id=staff_id;


	 
 }


	
	
}
