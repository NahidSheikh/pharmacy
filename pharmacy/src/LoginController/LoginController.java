package LoginController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;

public class LoginController extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String uname;
	private String pass;
	private String msg;
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getUname() { 
		return uname;  
	}

	public void setUname(String uname) {
		this.uname = uname;
	}
	  
	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	String access="";
	String staff_name="";
	String staff_id="";
	String id="";


	String ret="error";
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		try {
			
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Select * from master_staff where username=? and password=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,uname);
			pst.setString(2,pass);

			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
				staff_name=rs.getString("staff_name");
				staff_id=rs.getString("staff_id");
				access=rs.getString("access");
				id=rs.getString("id");
			}
			rs.close();
			pst.close();
			con.close();
			
			if(access.trim().equals("1"))
			{
				request.getSession().setAttribute("lg_name",staff_name);
				request.getSession().setAttribute("lg_id",staff_id);
				request.getSession().setAttribute("lg_access",access);
				request.getSession().setAttribute("lg_id",id);
				ret="success";
			}else {
				ret="error";
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		return ret;
	}
	
	
}
