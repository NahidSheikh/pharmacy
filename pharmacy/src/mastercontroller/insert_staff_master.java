package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_staff_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String staff_name;
	private String address;
	private String email_id;
	private String blood_group;
	private String designation;
	private String dob;
	private String age;
	private String state;
	private String mobile_no;
	private String qualification;
	private String department;
	

	


	public String getStaff_name() {
		return staff_name;
	}
	public void setStaff_name(String staff_name) {
		this.staff_name = staff_name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getBlood_group() {
		return blood_group;
	}
	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
	String staff_id="";
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_staff (staff_name,address,email_id,blood_group,designation,"
					+ "dob,age,state,mobile_no,qualification,department,"
					+ "date) Values(?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(staff_name));
			pst.setString(2,address);
			pst.setString(3,email_id);
			pst.setString(4,blood_group);
			pst.setString(5,designation);
			pst.setString(6,dob);
			pst.setString(7,age);
			pst.setString(8,state);
			pst.setString(9,mobile_no);
			pst.setString(10,qualification);
			pst.setString(11,department);
			pst.setString(12,todaystring);
			pst.executeUpdate();
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			
			rs.close();
			pst.close();
			con.close();
			
			generate_id();
			
			
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		
		
		return ret;
	}
	
	public void generate_id()
	{
		String year[]=todaystring.split("/");

		staff_id="staff-"+lastkey+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_staff set staff_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,staff_id);
			pst.setLong(2,lastkey);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
