package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class delete_staff_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String s_id_del;
	
    
	public String getS_id_del() {
		return s_id_del;
	}
	public void setS_id_del(String s_id_del) {
		this.s_id_del = s_id_del;
	}
	
	String ret="error";
	public String execute()
	{	
		HttpServletRequest request = ServletActionContext.getRequest();
		master_staff_history(s_id_del);
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Delete from master_staff where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,s_id_del);
			
			pst.executeUpdate();
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Data Deleted Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";

			
		}
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Delete from master_staff_history where ref_id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,s_id_del);
			
			pst.executeUpdate();
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Data Deleted Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";

			
		}
		
		
		
		return ret;
	}
	
	public void master_staff_history(String id)
	{
			/************* history of hospital ****************/
	String staff_name="";
	String address="";
	String email_id="";
	String blood_group="";
	String designation="";
	String dob="";
	String age="";
	String state="";
	String mobile_no="";
	String qualification="";
	String department="";
	String date="";
	String staff_id="";



			try {
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				String sql="Select * from master_staff where id=?";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.setString(1,id);
				ResultSet rs=pst.executeQuery();
				while(rs.next())
				{
					staff_name=rs.getString("staff_name");
					address=rs.getString("address");
					email_id=rs.getString("email_id");
					blood_group=rs.getString("blood_group");
					designation=rs.getString("designation");
					dob=rs.getString("dob");
					age=rs.getString("age");
					state=rs.getString("state");
					mobile_no=rs.getString("mobile_no");
					qualification=rs.getString("qualification");
					department=rs.getString("department");
					date=rs.getString("date");
					staff_id=rs.getString("staff_id");
					
				}
				rs.close();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				String sql="Insert into master_staff_delete_entries (staff_name,address,email_id,blood_group,designation,"
						+ "dob,age,state,mobile_no,qualification,department,"
						+ "date,ref_id,staff_id) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.setString(1,WordUtils.capitalizeFully(staff_name));
				pst.setString(2,address);
				pst.setString(3,email_id);
				pst.setString(4,blood_group);
				pst.setString(5,designation);
				pst.setString(6,dob);
				pst.setString(7,age);
				pst.setString(8,state);
				pst.setString(9,mobile_no);
				pst.setString(10,qualification);
				pst.setString(11,department);
				pst.setString(12,date);
				pst.setString(13,id);
				pst.setString(14,staff_id);
				pst.executeUpdate();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	

}
