package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class delete_test_lab_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String s_id_del;
	
    
	public String getS_id_del() {
		return s_id_del;
	}
	public void setS_id_del(String s_id_del) {
		this.s_id_del = s_id_del;
	}
	
	String ret="error";
	public String execute()
	{	
		HttpServletRequest request = ServletActionContext.getRequest();
		master_staff_history(s_id_del);
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Delete from master_test_lab where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,s_id_del);
			
			pst.executeUpdate();
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Data Deleted Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";

			
		}
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Delete from master_test_lab_history where ref_id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,s_id_del);
			
			pst.executeUpdate();
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Data Deleted Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";

			
		}
		
		
		
		return ret;
	}
	
	public void master_staff_history(String id)
	{
			/************* history of hospital ****************/
	      String lab_name="";
	      String lab_price="";
	      String date="";
	      String lab_id="";

			try {
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				String sql="Select * from master_test_lab where id=?";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.setString(1,id);
				ResultSet rs=pst.executeQuery();
				while(rs.next())
				{
					lab_name=rs.getString("test_name");
					lab_price=rs.getString("test_price");
					date=rs.getString("date");
					lab_id=rs.getString("test_id");
					
				}
				rs.close();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			
			try {
				new ConnectionClass();
				Connection con= ConnectionClass.getconnection();
				String sql="Insert into master_test_lab_delete_entries (test_name,test_price,test_id,ref_id,date) Values(?,?,?,?,?)";
				PreparedStatement pst=con.prepareStatement(sql);
				pst.setString(1,WordUtils.capitalizeFully(lab_name));
				pst.setString(2,lab_price);
				pst.setString(3,lab_id);
				pst.setString(4,id);
				pst.setString(5,date);
				pst.executeUpdate();
				pst.close();
				con.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	

}
