package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_opd_lab_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String lab_name;
	private String lab_price;
	
	public String getLab_name() {
		return lab_name;
	}

	public void setLab_name(String lab_name) {
		this.lab_name = lab_name;
	}

	public String getLab_price() {
		return lab_price;
	}

	public void setLab_price(String lab_price) {
		this.lab_price = lab_price;
	}

	String lab_id="";
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_opd_lab (lab_name,lab_price,date) Values(?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(lab_name));
			pst.setString(2,lab_price);
			pst.setString(3,todaystring);
			pst.executeUpdate();
			System.out.println(pst);
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			
			rs.close();
			pst.close();
			con.close();
			
			generate_id();
			
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		
		
		return ret;
	}
	
	public void generate_id()
	{
		String year[]=todaystring.split("/");

		lab_id="opdlab-"+lastkey+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_opd_lab set lab_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,lab_id);
			pst.setLong(2,lastkey);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
