package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_medicine_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String medicine_name;
	private String pack;
	private String medicine_type;
	private String company_name;
	private String medicine_category;
	private String unit;
	private String hsn;
	private String low_stock;




	
	

	public String getPack() {
		return pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getMedicine_category() {
		return medicine_category;
	}

	public void setMedicine_category(String medicine_category) {
		this.medicine_category = medicine_category;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getHsn() {
		return hsn;
	}

	public void setHsn(String hsn) {
		this.hsn = hsn;
	}

	public String getLow_stock() {
		return low_stock;
	}

	public void setLow_stock(String low_stock) {
		this.low_stock = low_stock;
	}

	public String getMedicine_name() {
		return medicine_name;
	}

	public void setMedicine_name(String medicine_name) {
		this.medicine_name = medicine_name;
	}

	public String getMedicine_type() {
		return medicine_type;
	}

	public void setMedicine_type(String medicine_type) {
		this.medicine_type = medicine_type;
	}

	String test_id="";
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_medicine (medicine_name,pack,medicine_type,company_name,"
					+ "medicine_category,unit,hsn,low_stock,date) Values(?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(medicine_name));
			pst.setString(2,pack);
			pst.setString(3,medicine_type);
			pst.setString(4,WordUtils.capitalizeFully(company_name));
			pst.setString(5,medicine_category);
			pst.setString(6,unit);
			pst.setString(7,hsn);
			pst.setString(8,low_stock);
			pst.setString(9,todaystring);
			pst.executeUpdate();
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			
			rs.close();
			pst.close();
			con.close();
			
			generate_id();
			
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		
		
		return ret;
	}
	
	public void generate_id()
	{
		String year[]=todaystring.split("/");

		test_id="Med-"+lastkey+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_medicine set medicine_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,test_id);
			pst.setLong(2,lastkey);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
