package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_vendor_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String vendor_name;
	private String state;
	private String mobile_no_second;
	private String email_second;
	private String fax_no;
	private String registration_no;
	private String bank_name;
	private String account_no;
	private String opening_balance;
	private String opening_date;
	
	private String vendor_address;
	private String mobile_no_first;
	private String email_first;
	private String website;
	private String pan_no;
	private String gst_no;
	private String ifsc_no;
	private String branch_no;
	private String balance_type;
	
	


	
	
	public String getVendor_name() {
		return vendor_name;
	}

	public void setVendor_name(String vendor_name) {
		this.vendor_name = vendor_name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMobile_no_second() {
		return mobile_no_second;
	}

	public void setMobile_no_second(String mobile_no_second) {
		this.mobile_no_second = mobile_no_second;
	}

	public String getEmail_second() {
		return email_second;
	}

	public void setEmail_second(String email_second) {
		this.email_second = email_second;
	}

	public String getFax_no() {
		return fax_no;
	}

	public void setFax_no(String fax_no) {
		this.fax_no = fax_no;
	}

	public String getRegistration_no() {
		return registration_no;
	}

	public void setRegistration_no(String registration_no) {
		this.registration_no = registration_no;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getAccount_no() {
		return account_no;
	}

	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}

	public String getOpening_balance() {
		return opening_balance;
	}

	public void setOpening_balance(String opening_balance) {
		this.opening_balance = opening_balance;
	}

	public String getOpening_date() {
		return opening_date;
	}

	public void setOpening_date(String opening_date) {
		this.opening_date = opening_date;
	}

	public String getVendor_address() {
		return vendor_address;
	}

	public void setVendor_address(String vendor_address) {
		this.vendor_address = vendor_address;
	}

	public String getMobile_no_first() {
		return mobile_no_first;
	}

	public void setMobile_no_first(String mobile_no_first) {
		this.mobile_no_first = mobile_no_first;
	}

	public String getEmail_first() {
		return email_first;
	}

	public void setEmail_first(String email_first) {
		this.email_first = email_first;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getPan_no() {
		return pan_no;
	}

	public void setPan_no(String pan_no) {
		this.pan_no = pan_no;
	}

	public String getGst_no() {
		return gst_no;
	}

	public void setGst_no(String gst_no) {
		this.gst_no = gst_no;
	}

	public String getIfsc_no() {
		return ifsc_no;
	}

	public void setIfsc_no(String ifsc_no) {
		this.ifsc_no = ifsc_no;
	}

	public String getBranch_no() {
		return branch_no;
	}

	public void setBranch_no(String branch_no) {
		this.branch_no = branch_no;
	}

	public String getBalance_type() {
		return balance_type;
	}

	public void setBalance_type(String balance_type) {
		this.balance_type = balance_type;
	}

	String vendor_id="";
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_vendor (vendor_name,state,mobile_no_second,email_second,fax_no,"
					+ "registration_no,bank_name,account_no,opening_balance,opening_date,vendor_address,"
					+ "mobile_no_first,email_first,website,pan_no,gst_no,ifsc_no,branch_no,balance_type,"
					+ "date) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(vendor_name));
			pst.setString(2,state);
			pst.setString(3,mobile_no_second);
			pst.setString(4,email_second);
			pst.setString(5,fax_no);
			pst.setString(6,registration_no);
			pst.setString(7,bank_name);
			pst.setString(8,account_no);
			pst.setString(9,opening_balance);
			pst.setString(10,opening_date);
			pst.setString(11,vendor_address);
			pst.setString(12,mobile_no_first);
			
			pst.setString(13,email_first);
			pst.setString(14,website);
			pst.setString(15,pan_no);
			pst.setString(16,gst_no);
			pst.setString(17,ifsc_no);
			pst.setString(18,branch_no);
			pst.setString(19,balance_type);
			pst.setString(20,todaystring);
			pst.executeUpdate();
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			
			rs.close();
			pst.close();
			con.close();
			
			generate_id();
			hospital_payment();
			
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		
		
		return ret;
	}
	
	public void generate_id()
	{
		String year[]=todaystring.split("/");

		vendor_id="Vendor-"+lastkey+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_vendor set vendor_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,vendor_id);
			pst.setLong(2,lastkey);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void hospital_payment() {
		String type="";
		if(balance_type.trim().equals("Debit"))
		{
			type="debit";
		}else {
			type="credit";
		}
		
		try {
			
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into hospital_payment(name_id,from_name,"+type+",payment_type,"
					+ "payment_date,date,ref_id,ref_name) value(?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,vendor_id);
			pst.setString(2,WordUtils.capitalizeFully(vendor_name));
			pst.setString(3,opening_balance);
			pst.setString(4,"Opening Balance");
			pst.setString(5,opening_date);
			pst.setString(6,todaystring);
			pst.setLong(7,lastkey);
			pst.setString(8,"Vendor");
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


}
