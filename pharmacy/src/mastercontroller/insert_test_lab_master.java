package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_test_lab_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String test_name;
	private String test_price;
	
	

	public String getTest_name() {
		return test_name;
	}

	public void setTest_name(String test_name) {
		this.test_name = test_name;
	}

	public String getTest_price() {
		return test_price;
	}

	public void setTest_price(String test_price) {
		this.test_price = test_price;
	}

	String test_id="";
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_test_lab (test_name,test_price,date) Values(?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(test_name));
			pst.setString(2,test_price);
			pst.setString(3,todaystring);
			pst.executeUpdate();
			System.out.println(pst);
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			
			rs.close();
			pst.close();
			con.close();
			
			generate_id();
			
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		
		
		return ret;
	}
	
	public void generate_id()
	{
		String year[]=todaystring.split("/");

		test_id="test-"+lastkey+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_test_lab set test_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,test_id);
			pst.setLong(2,lastkey);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
