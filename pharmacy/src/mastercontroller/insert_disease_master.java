package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_disease_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String disease_name;
	
	public String getDisease_name() {
		return disease_name;
	}

	public void setDisease_name(String disease_name) {
		this.disease_name = disease_name;
	}

	String dis_id="";
	String ret="error";
	long lastkey=0;
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_disease (disease_name,date) Values(?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,WordUtils.capitalizeFully(disease_name));
			pst.setString(2,todaystring);
			pst.executeUpdate();
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			rs.close();
			pst.close();
			con.close();
			generate_id();
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
				
		return ret;
	}
	
	public void generate_id()
	{
		String year[]=todaystring.split("/");
		dis_id="Dis-"+lastkey+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_disease set disease_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,dis_id);
			pst.setLong(2,lastkey);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
