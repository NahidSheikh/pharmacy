package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_role_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String s_id;
	private String [] Authority;
	private String [] SubAuthority;
	private String access;
	
	
	public String getS_id() {
		return s_id;
	}

	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	public String[] getAuthority() {
		return Authority;
	}

	public void setAuthority(String[] authority) {
		Authority = authority;
	}

	public String[] getSubAuthority() {
		return SubAuthority;
	}

	public void setSubAuthority(String[] subAuthority) {
		SubAuthority = subAuthority;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}
		 
    String ret="error";
    HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		String auth="";
		String sub_auth="";
		int i=0;
		if(Authority!=null) {
		for(i=0;i<Authority.length;i++)
		{
			if(i==0)
			{
				auth=Authority[i];

			}else {
				auth=auth+","+Authority[i];
			}
		}
		}
		
		i=0;
		if(SubAuthority!=null) {
			for(i=0;i<SubAuthority.length;i++)
			{
				if(i==0)
				{
					sub_auth=SubAuthority[i];

				}else {
					sub_auth=sub_auth+","+SubAuthority[i];
				}
			}
			}
		
		try {
			
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_staff set authority=?,sub_authority=?,access=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,auth);
			pst.setString(2,sub_auth);
			pst.setString(3,access);
			pst.setString(4,s_id);
			pst.executeUpdate();
			System.out.println("pst==========="+pst);
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Role Updated Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";

			
		}

		return ret;
	}

	
	
}
