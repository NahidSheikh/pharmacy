package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class update_hospital_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String h_id;
	private String hospital_name;
	private String state;
	private String person_name_second;
	private String mobile_no_second;
	private String email_second;
	private String fax_no;
	private String registration_no;
	private String bank_name;
	private String account_no;
	private String hospital_address;
	private String person_name_first;
	private String mobile_no_first;
	private String email_first;
	private String website;
	private String pan_no;
	private String gst_no;
	private String ifsc_no;
	private String branch_no;
	private String fin_year_from;
	private String fin_year_to;
	
	
	
	public String getH_id() {
		return h_id;
	}

	public void setH_id(String h_id) {
		this.h_id = h_id;
	}

	public String getHospital_name() {
		return hospital_name;
	}

	public void setHospital_name(String hospital_name) {
		this.hospital_name = hospital_name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPerson_name_second() {
		return person_name_second;
	}

	public void setPerson_name_second(String person_name_second) {
		this.person_name_second = person_name_second;
	}

	public String getMobile_no_second() {
		return mobile_no_second;
	}

	public void setMobile_no_second(String mobile_no_second) {
		this.mobile_no_second = mobile_no_second;
	}

	public String getEmail_second() {
		return email_second;
	}

	public void setEmail_second(String email_second) {
		this.email_second = email_second;
	}

	public String getFax_no() {
		return fax_no;
	}

	public void setFax_no(String fax_no) {
		this.fax_no = fax_no;
	}

	public String getRegistration_no() {
		return registration_no;
	}

	public void setRegistration_no(String registration_no) {
		this.registration_no = registration_no;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}


	public String getAccount_no() {
		return account_no;
	}

	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}

	public String getHospital_address() {
		return hospital_address;
	}

	public void setHospital_address(String hospital_address) {
		this.hospital_address = hospital_address;
	}

	public String getPerson_name_first() {
		return person_name_first;
	}

	public void setPerson_name_first(String person_name_first) {
		this.person_name_first = person_name_first;
	}

	public String getMobile_no_first() {
		return mobile_no_first;
	}

	public void setMobile_no_first(String mobile_no_first) {
		this.mobile_no_first = mobile_no_first;
	}

	public String getEmail_first() {
		return email_first;
	}

	public void setEmail_first(String email_first) {
		this.email_first = email_first;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getPan_no() {
		return pan_no;
	}

	public void setPan_no(String pan_no) {
		this.pan_no = pan_no;
	}

	public String getGst_no() {
		return gst_no;
	}

	public void setGst_no(String gst_no) {
		this.gst_no = gst_no;
	}

	public String getIfsc_no() {
		return ifsc_no;
	}

	public void setIfsc_no(String ifsc_no) {
		this.ifsc_no = ifsc_no;
	}

	public String getBranch_no() {
		return branch_no;
	}

	public void setBranch_no(String branch_no) {
		this.branch_no = branch_no;
	}
	

	 public String getFin_year_from() {
		return fin_year_from;
	}

	public void setFin_year_from(String fin_year_from) {
		this.fin_year_from = fin_year_from;
	}

	public String getFin_year_to() {
		return fin_year_to;
	}

	public void setFin_year_to(String fin_year_to) {
		this.fin_year_to = fin_year_to;
	}


	String h_id1;
	 String hospital_name1;
	 String state1;
	 String person_name_second1;
	 String mobile_no_second1;
	 String email_second1;
	 String fax_no1;
	 String registration_no1;
	 String bank_name1;
	 String account_no1;
	 String hospital_address1;
	 String person_name_first1;
	 String mobile_no_first1;
	 String email_first1;
	 String website1;
	 String pan_no1;
	 String gst_no1;
	 String ifsc_no1;
	 String branch_no1;
	 String date="";
	 String fin_year_from1;
	 String fin_year_to1;
	 
	 
    String ret="error";
    HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		//System.out.println("h_id================="+h_id);
		master_hospital_history();
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_hospital set hospital_name=?,state=?,person_name_second=?,mobile_no_second=?,email_second=?,"
					+ "fax_no=?,registration_no=?,bank_name=?,account_no=?,hospital_address=?,person_name_first=?,"
					+ "mobile_no_first=?,email_first=?,website=?,pan_no=?,gst_no=?,ifsc_no=?,branch_no=?,date=?,fin_year_from=?,fin_year_to=? where id=?";
			
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(hospital_name));
			pst.setString(2,state );
			pst.setString(3,person_name_second);
			pst.setString(4,mobile_no_second);
			pst.setString(5,email_second);
			pst.setString(6,fax_no);
			pst.setString(7,registration_no);
			pst.setString(8,bank_name);
			pst.setString(9,account_no);
			pst.setString(10,hospital_address);
			pst.setString(11,person_name_first);
			pst.setString(12,mobile_no_first);
			pst.setString(13,email_first);
			pst.setString(14,website);
			pst.setString(15,pan_no);
			pst.setString(16,gst_no);
			pst.setString(17,ifsc_no);
			pst.setString(18,branch_no);
			pst.setString(19,todaystring);
			pst.setString(20,fin_year_from);
			pst.setString(21,fin_year_to);
			pst.setString(22,h_id);


			pst.executeUpdate();
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Data Updated Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";

			
		}
		
		
		return ret;
	}
	
	public void master_hospital_history()
	{
		/************* history of hospital ****************/

		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Select * from master_hospital where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,h_id);
			ResultSet rs=pst.executeQuery();
			while(rs.next())
			{
				hospital_name1=rs.getString("hospital_name");
				state1=rs.getString("state");
				person_name_second1=rs.getString("person_name_second");
				mobile_no_second1=rs.getString("mobile_no_second");
				email_second1=rs.getString("email_second");
				fax_no1=rs.getString("fax_no");
				registration_no1=rs.getString("registration_no");
				bank_name1=rs.getString("bank_name");
				account_no1=rs.getString("account_no");
				hospital_address1=rs.getString("hospital_address");
				person_name_first1=rs.getString("person_name_first");
				mobile_no_first1=rs.getString("mobile_no_first");
				email_first1=rs.getString("email_first");
				website1=rs.getString("website");
				pan_no1=rs.getString("pan_no");
				gst_no1=rs.getString("gst_no");
				ifsc_no1=rs.getString("ifsc_no");
				branch_no1=rs.getString("branch_no");
				date=rs.getString("date");
				fin_year_from1=rs.getString("fin_year_from");
				fin_year_to1=rs.getString("fin_year_to");

	
			}
			rs.close();
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		
		
		
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_hospital_history (hospital_name,state,person_name_second,mobile_no_second,email_second,"
					+ "fax_no,registration_no,bank_name,account_no,hospital_address,person_name_first,"
					+ "mobile_no_first,email_first,website,pan_no,gst_no,ifsc_no,branch_no,date,fin_year_from,fin_year_to) Values(?,?,?,?,?,?,?,?,?,?,"
					+ "?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,WordUtils.capitalizeFully(hospital_name1));
			pst.setString(2,state1);
			pst.setString(3,person_name_second1);
			pst.setString(4,mobile_no_second1);
			pst.setString(5,email_second1);
			pst.setString(6,fax_no1);
			pst.setString(7,registration_no1);
			pst.setString(8,bank_name1);
			pst.setString(9,account_no1);
			pst.setString(10,hospital_address1);
			pst.setString(11,person_name_first1);
			pst.setString(12,mobile_no_first1);
			pst.setString(13,email_first1);
			pst.setString(14,website1);
			pst.setString(15,pan_no1);
			pst.setString(16,gst_no1);
			pst.setString(17,ifsc_no1);
			pst.setString(18,branch_no1);
			pst.setString(19,date);
			pst.setString(20,fin_year_from1);
			pst.setString(21,fin_year_to1);
			pst.executeUpdate();
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";

			
		}
	}

}
