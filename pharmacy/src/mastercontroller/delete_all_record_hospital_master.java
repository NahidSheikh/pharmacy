package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class delete_all_record_hospital_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	
	String ret="error";
	public String execute()
	{	
		HttpServletRequest request = ServletActionContext.getRequest();
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Delete from master_hospital_history";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.executeUpdate();
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Data Clear Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";

			
		}
		
		
		
		return ret;
	}
	
	

}
