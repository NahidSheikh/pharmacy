package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class insert_tax_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String tax_percentage;
	private String tax_name;
	

	public String getTax_percentage() {
		return tax_percentage;
	}

	public void setTax_percentage(String tax_percentage) {
		this.tax_percentage = tax_percentage;
	}

	public String getTax_name() {
		return tax_name;
	}

	public void setTax_name(String tax_name) {
		this.tax_name = tax_name;
	}

	String tax_id="";
	String ret="error";
	long lastkey=0;
	String tax="";
	
	HttpServletRequest request = ServletActionContext.getRequest();
	String todaystring=new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	
	public String execute()
	{	
		tax=tax_name+"@"+tax_percentage+"%";
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Insert into master_tax (tax_percentage,tax_name,tax,date) Values(?,?,?,?)";
			PreparedStatement pst=con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1,tax_percentage);
			pst.setString(2,tax_name);
			pst.setString(3,tax);
			pst.setString(4,todaystring);
			pst.executeUpdate();
			//System.out.println(pst);
			ResultSet rs=pst.getGeneratedKeys();
			while(rs.next())
			{
				lastkey=rs.getLong(1);
			}
			
			rs.close();
			pst.close();
			con.close();
			
			generate_id();
			
			request.getSession().setAttribute("successmsg", "Data Saved Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";
		}
		
		
		
		return ret;
	}
	
	public void generate_id()
	{
		String year[]=todaystring.split("/");

		tax_id="Tax-"+lastkey+"-"+year[2];
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Update master_tax set tax_id=? where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,tax_id);
			pst.setLong(2,lastkey);
			pst.executeUpdate();
			pst.close();
			con.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
