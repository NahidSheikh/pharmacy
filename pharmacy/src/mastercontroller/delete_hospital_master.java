package mastercontroller;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
import generatedclasses.ConnectionClass;
public class delete_hospital_master extends ActionSupport{	
	
	private static final long serialVersionUID = 1L;
	
	private String del_id;
	
    public String getDel_id() {
		return del_id;
	}
	public void setDel_id(String del_id) {
		this.del_id = del_id;
	}
	String ret="error";
	public String execute()
	{	
		HttpServletRequest request = ServletActionContext.getRequest();
		
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="Delete from master_hospital_history where id=?";
			PreparedStatement pst=con.prepareStatement(sql);
			pst.setString(1,del_id);
			
			pst.executeUpdate();
			pst.close();
			con.close();
			request.getSession().setAttribute("successmsg", "Data Deleted Successfully.");
			ret="success";
		}catch (Exception e) {
			e.printStackTrace();
			ret="error";

			
		}
		
		
		
		return ret;
	}
	
	

}
