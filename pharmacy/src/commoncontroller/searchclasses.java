package commoncontroller;
public class searchclasses {
	
	public 	static String FourFieldSearch(String first,String second, String third, String four, 
			String table_name, String first_column, String second_column, String third_column, String fourth_column)
	{
		String sql="";
		if(first.trim().equals("") && second.trim().equals("") && third.trim().equals("") && four.trim().equals(""))
		{
			sql="select * from "+table_name+"";
		}
		else if(!first.trim().equals("") && second.trim().equals("") && third.trim().equals("") && four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+first_column+"='"+first+"'";

		}
		else if(first.trim().equals("") && !second.trim().equals("") && third.trim().equals("") && four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+second_column+"='"+second+"'";

		}
		else if(!first.trim().equals("") && !second.trim().equals("") && third.trim().equals("") && four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+first_column+"='"+first+"' and "+second_column+"='"+second+"' ";

		}
		else if(first.trim().equals("") && second.trim().equals("") && !third.trim().equals("") && four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+third_column+"='"+third+"'";

		}
		else if(!first.trim().equals("") && second.trim().equals("") && !third.trim().equals("") && four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+first_column+"='"+first+"' and "+third_column+"='"+third+"'";

		}
		else if(first.trim().equals("") && !second.trim().equals("") && !third.trim().equals("") && four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+second_column+"='"+second+"' and "+third_column+"='"+third+"'";

		}
		else if(!first.trim().equals("") && !second.trim().equals("") && !third.trim().equals("") && four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+first_column+"='"+first+"' and "+second_column+"='"+second+"' and "+third_column+"='"+third+"'";

		}
		else if(first.trim().equals("") && second.trim().equals("") && third.trim().equals("") && !four.trim().equals(""))
		{
			sql="select * from "+table_name+" where  "+fourth_column+"='"+four+"'";

		}
		else if(!first.trim().equals("") && second.trim().equals("") && third.trim().equals("") && !four.trim().equals(""))
		{
			sql="select * from "+table_name+" where  "+first_column+"='"+first+"' and "+fourth_column+"='"+four+"'";

		}
		else if(first.trim().equals("") && !second.trim().equals("") && third.trim().equals("") && !four.trim().equals(""))
		{
			sql="select * from "+table_name+" where  "+second_column+"='"+second+"' and "+fourth_column+"='"+four+"'";

		}
		else if(!first.trim().equals("") && !second.trim().equals("") && third.trim().equals("") && !four.trim().equals(""))
		{
			sql="select * from "+table_name+" where  "+first_column+"='"+first+"' and "+second_column+"='"+second+"' and "+fourth_column+"='"+four+"'";

		}
		else if(first.trim().equals("") && second.trim().equals("") && !third.trim().equals("") && !four.trim().equals(""))
		{
			sql="select * from "+table_name+" where STR_TO_DATE("+third_column+", '%d/%m/%Y') Between STR_TO_DATE('"+third+"', '%d/%m/%Y') and  STR_TO_DATE('"+four+"', '%d/%m/%Y')";
		}
		else if(!first.trim().equals("") && second.trim().equals("") && !third.trim().equals("") && !four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+first_column+"='"+first+"' and STR_TO_DATE("+third_column+", '%d/%m/%Y') Between STR_TO_DATE('"+third+"', '%d/%m/%Y') and  STR_TO_DATE('"+four+"', '%d/%m/%Y')";

		}
		else if(first.trim().equals("") && !second.trim().equals("") && !third.trim().equals("") && !four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+second_column+"='"+second+"' and STR_TO_DATE("+third_column+", '%d/%m/%Y') Between STR_TO_DATE('"+third+"', '%d/%m/%Y') and  STR_TO_DATE('"+four+"', '%d/%m/%Y')";

		}
		else if(!first.trim().equals("") && !second.trim().equals("") && !third.trim().equals("") && !four.trim().equals(""))
		{
			sql="select * from "+table_name+" where "+first_column+"='"+first+"' and "+second_column+"='"+second+"' and STR_TO_DATE("+third_column+", '%d/%m/%Y') Between STR_TO_DATE('"+third+"', '%d/%m/%Y') and  STR_TO_DATE('"+four+"', '%d/%m/%Y')";

		}else {
			sql="select * from "+table_name+"";
		}



		return sql;
	}
	
	public 	static String TwoFieldSearch(String first,String second, 
			String table_name, String first_column, String second_column)
	{
		String sql="";
		if(first.trim().equals("") && second.trim().equals("") )
		{
			sql="select * from "+table_name+"";
		}
		else if(!first.trim().equals("") && second.trim().equals("") )
		{
			sql="select * from "+table_name+" where "+first_column+"='"+first+"'";

		}
		else if(first.trim().equals("") && !second.trim().equals("") )
		{
			sql="select * from "+table_name+" where "+second_column+"='"+second+"'";

		}
		else if(!first.trim().equals("") && !second.trim().equals("") )
		{
			sql="select * from "+table_name+" where "+first_column+"='"+first+"' and "+second_column+"='"+second+"' ";

		}else {
			sql="select * from "+table_name+"";

		}
		return sql;
	}
	

}
