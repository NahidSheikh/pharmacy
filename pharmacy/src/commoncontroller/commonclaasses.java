package commoncontroller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import generatedclasses.ConnectionClass;

public class commonclaasses {

	public static int hospital_master()
	{
		int count=0;
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="select Count(*) from master_hospital";
			PreparedStatement pst=con.prepareStatement(sql);
			ResultSet rs= pst.executeQuery();
			while(rs.next())
			{
				count=rs.getInt(1);
			}
			rs.close();
			pst.close();
			con.close();
			
		}catch(Exception e)
		{
			e.getMessage();
		}
		return count;
	}
	
	
	public static int inward_invoice_no()
	{
		int count=0;
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="select id from pharmacy_medicine_inward_order order by id desc limit 1";
			PreparedStatement pst=con.prepareStatement(sql);
			ResultSet rs= pst.executeQuery();
			while(rs.next())
			{
				count=rs.getInt(1);
			}
			rs.close();
			pst.close();
			con.close();
			
		}catch(Exception e)
		{
			e.getMessage();
		}
		return count;
	}
	
	public static int sale_bill_no()
	{
		int count=0;
		try {
			new ConnectionClass();
			Connection con= ConnectionClass.getconnection();
			String sql="select id from pharmacy_outpatient_sale_bill order by id desc limit 1";
			PreparedStatement pst=con.prepareStatement(sql);
			ResultSet rs= pst.executeQuery();
			while(rs.next())
			{
				count=rs.getInt(1);
			}
			rs.close();
			pst.close();
			con.close();
			
		}catch(Exception e)
		{
			e.getMessage();
		}
		return count;
	}
	
}
